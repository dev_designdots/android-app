package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class Consumable {

    @SerializedName("consumableId")
    public Integer consumableId;

    @SerializedName("quantity")
    public Float quantity;

    @SerializedName("receivedQuantity")
    public Float receivedQuantity;

    public Float availableQuantity;

    @SerializedName("receiptStatus")
    public Integer receiptStatus;

    @SerializedName("consumableName")
    public String consumableName;

    @SerializedName("unitOfMeasure")
    public String unitOfMeasure;

    @SerializedName("description")
    public String description;


}
