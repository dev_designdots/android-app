package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class ConsumableTable {

    private static String TAG = ConsumableTable.class.getCanonicalName();

    private static ConsumableTable mInstance;
    private static SQLiteDatabase mDatabase;

    private ConsumableTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new ConsumableTable(database);
        }
    }

    /***
     * @return instance
     */
    public static ConsumableTable getInstance() {
        return mInstance;
    }


    public ArrayList<Consumable> getAllConsumables() {
        ArrayList<Consumable> consumableList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_CONSUMABLE;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Consumable consumable = new Consumable();

            consumable.consumableId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLEID));
            consumable.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            consumable.receivedQuantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            consumable.consumableName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLE_NAME));
            consumable.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
            consumable.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            consumable.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));
            consumable.availableQuantity = consumable.receivedQuantity - ConsumableAllocationTable.getInstance().getAllocatedConsumableQuantity(consumable.consumableId);

            consumableList.add(consumable);

            cursor.moveToNext();
        }


        return consumableList;


    }

    public ArrayList<Consumable> getConsumables(Integer receiptStatus) {
        ArrayList<Consumable> consumableList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_CONSUMABLE + " WHERE " + DbConstants.KEY_RECEIPT_STATUS + "='" + receiptStatus + "'";
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Consumable consumable = new Consumable();

            consumable.consumableId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLEID));
            consumable.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            consumable.receivedQuantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            consumable.consumableName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLE_NAME));
            consumable.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
            consumable.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            consumable.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));
            consumable.availableQuantity = consumable.receivedQuantity - ConsumableAllocationTable.getInstance().getAllocatedConsumableQuantity(consumable.consumableId);

            consumableList.add(consumable);

            cursor.moveToNext();
        }


        return consumableList;
    }


    public void updateConsumablesStatus(ArrayList<Consumable> updatedConsumablesList) {

        for (Consumable aConsumable : updatedConsumablesList) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_CONSUMABLEID, aConsumable.consumableId);
            values.put(DbConstants.KEY_QUANTITY, aConsumable.quantity);
            values.put(DbConstants.KEY_RECEIVED_QUANTITY, aConsumable.receivedQuantity);
            values.put(DbConstants.KEY_RECEIPT_STATUS, aConsumable.receiptStatus);
            values.put(DbConstants.KEY_CONSUMABLE_NAME, aConsumable.consumableName);
            values.put(DbConstants.KEY_DESCRIPTION, aConsumable.description);
            values.put(DbConstants.KEY_UOM, aConsumable.unitOfMeasure);


            mDatabase.update(DbConstants.TABLE_CONSUMABLE, values,
                    DbConstants.KEY_CONSUMABLEID + "=" + aConsumable.consumableId,
                    null);

        }


    }

    public void insertAllConsumables(ArrayList<Consumable> consumableList) {


        for (Consumable aConsumable : consumableList) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_CONSUMABLEID, aConsumable.consumableId);
            values.put(DbConstants.KEY_QUANTITY, aConsumable.quantity);
            values.put(DbConstants.KEY_RECEIVED_QUANTITY, aConsumable.receivedQuantity);
            values.put(DbConstants.KEY_RECEIPT_STATUS, aConsumable.receiptStatus);
            values.put(DbConstants.KEY_CONSUMABLE_NAME, aConsumable.consumableName);
            values.put(DbConstants.KEY_DESCRIPTION, aConsumable.description);
            values.put(DbConstants.KEY_UOM, aConsumable.unitOfMeasure);


            mDatabase.insert(DbConstants.TABLE_CONSUMABLE, null, values);

        }

    }


    public Consumable getAllocatedConsumable(Integer aId) {
        Consumable consumable = null;
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_CONSUMABLE + " WHERE " + DbConstants.KEY_CONSUMABLEID + "=" + aId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            consumable = new Consumable();

            consumable.consumableId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLEID));
            consumable.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            consumable.receivedQuantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            consumable.consumableName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLE_NAME));
            consumable.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
            consumable.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            consumable.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));
            consumable.availableQuantity = consumable.receivedQuantity - ConsumableAllocationTable.getInstance().getAllocatedConsumableQuantity(consumable.consumableId);


            cursor.moveToNext();
        }
        return consumable;
    }
}
