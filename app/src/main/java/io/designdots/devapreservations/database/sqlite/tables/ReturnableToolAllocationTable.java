package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.model.ReturnableToolAllocation;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class ReturnableToolAllocationTable {

    private static String TAG = ReturnableToolAllocationTable.class.getCanonicalName();

    private static ReturnableToolAllocationTable mInstance;
    private static SQLiteDatabase mDatabase;

    private ReturnableToolAllocationTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new ReturnableToolAllocationTable(database);
        }
    }

    /***
     * @return instance
     */
    public static ReturnableToolAllocationTable getInstance() {
        return mInstance;
    }


    public void allocateorReturnReturnableTool(ReturnableToolAllocation alloc) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_RETURNABLE_TOOL_ID, alloc.toolId);
        values.put(DbConstants.KEY_ISSUED_DATETIME, alloc.issuedDateTime);
        values.put(DbConstants.KEY_RETURNED_DATETIME, alloc.returnedDateTime);
        values.put(DbConstants.KEY_WORK_HOURS, alloc.workHours);
        values.put(DbConstants.KEY_RETURN_STATUS, alloc.returnStatus);
        values.put(DbConstants.KEY_EQUIPMENTS, alloc.equipments);
        values.put(DbConstants.KEY_ENGINEER_ID, alloc.engineerId);
        values.put(DbConstants.KEY_TECHNICIAN_ID, alloc.technicianId);


        if (alloc.toolAllocKey == null) {
            mDatabase.insert(DbConstants.TABLE_RETURNABLE_TOOL_ALLOCATION, null, values);
        } else {
            mDatabase.update(DbConstants.TABLE_RETURNABLE_TOOL_ALLOCATION, values, DbConstants.KEY_RETURNABLE_TOOL_ALLOC_KEY + "=" + alloc.toolAllocKey, null);
        }

    }

    public ArrayList<ReturnableToolAllocation> getAllocatedToolsDetails(ArrayList<ReturnableTool> atoolsList) {

        String atoolIds = "";
        for (int i = 0; i < atoolsList.size(); i++) {
            ReturnableTool t = atoolsList.get(i);
            if (i == (atoolsList.size() - 1)) {
                atoolIds = atoolIds + t.toolId;
            } else {
                atoolIds = atoolIds + t.toolId + ",";
            }
        }
        ArrayList<ReturnableToolAllocation> allocatedToolsDetailsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL_ALLOCATION + " WHERE "
                + DbConstants.KEY_RETURNED_DATETIME + "=null AND "
                + DbConstants.KEY_RETURNABLE_TOOL_ID + " IN (" + atoolIds + ")";

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ReturnableToolAllocation allocation = new ReturnableToolAllocation();
            allocation.toolAllocKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ALLOC_KEY));
            allocation.toolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
            allocation.equipments = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENTS));
            allocation.issuedDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ISSUED_DATETIME));
            allocation.returnedDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_RETURNED_DATETIME));
            allocation.engineerId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_ENGINEER_ID));
            allocation.technicianId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));
            allocation.workHours = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_WORK_HOURS));

            allocatedToolsDetailsList.add(allocation);

            cursor.moveToNext();
        }
        return allocatedToolsDetailsList;
    }

    public String getAllocatedToolIds() {
        String toolIds = "";
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL_ALLOCATION + " WHERE " +
                DbConstants.KEY_RETURNED_DATETIME + "=null GROUP BY " + DbConstants.KEY_RETURNABLE_TOOL_ID;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Integer toolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
            if (cursor.getPosition() == cursor.getCount() - 1) {
                toolIds = toolIds + toolId;
            } else {
                toolIds = toolIds + toolId + ",";
            }
            cursor.moveToNext();
        }

        return toolIds;
    }

    public void updateToolAllocation(ReturnableToolAllocation allocation) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_RETURN_STATUS, allocation.returnStatus);
        values.put(DbConstants.KEY_RETURNED_DATETIME, allocation.returnedDateTime);

        mDatabase.update(DbConstants.TABLE_RETURNABLE_TOOL_ALLOCATION, values, DbConstants.KEY_RETURNABLE_TOOL_ID + "=" + allocation.toolId, null);
    }

    public ArrayList<ReturnableToolAllocation> getAllocationDetails(Integer aReturnabletoolId) {

        ArrayList<ReturnableToolAllocation> allocatedToolsDetailsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL_ALLOCATION + " WHERE "
                + DbConstants.KEY_RETURNABLE_TOOL_ID + "=" + aReturnabletoolId;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ReturnableToolAllocation allocation = new ReturnableToolAllocation();
            allocation.toolAllocKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ALLOC_KEY));
            allocation.toolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
            allocation.equipments = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENTS));
            allocation.issuedDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ISSUED_DATETIME));
            allocation.returnedDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_RETURNED_DATETIME));
            allocation.engineerId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_ENGINEER_ID));
            allocation.technicianId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));
            allocation.workHours = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_WORK_HOURS));

            allocatedToolsDetailsList.add(allocation);

            cursor.moveToNext();
        }
        return allocatedToolsDetailsList;
    }
}
