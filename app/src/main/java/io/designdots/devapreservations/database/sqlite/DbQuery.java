package io.designdots.devapreservations.database.sqlite;

public class DbQuery {


    public String[] createTables = {

            "CREATE TABLE `Consumable` ( `consumableId` INTEGER, `quantity` REAL, `receivedQuantity` REAL, `receiptStatus` INTEGER, `consumableName` TEXT, `unitOfMeasure` TEXT, `description` TEXT, PRIMARY KEY(`consumableId`) )",

            "CREATE TABLE `Equipment` ( `equipmentId` INTEGER, `equipmentCategoryName` TEXT, `equipmentName` TEXT, `location` TEXT, `tagNumber` TEXT, `zone` TEXT, `contentWhenFilled` TEXT, `waterOrDiselOilBased` TEXT, `emptyVolumeM3` REAL, `contentVolume` REAL, `preservationEngineer` TEXT, PRIMARY KEY(`equipmentId`) )",

            "CREATE TABLE `Preservative` ( `preservativeId` INTEGER, `quantity` REAL, `receivedQuantity` REAL, `receiptStatus` INTEGER, `chemicalId` TEXT, `chemicalName` TEXT, `unitOfMeasure` TEXT, PRIMARY KEY(`preservativeId`) )",

            "CREATE TABLE `ReturnableTool` ( `returnableToolId` INTEGER, `quantity` INTEGER, `receivedQuantity` INTEGER, `receiptStatus` INTEGER, `brand` TEXT, `description` TEXT, `model` TEXT, `returnReason` TEXT, `toolId` TEXT, `toolName` TEXT, `unitOfMeasure` TEXT, PRIMARY KEY(`returnableToolId`) )",

            "CREATE TABLE `Technician`( `partyKey` INTEGER, `firstName` TEXT, `surName` TEXT, `userName` TEXT, `role` TEXT, `groupId` INTEGER DEFAULT -1, `equipmentId` INTEGER, PRIMARY KEY(`partyKey`) )",

            "CREATE TABLE `PreservationEngineer` ( `partyKey` INTEGER, `firstName` TEXT, `surName` TEXT, `userName` TEXT, `role` TEXT, PRIMARY KEY(`partyKey`) )",

            "CREATE TABLE `EquipmentTechnicianAssignment` ( `equipmentTechnicianAssignmentId` INTEGER PRIMARY KEY AUTOINCREMENT, `equipmentId` INTEGER, `technicianId` INTEGER, `createdDateTime` TEXT )",

            "CREATE TABLE `TechnicianAssignment` ( `engineerTechnicianAssignmentId` INTEGER, `engineerId` INTEGER, `technicianId` INTEGER, PRIMARY KEY(`engineerTechnicianAssignmentId`) )",

            "CREATE TABLE `PreservativeAllocation` ( `preservativeAllocKey` INTEGER PRIMARY KEY AUTOINCREMENT, `preservativeId` INTEGER, `equipments` TEXT, `quantity` REAL, `engineerId` INTEGER, `technicianId` INTEGER, `createdDateTime` TEXT )",

            "CREATE TABLE `ConsumableAllocation` ( `consumableAllocKey` INTEGER PRIMARY KEY AUTOINCREMENT, `consumableId` INTEGER, `equipments` TEXT, `quantity` REAL, `engineerId` INTEGER, `technicianId` INTEGER, `createdDateTime` TEXT )",

            "CREATE TABLE `ReturnableToolAllocation` ( `toolAllocKey` INTEGER PRIMARY KEY AUTOINCREMENT, `returnableToolId` INTEGER, `issuedDateTime` TEXT, `returnedDateTime` TEXT, `workHours` REAL, `returnStatus` INTEGER, `equipments` TEXT, `engineerId` INTEGER, `technicianId` INTEGER )",

            "CREATE TABLE `EquipmentPreservative` ( `equipmentPreservativeId` INTEGER, `preservativeDescription` TEXT, `quantityRequire` TEXT, `equipmentId` INTEGER, `preservativeId` INTEGER, `percentageComplete` INTEGER, `preservationStartDate` TEXT, `preservationEndDate` TEXT, `procedureDetailId` TEXT, `preservativeType` TEXT, PRIMARY KEY(`equipmentPreservativeId`) )"

    };
}