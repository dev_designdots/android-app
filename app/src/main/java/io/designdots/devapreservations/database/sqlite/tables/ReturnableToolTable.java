package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class ReturnableToolTable {

    private static String TAG = ReturnableToolTable.class.getCanonicalName();

    private static ReturnableToolTable mInstance;
    private static SQLiteDatabase mDatabase;

    private ReturnableToolTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new ReturnableToolTable(database);
        }
    }

    /***
     * @return instance
     */
    public static ReturnableToolTable getInstance() {
        return mInstance;
    }


    public HashMap<String, ArrayList<ReturnableTool>> getUnAllocatedTools() {
        String allocatedToolIds = ReturnableToolAllocationTable.getInstance().getAllocatedToolIds();

        HashMap<String, ArrayList<ReturnableTool>> aReturnableToolsListMap = new HashMap<>();

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL +
                " WHERE " + DbConstants.KEY_TOOL_ID + " NOT IN (" + allocatedToolIds + ") ORDER BY "
                + DbConstants.KEY_TOOL_NAME;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        String aToolName = "";
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            ArrayList<ReturnableTool> aReturnableToolsList = new ArrayList<>();
            aToolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
            while (!cursor.isAfterLast()) {

                if (aToolName.equals(cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME)))) {
                    // same category

                } else {
                    aReturnableToolsListMap.put(aToolName, aReturnableToolsList);

                    aReturnableToolsList = new ArrayList<>();
                    aToolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
                }


                ReturnableTool returnableTool = new ReturnableTool();
                returnableTool.returnableToolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
                returnableTool.toolId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_ID));
                returnableTool.toolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
                returnableTool.model = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_MODEL));
                returnableTool.brand = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BRAND));
                returnableTool.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
                returnableTool.quantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
                returnableTool.receivedQuantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
                returnableTool.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
                returnableTool.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));

                aReturnableToolsList.add(returnableTool);

                cursor.moveToNext();
            }

            aReturnableToolsListMap.put(aToolName, aReturnableToolsList);
        }
        return aReturnableToolsListMap;
    }


    public HashMap<String, ArrayList<ReturnableTool>> getAllocatedTools() {
        String allocatedToolIds = ReturnableToolAllocationTable.getInstance().getAllocatedToolIds();

        HashMap<String, ArrayList<ReturnableTool>> aReturnableToolsListMap = new HashMap<>();

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL +
                " WHERE " + DbConstants.KEY_TOOL_ID + " IN (" + allocatedToolIds + ") ORDER BY "
                + DbConstants.KEY_TOOL_NAME;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        String aToolName = "";
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            ArrayList<ReturnableTool> aReturnableToolsList = new ArrayList<>();
            aToolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
            while (!cursor.isAfterLast()) {

                if (aToolName.equals(cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME)))) {
                    // same category

                } else {
                    aReturnableToolsListMap.put(aToolName, aReturnableToolsList);

                    aReturnableToolsList = new ArrayList<>();
                    aToolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
                }


                ReturnableTool returnableTool = new ReturnableTool();
                returnableTool.returnableToolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
                returnableTool.toolId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_ID));
                returnableTool.toolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
                returnableTool.model = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_MODEL));
                returnableTool.brand = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BRAND));
                returnableTool.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
                returnableTool.quantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
                returnableTool.receivedQuantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
                returnableTool.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
                returnableTool.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));

                aReturnableToolsList.add(returnableTool);

                cursor.moveToNext();
            }

            aReturnableToolsListMap.put(aToolName, aReturnableToolsList);
        }
        return aReturnableToolsListMap;
    }


    public void updateTools(ArrayList<ReturnableTool> returnableTools) {

        // update receipt status of each tool
        if (returnableTools.size() > 0) {
            for (ReturnableTool tool : returnableTools) {
                ContentValues values = new ContentValues();
                values.put(DbConstants.KEY_RECEIPT_STATUS, tool.receiptStatus);

                mDatabase.update(DbConstants.TABLE_RETURNABLE_TOOL, values, DbConstants.KEY_RETURNABLE_TOOL_ID + "=" + tool.returnableToolId, null);
            }

            // Update received quantity count
            ReturnableTool tool = returnableTools.get(0);
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_RECEIVED_QUANTITY, tool.receivedQuantity);

            mDatabase.update(DbConstants.TABLE_RETURNABLE_TOOL, values, DbConstants.KEY_TOOL_NAME + "='" + tool.toolName + "'", null);
        }


    }


    public ArrayList<ReturnableTool> getDistinctTools() {
        ArrayList<ReturnableTool> aToolsList = new ArrayList<>();

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL + " GROUP BY " + DbConstants.KEY_TOOL_NAME;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ReturnableTool returnableTool = new ReturnableTool();
            returnableTool.quantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            returnableTool.receivedQuantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            returnableTool.toolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));

            aToolsList.add(returnableTool);

            cursor.moveToNext();
        }
        return aToolsList;
    }

    public ArrayList<ReturnableTool> getTools(String aToolname, int ReceiptStatus) {
        ArrayList<ReturnableTool> aToolsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL + " WHERE "
                + DbConstants.KEY_TOOL_NAME + " ='" + aToolname + "' AND "
                + DbConstants.KEY_RECEIPT_STATUS + " =" + ReceiptStatus;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ReturnableTool returnableTool = new ReturnableTool();

            returnableTool.returnableToolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
            returnableTool.toolId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_ID));
            returnableTool.toolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
            returnableTool.model = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_MODEL));
            returnableTool.brand = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BRAND));
            returnableTool.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
            returnableTool.quantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            returnableTool.receivedQuantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            returnableTool.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            returnableTool.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));

            aToolsList.add(returnableTool);

            cursor.moveToNext();
        }
        return aToolsList;

    }

    public void insertAllReturnableTools(ArrayList<ReturnableTool> aReturnableToolsList) {

        for (ReturnableTool tool : aReturnableToolsList) {

            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_RETURNABLE_TOOL_ID, tool.returnableToolId);
            values.put(DbConstants.KEY_TOOL_ID, tool.toolId);
            values.put(DbConstants.KEY_TOOL_NAME, tool.toolName);
            values.put(DbConstants.KEY_MODEL, tool.model);
            values.put(DbConstants.KEY_BRAND, tool.brand);
            values.put(DbConstants.KEY_DESCRIPTION, tool.description);
            values.put(DbConstants.KEY_QUANTITY, tool.quantity);
            values.put(DbConstants.KEY_RECEIVED_QUANTITY, tool.receivedQuantity);
            values.put(DbConstants.KEY_UOM, tool.unitOfMeasure);
            values.put(DbConstants.KEY_RECEIPT_STATUS, tool.receiptStatus);

            mDatabase.insert(DbConstants.TABLE_RETURNABLE_TOOL, null, values);
        }

    }

    public ArrayList<ReturnableTool> getFinalReturnableToolsforAllocation(ArrayList<Integer> toolIdsList) {

        String toolIdStr = "";
        for (int i = 0; i < toolIdsList.size(); i++) {
            if (i == toolIdsList.size() - 1) {
                toolIdStr = toolIdStr + toolIdsList.get(i);
            } else {
                toolIdStr = toolIdStr + toolIdsList.get(i) + ",";
            }
        }

        ArrayList<ReturnableTool> aToolsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL + " WHERE "
                + DbConstants.KEY_RETURNABLE_TOOL_ID + " IN (" + toolIdStr + ")";

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ReturnableTool returnableTool = new ReturnableTool();

            returnableTool.returnableToolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
            returnableTool.toolId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_ID));
            returnableTool.toolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
            returnableTool.model = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_MODEL));
            returnableTool.brand = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BRAND));
            returnableTool.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
            returnableTool.quantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            returnableTool.receivedQuantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            returnableTool.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            returnableTool.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));

            aToolsList.add(returnableTool);

            cursor.moveToNext();
        }
        return aToolsList;


    }

    public ArrayList<ReturnableTool> getAllTools(String aToolName) {

        ArrayList<ReturnableTool> aToolsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_RETURNABLE_TOOL + " WHERE "
                + DbConstants.KEY_TOOL_NAME + " ='" + aToolName + "'";
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            ReturnableTool returnableTool = new ReturnableTool();

            returnableTool.returnableToolId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RETURNABLE_TOOL_ID));
            returnableTool.toolId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_ID));
            returnableTool.toolName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TOOL_NAME));
            returnableTool.model = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_MODEL));
            returnableTool.brand = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BRAND));
            returnableTool.description = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DESCRIPTION));
            returnableTool.quantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            returnableTool.receivedQuantity = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            returnableTool.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            returnableTool.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));

            aToolsList.add(returnableTool);

            cursor.moveToNext();
        }
        return aToolsList;
    }
}
