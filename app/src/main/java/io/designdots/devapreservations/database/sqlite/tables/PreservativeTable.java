package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class PreservativeTable {

    private static String TAG = PreservativeTable.class.getCanonicalName();

    private static PreservativeTable mInstance;
    private static SQLiteDatabase mDatabase;

    private PreservativeTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new PreservativeTable(database);
        }
    }

    /***
     * @return instance
     */
    public static PreservativeTable getInstance() {
        return mInstance;
    }


    public ArrayList<Preservative> getAllPreservatives() {

        ArrayList<Preservative> preservativesList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATIVE;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Preservative preservative = new Preservative();

            preservative.preservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ID));
            preservative.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            preservative.receivedQuantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            preservative.chemicalId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CHEMICAL_ID));
            preservative.chemicalName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CHEMICAL_NAME));
            preservative.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            preservative.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));
            preservative.availableQuantity = preservative.receivedQuantity - PreservativeAllocationTable.getInstance().getAllocatedPreservativeQuantity(preservative.preservativeId);

            preservativesList.add(preservative);
            cursor.moveToNext();
        }


        return preservativesList;

    }


    public ArrayList<Preservative> getPreservatives(Integer receiptStatus) {
        ArrayList<Preservative> preservativesList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATIVE + " WHERE " + DbConstants.KEY_RECEIPT_STATUS + "='" + receiptStatus + "'";
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Preservative preservative = new Preservative();

            preservative.preservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ID));
            preservative.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            preservative.receivedQuantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            preservative.chemicalId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CHEMICAL_ID));
            preservative.chemicalName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CHEMICAL_NAME));
            preservative.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            preservative.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));
            preservative.availableQuantity = preservative.receivedQuantity - PreservativeAllocationTable.getInstance().getAllocatedPreservativeQuantity(preservative.preservativeId);

            preservativesList.add(preservative);
            cursor.moveToNext();
        }


        return preservativesList;
    }


    public void updatePreservativesStatus(ArrayList<Preservative> updatedPreservativeList) {

        for (Preservative aPreservative : updatedPreservativeList) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_QUANTITY, aPreservative.quantity);
            values.put(DbConstants.KEY_RECEIVED_QUANTITY, aPreservative.receivedQuantity);
            values.put(DbConstants.KEY_RECEIPT_STATUS, aPreservative.receiptStatus);
            values.put(DbConstants.KEY_CHEMICAL_ID, aPreservative.chemicalId);
            values.put(DbConstants.KEY_CHEMICAL_NAME, aPreservative.chemicalName);
            values.put(DbConstants.KEY_UOM, aPreservative.unitOfMeasure);


            mDatabase.update(DbConstants.TABLE_PRESERVATIVE, values,
                    DbConstants.KEY_PRESERVATIVE_ID + "=" + aPreservative.preservativeId,
                    null);

        }


    }

    public void insertAllPreservatives(ArrayList<Preservative> preservativesList) {


        for (Preservative aPreservative : preservativesList) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_PRESERVATIVE_ID, aPreservative.preservativeId);
            values.put(DbConstants.KEY_QUANTITY, aPreservative.quantity);
            values.put(DbConstants.KEY_RECEIVED_QUANTITY, aPreservative.receivedQuantity);
            values.put(DbConstants.KEY_RECEIPT_STATUS, DbConstants.NOT_RECEIVED);
            values.put(DbConstants.KEY_CHEMICAL_ID, aPreservative.chemicalId);
            values.put(DbConstants.KEY_CHEMICAL_NAME, aPreservative.chemicalName);
            values.put(DbConstants.KEY_UOM, aPreservative.unitOfMeasure);


            mDatabase.insert(DbConstants.TABLE_PRESERVATIVE, null, values);


            EquipmentPreservativeTable.getInstance().insertallEquipmentPreservatives(aPreservative.equipmentPreservatives);

        }

    }

    public Preservative getAllocatedPreservative(Integer aId) {
        Preservative preservative = null;

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATIVE + " WHERE " + DbConstants.KEY_PRESERVATIVE_ID + "=" + aId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            preservative = new Preservative();

            preservative.preservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ID));
            preservative.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            preservative.receivedQuantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_RECEIVED_QUANTITY));
            preservative.chemicalId = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CHEMICAL_ID));
            preservative.chemicalName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CHEMICAL_NAME));
            preservative.unitOfMeasure = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_UOM));
            preservative.receiptStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_RECEIPT_STATUS));
            preservative.availableQuantity = preservative.receivedQuantity - PreservativeAllocationTable.getInstance().getAllocatedPreservativeQuantity(preservative.preservativeId);

            cursor.moveToNext();
        }

        return preservative;
    }
}
