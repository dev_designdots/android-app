package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Equipment {
    @SerializedName("equipmentId")
    public Integer equipmentId;
    @SerializedName("equipmentCategoryName")
    public String equipmentCategoryName;
    @SerializedName("equipmentName")
    public String equipmentName;
    @SerializedName("location")
    public String location;
    @SerializedName("tagNumber")
    public String tagNumber;
    @SerializedName("zone")
    public String zone;
    @SerializedName("contentWhenFilled")
    public String contentWhenFilled;
    @SerializedName("waterOrDiselOilBased")
    public String waterOrDiselOilBased;
    @SerializedName("emptyVolumeM3")
    public Float emptyVolumeM3;
    @SerializedName("contentVolume")
    public Float contentVolume;
    @SerializedName("preservationEngineer")
    public PreservationEngineer preservationEngineer;
    @SerializedName("cmcrequired")
    public Boolean cmcrequired;
    @SerializedName("equipmentPreservatives")
    public ArrayList<EquipmentPreservative> equipmentPreservatives;


    public Float allocatedQuantity;

    public Integer internalPercentageCompletion;

    public Integer externalPercentageCompletion;

    public Integer totalPercentageCompletion;

    public String beforeImage;

    public String beforeImageDateTime;

    public String afterImage;

    public String afterImageDateTime;

    public String duringImage;

    public String duringImageDateTime;

    public Integer isCompatibilityTestRequired;

    public String compatibilityTestReason;

    public Integer percentageOfAddition;

    public Integer compatibilityTestStatus;

    public String compatibilityTestBeforePhoto;

    public String compatibilityTestBeforePhotoDateTime;

    public String compatiblityTestAfterPhoto;

    public String compatibilityTestAfterPhotoDateTime;

    public String testStartDateTime;

    public String testEndDateTime;

    public Integer isCMCAdded;

    public String cmcDetails;

    public String cmcInstalledDateTime;

    public String cmcBeforePhoto;

    public String cmcBeforePhotoDateTime;

    public String cmcAfterPhoto;

    public String cmcAfterPhotoDateTime;

    public Integer equipmentStatus;



}
