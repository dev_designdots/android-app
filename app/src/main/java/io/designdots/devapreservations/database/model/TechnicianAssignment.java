package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class TechnicianAssignment {

    @SerializedName("engineerTechnicianAssignmentId")
    public Integer engineerTechnicianAssignmentId;

    @SerializedName("engineerId")
    public Integer engineerId;

    @SerializedName("technicianId")
    public Integer technicianId;
}
