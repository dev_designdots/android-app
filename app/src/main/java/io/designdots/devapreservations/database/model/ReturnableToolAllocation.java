package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class ReturnableToolAllocation {

    @SerializedName("toolAllocKey")
    public Integer toolAllocKey;

    @SerializedName("toolId")
    public Integer toolId;

    @SerializedName("issuedDateTime")
    public String issuedDateTime;

    @SerializedName("returnedDateTime")
    public String returnedDateTime;

    @SerializedName("workHours")
    public Float workHours;

    @SerializedName("returnStatus")
    public Integer returnStatus;

    @SerializedName("equipments")
    public String equipments;

    @SerializedName("engineerId")
    public Integer engineerId;

    @SerializedName("technicianId")
    public Integer technicianId;


}
