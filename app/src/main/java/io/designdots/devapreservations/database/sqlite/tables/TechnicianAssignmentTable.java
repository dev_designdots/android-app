package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.TechnicianAssignment;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class TechnicianAssignmentTable {
    private static String TAG = TechnicianAssignmentTable.class.getCanonicalName();

    private static TechnicianAssignmentTable mInstance;
    private static SQLiteDatabase mDatabase;

    private TechnicianAssignmentTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new TechnicianAssignmentTable(database);
        }
    }

    /***
     * @return instance
     */
    public static TechnicianAssignmentTable getInstance() {
        return mInstance;
    }

    public void insertAllTechnicianAssignments(ArrayList<TechnicianAssignment> assignments) {

        for (TechnicianAssignment assignment : assignments) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_ENGINEER_TECHNICIAN_ASSIGNMENT_ID, assignment.engineerTechnicianAssignmentId);
            values.put(DbConstants.KEY_ENGINEER_ID, assignment.engineerId);
            values.put(DbConstants.KEY_TECHNICIAN_ID, assignment.technicianId);

            mDatabase.insert(DbConstants.TABLE_TECHNICIAN_ASSIGNMENT, null, values);

        }
    }

    public String getTechniciansUnderEngineer(Integer engineerId) {

        String aTechnicanIds = "";

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN_ASSIGNMENT + " WHERE "
                + DbConstants.KEY_ENGINEER_ID + "=" + engineerId;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Integer techid = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));


         if ((cursor.getPosition() == cursor.getCount() - 1)) {
                aTechnicanIds = aTechnicanIds + techid;
            } else {
                aTechnicanIds = aTechnicanIds + techid + ",";

            }
            cursor.moveToNext();
        }
        return aTechnicanIds;
    }


}
