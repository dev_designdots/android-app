package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class EquipmentPreservative {

    @SerializedName("equipmentPreservativeId")
    public Integer equipmentPreservativeId;

    @SerializedName("preservativeDescription")
    public String preservativeDescription;

    @SerializedName("quantityRequire")
    public String quantityRequire;

    @SerializedName("projectEquipmentId")
    public Integer equipmentId;

    @SerializedName("projectPreservativeId")
    public Integer preservativeId;

    public String chemicalName;

    @SerializedName("preservationStartDate")
    public String preservationStartDate;

    @SerializedName("preservationEndDate")
    public String preservationEndDate;

    @SerializedName("procedureDetailId")
    public String procedureDetailId;

    @SerializedName("preservativeTypeDto")
    public PreservativeType preservativeTypeDto;


    public class PreservativeType {

        @SerializedName("preservativeTypeKey")
        public Integer preservativeTypeKey;

        @SerializedName("preservationCategory")
        public String preservationCategory;

        @SerializedName("preservationType")
        public String preservationType;
    }

}
