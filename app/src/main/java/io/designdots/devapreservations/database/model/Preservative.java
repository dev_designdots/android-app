package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Preservative {

    @SerializedName("preservativeId")
    public Integer preservativeId;

    @SerializedName("quantity")
    public Float quantity;

    @SerializedName("receivedQuantity")
    public Float receivedQuantity;

    public Float availableQuantity;

    @SerializedName("receiptStatus")
    public Integer receiptStatus;

    @SerializedName("chemicalId")
    public String chemicalId;

    @SerializedName("chemicalName")
    public String chemicalName;

    @SerializedName("unitOfMeasure")
    public String unitOfMeasure;

    @SerializedName("equipmentPreservatives")
    public ArrayList<EquipmentPreservative> equipmentPreservatives;
}
