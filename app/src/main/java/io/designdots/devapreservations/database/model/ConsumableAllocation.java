package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class ConsumableAllocation {

    @SerializedName("consumableAllocKey")
    public Integer consumableAllocKey;

    @SerializedName("consumableId")
    public Integer consumableId;

    @SerializedName("equipments")
    public String equipments;

    @SerializedName("quantity")
    public Float quantity;

    @SerializedName("engineerId")
    public Integer engineerId;

    @SerializedName("technicianId")
    public Integer technicianId;

    @SerializedName("createdDateTime")
    public String createdDateTime;

}
