package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.ConsumableAllocation;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class ConsumableAllocationTable {

    private static String TAG = ConsumableAllocationTable.class.getCanonicalName();

    private static ConsumableAllocationTable mInstance;
    private static SQLiteDatabase mDatabase;

    private ConsumableAllocationTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new ConsumableAllocationTable(database);
        }
    }

    /***
     * @return instance
     */
    public static ConsumableAllocationTable getInstance() {
        return mInstance;
    }

    /**
     * Method to allocate a consumable during confirmation
     */
    public void allocateConsumable(ConsumableAllocation alloc) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_CONSUMABLEID, alloc.consumableId);
        values.put(DbConstants.KEY_QUANTITY, alloc.quantity);
        values.put(DbConstants.KEY_EQUIPMENTS, alloc.equipments);
        values.put(DbConstants.KEY_ENGINEER_ID, alloc.engineerId);
        values.put(DbConstants.KEY_TECHNICIAN_ID, alloc.technicianId);
        values.put(DbConstants.KEY_CREATED_DATE_TIME, alloc.createdDateTime);

        mDatabase.insert(DbConstants.TABLE_CONSUMABLE_ALLOCATION, null, values);
    }

    /**
     * Get the multiple allocation details for a consumable
     *
     * @param aConsumableId
     * @return
     */
    public ArrayList<ConsumableAllocation> getAllocatedDataforConsumable(Integer aConsumableId) {

        ArrayList<ConsumableAllocation> allocationDetailsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_CONSUMABLE_ALLOCATION + " WHERE " + DbConstants.KEY_CONSUMABLEID + "=" + aConsumableId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ConsumableAllocation allocation = new ConsumableAllocation();
            allocation.consumableAllocKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLE_ALLOC_KEY));
            allocation.consumableId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLEID));
            allocation.equipments = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENTS));
            allocation.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            allocation.engineerId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_ENGINEER_ID));
            allocation.technicianId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));

            allocationDetailsList.add(allocation);
            cursor.moveToNext();
        }
        return allocationDetailsList;
    }

    /**
     * Gives the total qty of consumable allocated so far
     *
     * @param consumableId
     * @return
     */
    public Float getAllocatedConsumableQuantity(Integer consumableId) {

        Float allocatedQty = null;
        Cursor cursor = null;
        String sql = "SELECT SUM (" + DbConstants.KEY_QUANTITY + ") AS TOTAL  FROM " + DbConstants.TABLE_CONSUMABLE_ALLOCATION + " WHERE " + DbConstants.KEY_CONSUMABLEID + "=" + consumableId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            allocatedQty = cursor.getFloat(cursor.getColumnIndex("TOTAL"));
            cursor.moveToNext();
        }

        if (allocatedQty != null)
            return allocatedQty;
        else return Float.parseFloat("0");
    }

    /**
     * Get allocation detail of a allocation
     * @param aConsumableAllocationKey
     * @return
     */
    public ConsumableAllocation getAllocation(Integer aConsumableAllocationKey) {
        ConsumableAllocation allocation = new ConsumableAllocation();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_CONSUMABLE_ALLOCATION + " WHERE " + DbConstants.KEY_CONSUMABLE_ALLOC_KEY + "=" + aConsumableAllocationKey;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            allocation.consumableAllocKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLE_ALLOC_KEY));
            allocation.consumableId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_CONSUMABLEID));
            allocation.equipments = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENTS));
            allocation.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            allocation.engineerId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_ENGINEER_ID));
            allocation.technicianId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));

            cursor.moveToNext();
        }
        return allocation;
    }
}
