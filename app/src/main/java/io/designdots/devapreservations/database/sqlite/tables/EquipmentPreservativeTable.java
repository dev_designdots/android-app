package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.EquipmentPreservative;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class EquipmentPreservativeTable {

    private static String TAG = EquipmentPreservativeTable.class.getCanonicalName();

    private static EquipmentPreservativeTable mInstance;
    private static SQLiteDatabase mDatabase;

    private EquipmentPreservativeTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new EquipmentPreservativeTable(database);
        }
    }

    /***
     * @return instance
     */
    public static EquipmentPreservativeTable getInstance() {
        return mInstance;
    }

    public void insertallEquipmentPreservatives(ArrayList<EquipmentPreservative> equipmentPreservatives) {

        for (EquipmentPreservative equipmentPreservative : equipmentPreservatives) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_EQUIPMENT_PRESERVATIVE_ID, equipmentPreservative.equipmentPreservativeId);
            values.put(DbConstants.KEY_EQUIPMENT_PRESERVATIVE_DESCRIPTION, equipmentPreservative.preservativeDescription);
            values.put(DbConstants.KEY_EQUIPMENT_ID, equipmentPreservative.equipmentId);
            values.put(DbConstants.KEY_PRESERVATIVE_ID, equipmentPreservative.preservativeId);
            values.put(DbConstants.KEY_QUANTITY_REQUIRE, equipmentPreservative.quantityRequire);
            values.put(DbConstants.KEY_PROCEDURE_DETAIL_ID, equipmentPreservative.procedureDetailId);
            values.put(DbConstants.KEY_PRESERVATION_START_DATE, equipmentPreservative.preservationStartDate);
            values.put(DbConstants.KEY_PRESERVATION_END_DATE, equipmentPreservative.preservationEndDate);

            if (equipmentPreservative.preservativeTypeDto != null) {
                values.put(DbConstants.KEY_PRESERVATION_TYPE, ((EquipmentPreservative.PreservativeType) equipmentPreservative.preservativeTypeDto).preservationType);
                values.put(DbConstants.KEY_PRESERVATION_CATEGORY, ((EquipmentPreservative.PreservativeType) equipmentPreservative.preservativeTypeDto).preservationCategory);
            }

            mDatabase.insert(DbConstants.TABLE_EQUIPMENT_PRESERVATIVE, null, values);
        }
    }


    public String getAllEquipmentsforPreservative(Integer preservativeId) {

        String equipmentIds = "";
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT_PRESERVATIVE + " WHERE " + DbConstants.KEY_PRESERVATIVE_ID + "=" + preservativeId;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Integer equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));

            if (!(cursor.getPosition() == cursor.getCount() - 1)) {
                equipmentIds = equipmentIds + equipmentId + ",";
            } else {
                equipmentIds = equipmentIds + equipmentId;
            }
            cursor.moveToNext();
        }
        return equipmentIds;
    }

    public ArrayList<EquipmentPreservative> getEquipmentPreservatives(Integer preservativeId, String equipmentIds) {

        ArrayList<EquipmentPreservative> equipmentPreservativeList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT_PRESERVATIVE + " WHERE " + DbConstants.KEY_PRESERVATIVE_ID + "=" + preservativeId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            EquipmentPreservative equipmentPreservative = new EquipmentPreservative();
            equipmentPreservative.equipmentPreservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_PRESERVATIVE_ID));
            equipmentPreservative.quantityRequire = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_QUANTITY_REQUIRE));
            equipmentPreservative.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));
            equipmentPreservative.preservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ID));
            equipmentPreservative.preservativeDescription = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_PRESERVATIVE_DESCRIPTION));

            equipmentPreservativeList.add(equipmentPreservative);
            cursor.moveToNext();
        }
        return equipmentPreservativeList;
    }


    public Float getEquipmentPreservativeQuantity(Integer aConsumableorPreservativeId, Integer equipmentId) {

        Float aQuantityrequire = Float.parseFloat("0");
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT_PRESERVATIVE + " WHERE "
                + DbConstants.KEY_PRESERVATIVE_ID + "=" + aConsumableorPreservativeId + " AND "
                + DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            aQuantityrequire = Float.parseFloat(cursor.getString(cursor.getColumnIndex(DbConstants.KEY_QUANTITY_REQUIRE)));
            cursor.moveToNext();
        }
        return aQuantityrequire;
    }
}
