package io.designdots.devapreservations.database.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import io.designdots.devapreservations.database.sqlite.tables.ConsumableAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.database.sqlite.tables.EngineerTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentPreservativeTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianAssignmentTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;

public class DbManager {

    private static DbManager mInstance;
    private static SQLiteDatabase mDatabase;
    private static DbHelper mDbHelper;

    private DbManager() {
    }

    public static void init(Context context) {
        if (mInstance == null) {
            mInstance = new DbManager();
            mDbHelper = DbHelper.getInstance(context);
        }
    }

    public static DbManager getInstance() {
        return mInstance;
    }

    public static void open() {
        if (mDbHelper != null) {
            mDatabase = mDbHelper.getWritableDatabase();

            // Initialise tables here
            EngineerTable.init(mDatabase);
            TechnicianTable.init(mDatabase);
            EquipmentTable.init(mDatabase);
            ConsumableTable.init(mDatabase);
            PreservativeTable.init(mDatabase);
            ReturnableToolTable.init(mDatabase);
            EquipmentPreservativeTable.init(mDatabase);
            ConsumableAllocationTable.init(mDatabase);
            PreservativeAllocationTable.init(mDatabase);
            ReturnableToolAllocationTable.init(mDatabase);
            TechnicianAssignmentTable.init(mDatabase);


        }
    }

    public static void close() {
        if (mDatabase != null) {
            mDbHelper.close();
        }
    }

}



