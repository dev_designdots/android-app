package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import io.designdots.devapreservations.database.sqlite.DbConstants;

public class EquipmentTechnicianAssignmentTable {
    private static String TAG = EquipmentTechnicianAssignmentTable.class.getCanonicalName();

    private static EquipmentTechnicianAssignmentTable mInstance;
    private static SQLiteDatabase mDatabase;

    private EquipmentTechnicianAssignmentTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new EquipmentTechnicianAssignmentTable(database);
        }
    }

    /***
     * @return instance
     */
    public static EquipmentTechnicianAssignmentTable getInstance() {
        return mInstance;
    }


    public void assignTechniciantoEquipment(Integer equipmentId, ArrayList<Integer> technicanIds) {

        for (Integer techId : technicanIds) {

            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_EQUIPMENT_ID, equipmentId);
            values.put(DbConstants.KEY_TECHNICIAN_ID, techId);
            values.put(DbConstants.KEY_CREATED_DATE_TIME, "" + System.currentTimeMillis());

            mDatabase.insert(DbConstants.TABLE_EQUIPMENT_TECHNICAN_ASSIGNMENT, null, values);
        }
    }

}
