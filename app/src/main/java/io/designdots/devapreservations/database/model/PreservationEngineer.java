package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class PreservationEngineer {

    @SerializedName("partyKey")
    public Integer partyKey;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("surName")
    public String surName;

    @SerializedName("userName")
    public String userName;

    @SerializedName("role")
    public String role;
}
