package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class PreservativeAllocation {

    @SerializedName("preservativeAllocKey")
    public Integer preservativeAllocKey;

    @SerializedName("preservativeId")
    public Integer preservativeId;

    @SerializedName("equipments")
    public String equipments;

    @SerializedName("quantity")
    public Float quantity;

    @SerializedName("engineerId")
    public Integer engineerId;

    @SerializedName("technicianId")
    public Integer technicianId;

    @SerializedName("createdDateTime")
    public String createdDateTime;


}
