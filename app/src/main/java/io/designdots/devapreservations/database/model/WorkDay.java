package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class WorkDay {

    @SerializedName("dayDtl")
    public String dayDtl;

    @SerializedName("totalWorkHrs")
    public String totalWorkHrs;

}
