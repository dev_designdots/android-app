package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.model.TechnicianGroup;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class TechnicianTable {

    private static String TAG = TechnicianTable.class.getCanonicalName();

    private static TechnicianTable mInstance;
    private static SQLiteDatabase mDatabase;

    private TechnicianTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new TechnicianTable(database);
        }
    }

    /***
     * @return instance
     */
    public static TechnicianTable getInstance() {
        return mInstance;
    }


    public void insertallTechnicians(ArrayList<Technician> technicians) {

        for (Technician aTechnician : technicians) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_PARTY_KEY, aTechnician.partyKey);
            values.put(DbConstants.KEY_FIRST_NAME, aTechnician.firstName);
            values.put(DbConstants.KEY_SURNAME, aTechnician.surName);
            values.put(DbConstants.KEY_USERNAME, aTechnician.userName);
            values.put(DbConstants.KEY_ROLE, aTechnician.role);

            mDatabase.insert(DbConstants.TABLE_TECHNICIAN, null, values);
        }
    }


    public ArrayList<Technician> getTechnicians(Integer aEngineerId) {

        String aTechnicianIds = TechnicianAssignmentTable.getInstance().getTechniciansUnderEngineer(aEngineerId);

        ArrayList<Technician> technicians = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN + " WHERE "
                + DbConstants.KEY_PARTY_KEY + " IN (" + aTechnicianIds + ")";
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Technician technician = new Technician();
            technician.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            technician.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            technician.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            technician.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            technician.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));
            technician.groupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
          //  technician.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));

            technicians.add(technician);

            cursor.moveToNext();
        }


        return technicians;
    }


    public Technician getAllocatedTechnician(Integer aTechnicianId) {
        Technician technician = null;
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN + " WHERE "
                + DbConstants.KEY_PARTY_KEY + " IN (" + aTechnicianId + ")";
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            technician = new Technician();
            technician.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            technician.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            technician.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            technician.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            technician.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));
            technician.groupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
            technician.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));


            cursor.moveToNext();
        }
        return technician;
    }

    public Integer getMaxGroupId() {
        Integer maxGroupId = -1;

        Cursor cursor = null;
        String sql = "SELECT MAX (" + DbConstants.KEY_GROUP_ID + ") AS " + DbConstants.KEY_MAX_GROUP_ID + " FROM " + DbConstants.TABLE_TECHNICIAN;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            maxGroupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_MAX_GROUP_ID));
            cursor.moveToNext();
        }


        return maxGroupId;
    }


    public void assignTechnicianstoEquipments(ArrayList<Integer> technicianIds, Integer
            aEquipmentId) {
        for (Integer techId : technicianIds) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_EQUIPMENT_ID, aEquipmentId);

            mDatabase.update(DbConstants.TABLE_TECHNICIAN, values, DbConstants.KEY_PARTY_KEY + "=" + techId, null);

        }
    }


    public void assignTechnicianstoGroup(ArrayList<Integer> technicianIds, Integer aGroupId) {

        for (Integer techId : technicianIds) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_GROUP_ID, aGroupId);

            mDatabase.update(DbConstants.TABLE_TECHNICIAN, values, DbConstants.KEY_PARTY_KEY + "=" + techId, null);
        }
    }

    public void removeTechniciansFromGroup(ArrayList<Integer> technicianIds) {
        for (Integer technicanId : technicianIds) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_GROUP_ID, -1);

            mDatabase.update(DbConstants.TABLE_TECHNICIAN, values, DbConstants.KEY_PARTY_KEY + "=" + technicanId, null);
        }

    }


    public ArrayList<TechnicianGroup> getTechnicianGroups() {
        ArrayList<TechnicianGroup> techniciangroupsList = new ArrayList<>();
        Cursor cursor = null;

        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN + " ORDER BY "
                + DbConstants.KEY_GROUP_ID;

        cursor = mDatabase.rawQuery(sql, null);

        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        int aDefaultGroupId = -1;

        Integer aGroupId = aDefaultGroupId;

        ArrayList<Technician> aTechnicians = new ArrayList<>();
        TechnicianGroup technicianGroup = new TechnicianGroup();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {


            if (cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID)) != aDefaultGroupId) {

                if (cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID)) == aGroupId) {

                    Technician technician = new Technician();

                    technician.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
                    technician.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
                    technician.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
                    technician.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
                    technician.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));
                    technician.groupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
                    technician.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));

                    aTechnicians.add(technician);

                    cursor.moveToNext();

                } else {

                    if (aTechnicians.size() > 0) {
                        technicianGroup.technicians = aTechnicians;
                        techniciangroupsList.add(technicianGroup);
                    }

                    aGroupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
                    technicianGroup = new TechnicianGroup();
                    technicianGroup.groupId = aGroupId;
                    aTechnicians = new ArrayList<>();

                }
            } else {
                cursor.moveToNext();
            }
        }
        if(aTechnicians.size()>0){
            technicianGroup.technicians = aTechnicians;
            techniciangroupsList.add(technicianGroup);
        }

        return techniciangroupsList;
    }


    public ArrayList<Technician> getAllTechniciansWithoutGroup() {
        ArrayList<Technician> technicians = new ArrayList<>();
        Cursor cursor = null;

        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN + " WHERE "
                + DbConstants.KEY_GROUP_ID + "=" + -1;

        cursor = mDatabase.rawQuery(sql, null);

        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Technician technician = new Technician();
            technician.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            technician.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            technician.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            technician.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            technician.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));
            technician.groupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
            technician.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));

            technicians.add(technician);

            cursor.moveToNext();
        }
        return technicians;
    }

    public ArrayList<Technician> getAllTechniciansUnderGroup(Integer aGroupId) {
        ArrayList<Technician> technicians = new ArrayList<>();
        Cursor cursor = null;

        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN + " WHERE "
                + DbConstants.KEY_GROUP_ID + "=" + aGroupId;

        cursor = mDatabase.rawQuery(sql, null);

        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Technician technician = new Technician();
            technician.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            technician.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            technician.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            technician.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            technician.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));
            technician.groupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
            technician.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));

            technicians.add(technician);

            cursor.moveToNext();
        }
        return technicians;
    }

    public ArrayList<Technician> getAllTechnicians() {

        ArrayList<Technician> technicians = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_TECHNICIAN;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Technician technician = new Technician();
            technician.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            technician.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            technician.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            technician.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            technician.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));
            technician.groupId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_GROUP_ID));
            technician.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));

            technicians.add(technician);

            cursor.moveToNext();
        }


        return technicians;


    }
}
