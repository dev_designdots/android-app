package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class EquipmentTable {

    private static String TAG = EquipmentTable.class.getCanonicalName();

    private static EquipmentTable mInstance;
    private static SQLiteDatabase mDatabase;

    private EquipmentTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new EquipmentTable(database);
        }
    }

    /***
     * @return instance
     */
    public static EquipmentTable getInstance() {
        return mInstance;
    }


    public void insertAllEquipments(ArrayList<Equipment> equipmentList) {

        for (Equipment equipment : equipmentList) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_EQUIPMENT_ID, equipment.equipmentId);
            values.put(DbConstants.KEY_EQUIPMENT_NAME, equipment.equipmentName);
            values.put(DbConstants.KEY_EQUIPMENT_CATEGORY_NAME, equipment.equipmentCategoryName);
            values.put(DbConstants.KEY_LOCATION, equipment.location);
            values.put(DbConstants.KEY_TAG_NUMBER, equipment.tagNumber);
            values.put(DbConstants.KEY_ZONE, equipment.zone);
            values.put(DbConstants.KEY_CONTENT_WHEN_FILLED, equipment.contentWhenFilled);
            values.put(DbConstants.KEY_WATER_DIESEL_OIL_BASED, equipment.waterOrDiselOilBased);
            values.put(DbConstants.KEY_CONTENT_VOLUME, equipment.contentVolume);
            values.put(DbConstants.KEY_EMPTY_VOLUME, equipment.emptyVolumeM3);

            if (equipment.preservationEngineer != null)
                values.put(DbConstants.KEY_PRESERVATION_ENGINEER, new Gson().toJson(equipment.preservationEngineer));
//
//            if (equipment.equipmentPreservatives != null)

                mDatabase.insert(DbConstants.TABLE_EQUIPMENT, null, values);

        }


    }


    public ArrayList<Equipment> getEquipmentsforPreservativeAllocation(Integer aPreservativeId, Integer aEngineerId) {

        String aEquipmentIds = EquipmentPreservativeTable.getInstance().getAllEquipmentsforPreservative(aPreservativeId);

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT
                + " WHERE " + DbConstants.KEY_EQUIPMENT_ID + " IN (" + aEquipmentIds + ")";

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        ArrayList<Equipment> aEquipmentList = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            String pe = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_PRESERVATION_ENGINEER));
            PreservationEngineer preservationEngineer = new Gson().fromJson(pe, PreservationEngineer.class);

            if (preservationEngineer.partyKey.equals(aEngineerId)) {
                Equipment equipment = new Equipment();

                equipment.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));
                equipment.equipmentName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_NAME));
                equipment.equipmentCategoryName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_CATEGORY_NAME));
                equipment.tagNumber = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TAG_NUMBER));

                aEquipmentList.add(equipment);

            }

            cursor.moveToNext();
        }

        return aEquipmentList;
    }

    public ArrayList<Equipment> getFinalEquipmentsListforPreservativeAllocation(Integer aConsumableorPreservativeId, ArrayList<Integer> equipmentIds) {

        String equipmentIdStr = "";
        for (int i = 0; i < equipmentIds.size(); i++) {
            if (i == equipmentIds.size() - 1) {
                equipmentIdStr = equipmentIdStr + equipmentIds.get(i);
            } else {
                equipmentIdStr = equipmentIdStr + equipmentIds.get(i) + ",";
            }
        }

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT
                + " WHERE " + DbConstants.KEY_EQUIPMENT_ID + " IN (" + equipmentIdStr + ")";

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        ArrayList<Equipment> aEquipmentList = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Equipment equipment = new Equipment();

            equipment.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));
            equipment.equipmentName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_NAME));
            equipment.equipmentCategoryName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_CATEGORY_NAME));
            equipment.tagNumber = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TAG_NUMBER));
            equipment.allocatedQuantity = EquipmentPreservativeTable.getInstance().getEquipmentPreservativeQuantity(aConsumableorPreservativeId, equipment.equipmentId);

            aEquipmentList.add(equipment);
            cursor.moveToNext();
        }

        return aEquipmentList;
    }


    public ArrayList<Equipment> getEquipmentsUnderEngineer(Integer aEngineerId) {
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        ArrayList<Equipment> aEquipmentList = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            String pe = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_PRESERVATION_ENGINEER));
            PreservationEngineer preservationEngineer = new Gson().fromJson(pe, PreservationEngineer.class);

            if (preservationEngineer.partyKey.equals(aEngineerId)) {
                Equipment equipment = new Equipment();

                equipment.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));
                equipment.equipmentName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_NAME));
                equipment.equipmentCategoryName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_CATEGORY_NAME));
                equipment.tagNumber = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TAG_NUMBER));

                aEquipmentList.add(equipment);

            }

            cursor.moveToNext();
        }

        return aEquipmentList;


    }


    public ArrayList<Equipment> getFinalEquipmentsListforAllocation(ArrayList<Integer> equipmentIds) {
        String equipmentIdStr = "";
        for (int i = 0; i < equipmentIds.size(); i++) {
            if (i == equipmentIds.size() - 1) {
                equipmentIdStr = equipmentIdStr + equipmentIds.get(i);
            } else {
                equipmentIdStr = equipmentIdStr + equipmentIds.get(i) + ",";
            }
        }

        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT
                + " WHERE " + DbConstants.KEY_EQUIPMENT_ID + " IN (" + equipmentIdStr + ")";

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        ArrayList<Equipment> aEquipmentList = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Equipment equipment = new Equipment();

            equipment.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));
            equipment.equipmentName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_NAME));
            equipment.equipmentCategoryName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_CATEGORY_NAME));
            equipment.tagNumber = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TAG_NUMBER));


            aEquipmentList.add(equipment);
            cursor.moveToNext();
        }

        return aEquipmentList;
    }


    //


    public ArrayList<Equipment> getAllEquipmentsWithStatus(Integer status) {
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_EQUIPMENT
                + " WHERE " + DbConstants.KEY_EQUIPMENT_STATUS + "=" + status;

        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        ArrayList<Equipment> aEquipmentList = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            Equipment equipment = new Equipment();

            equipment.equipmentId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_ID));
            equipment.equipmentName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_NAME));
            equipment.equipmentCategoryName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_CATEGORY_NAME));
            equipment.tagNumber = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_TAG_NUMBER));
            equipment.location = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_LOCATION));
            equipment.zone = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ZONE));
            equipment.contentWhenFilled = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CONTENT_WHEN_FILLED));
            equipment.waterOrDiselOilBased = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_WATER_DIESEL_OIL_BASED));
            equipment.contentVolume = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_CONTENT_VOLUME));
            equipment.emptyVolumeM3 = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_EMPTY_VOLUME));

            equipment.beforeImage = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BEFORE_IMAGE));
            equipment.beforeImageDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_BEFORE_IMAGE_DATE));
            equipment.duringImage = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DURING_IMAGE));
            equipment.duringImageDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_DURING_IMAGE_DATE));
            equipment.afterImage = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_AFTER_IMAGE));
            equipment.afterImageDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_AFTER_IMAGE_DATE));
            equipment.internalPercentageCompletion = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_INTERNAL_PERCENTAGE_COMPLETION));
            equipment.externalPercentageCompletion = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EXTERNAL_PERCENTAGE_COMPLETION));
            equipment.totalPercentageCompletion = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TOTAL_PERCENTAGE_COMPLETION));

            equipment.isCompatibilityTestRequired = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_IS_COMPATIBILITY_TEST_REQUIRED));
            equipment.compatibilityTestReason = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_COMPATIBILITY_TEST_REASON));
            equipment.percentageOfAddition = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PERCENTAGE_OF_ADDITION));
            equipment.compatibilityTestStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_COMPATIBILITY_TEST_STATUS));
            equipment.compatibilityTestBeforePhoto = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_COMPATIBILITY_TEST_BEFORE_PHOTO));
            equipment.compatibilityTestBeforePhotoDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_COMPATIBILITY_TEST_BEFORE_PHOTO_DATE));
            equipment.compatiblityTestAfterPhoto = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_COMPATIBILITY_TEST_AFTER_PHOTO));
            equipment.compatibilityTestAfterPhotoDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_COMPATIBILITY_TEST_AFTER_PHOTO_DATE));

            equipment.isCMCAdded = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_IS_CMC_ADDED));
            equipment.cmcDetails = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CMC_DETAILS));
            equipment.cmcInstalledDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CMC_INSTALLED_DATE));
            equipment.cmcBeforePhoto = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CMC_BEFORE_PHOTO));
            equipment.cmcBeforePhotoDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CMC_BEFORE_PHOTO_DATE));
            equipment.cmcAfterPhoto = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CMC_AFTER_PHOTO));
            equipment.cmcAfterPhotoDateTime = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_CMC_AFTER_PHOTO_DATE));

            equipment.equipmentStatus = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENT_STATUS));

            aEquipmentList.add(equipment);
            cursor.moveToNext();

        }

        return aEquipmentList;
    }


    public void updateEquipmentStatus(Integer equipmentId, Integer aStatus) {

        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_EQUIPMENT_STATUS, aStatus);

        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);
    }


    public void updateBeforePhoto(Integer equipmentId, String BeforePhoto) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_BEFORE_IMAGE, BeforePhoto);
        values.put(DbConstants.KEY_BEFORE_IMAGE_DATE, "" + System.currentTimeMillis());


        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);

    }

    public void updateDuringPhoto(Integer equipmentId, String DuringPhoto) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_DURING_IMAGE, DuringPhoto);
        values.put(DbConstants.KEY_DURING_IMAGE_DATE, "" + System.currentTimeMillis());

        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);

    }

    public void updateAfterPhoto(Integer equipmentId, String AfterPhoto) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_AFTER_IMAGE, AfterPhoto);
        values.put(DbConstants.KEY_AFTER_IMAGE_DATE, "" + System.currentTimeMillis());


        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);

    }


    public void updatepercentageCompletion(Integer equipmentId, Integer internal, Integer external, Integer total) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_INTERNAL_PERCENTAGE_COMPLETION, internal);
        values.put(DbConstants.KEY_EXTERNAL_PERCENTAGE_COMPLETION, external);
        values.put(DbConstants.KEY_TOTAL_PERCENTAGE_COMPLETION, total);


        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);

    }


    public void updatecompatibilityTestNotRequired(Integer equipmentId, String reason) {

        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_IS_COMPATIBILITY_TEST_REQUIRED, DbConstants.TEST_NOT_REQUIRED);
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_REASON, reason);


        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);


    }

    public void updateCompatibilityTestDetails(Integer equipmentId, Integer percentageAdded, String BeforePhoto, String BeforeDate, String AfterPhoto, String AfterDate) {
        ContentValues values = new ContentValues();
        
        values.put(DbConstants.KEY_IS_COMPATIBILITY_TEST_REQUIRED, DbConstants.TEST_REQUIRED);
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_STATUS, DbConstants.TEST_STARTED);
        values.put(DbConstants.KEY_PERCENTAGE_OF_ADDITION, percentageAdded);
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_BEFORE_PHOTO, BeforePhoto);
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_BEFORE_PHOTO_DATE, BeforeDate);
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_AFTER_PHOTO, AfterPhoto);
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_AFTER_PHOTO_DATE, AfterDate);

        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);

    }

    public void updateCompatibilityTestStatusCompleted(Integer equipmentId) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_COMPATIBILITY_TEST_STATUS, DbConstants.TEST_COMPLETED);

        mDatabase.update(DbConstants.TABLE_EQUIPMENT, values, DbConstants.KEY_EQUIPMENT_ID + "=" + equipmentId, null);

    }


}
