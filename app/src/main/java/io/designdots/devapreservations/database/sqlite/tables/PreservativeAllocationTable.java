package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.PreservativeAllocation;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class PreservativeAllocationTable {

    private static String TAG = PreservativeAllocationTable.class.getCanonicalName();

    private static PreservativeAllocationTable mInstance;
    private static SQLiteDatabase mDatabase;

    private PreservativeAllocationTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new PreservativeAllocationTable(database);
        }
    }

    /***
     * @return instance
     */
    public static PreservativeAllocationTable getInstance() {
        return mInstance;
    }


    public void allocatePreservative(PreservativeAllocation alloc) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.KEY_PRESERVATIVE_ID, alloc.preservativeId);
        values.put(DbConstants.KEY_QUANTITY, alloc.quantity);
        values.put(DbConstants.KEY_EQUIPMENTS, alloc.equipments);
        values.put(DbConstants.KEY_ENGINEER_ID, alloc.engineerId);
        values.put(DbConstants.KEY_TECHNICIAN_ID, alloc.technicianId);
        values.put(DbConstants.KEY_CREATED_DATE_TIME, alloc.createdDateTime);

        mDatabase.insert(DbConstants.TABLE_PRESERVATIVE_ALLOCATION, null, values);


    }


    public ArrayList<PreservativeAllocation> getAllocatedDataforPreservative(Integer preservativeId) {

        ArrayList<PreservativeAllocation> allocationDetailsList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATIVE_ALLOCATION + " WHERE " + DbConstants.KEY_PRESERVATIVE_ID + "=" + preservativeId + " ORDER BY " + DbConstants.KEY_CREATED_DATE_TIME;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PreservativeAllocation allocation = new PreservativeAllocation();
            allocation.preservativeAllocKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ALLOC_KEY));
            allocation.preservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ID));
            allocation.equipments = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENTS));
            allocation.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            allocation.engineerId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_ENGINEER_ID));
            allocation.technicianId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));

            allocationDetailsList.add(allocation);
            cursor.moveToNext();
        }
        return allocationDetailsList;
    }

    public Float getAllocatedPreservativeQuantity(Integer preservativeId) {

        Float allocatedQty = null;
        Cursor cursor = null;
        String sql = "SELECT SUM (" + DbConstants.KEY_QUANTITY + ") AS TOTAL  FROM " + DbConstants.TABLE_PRESERVATIVE_ALLOCATION + " WHERE " + DbConstants.KEY_PRESERVATIVE_ID + "=" + preservativeId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            allocatedQty = cursor.getFloat(cursor.getColumnIndex("TOTAL"));
            cursor.moveToNext();
        }

        if (allocatedQty != null)
            return allocatedQty;
        else return Float.parseFloat("0");
    }

    public PreservativeAllocation getAllocation(Integer aPreservativeAllocationKey) {
        PreservativeAllocation allocation = new PreservativeAllocation();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATIVE_ALLOCATION + " WHERE " + DbConstants.KEY_PRESERVATIVE_ALLOC_KEY + "=" + aPreservativeAllocationKey;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            allocation.preservativeAllocKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ALLOC_KEY));
            allocation.preservativeId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PRESERVATIVE_ID));
            allocation.equipments = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_EQUIPMENTS));
            allocation.quantity = cursor.getFloat(cursor.getColumnIndex(DbConstants.KEY_QUANTITY));
            allocation.engineerId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_ENGINEER_ID));
            allocation.technicianId = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_TECHNICIAN_ID));

            cursor.moveToNext();
        }
        return allocation;
    }


}
