package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class Technician {

    @SerializedName("partyKey")
    public Integer partyKey;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("surName")
    public String surName;

    @SerializedName("userName")
    public String userName;

    @SerializedName("role")
    public String role;

    public Integer groupId;

    public Integer equipmentId;


}
