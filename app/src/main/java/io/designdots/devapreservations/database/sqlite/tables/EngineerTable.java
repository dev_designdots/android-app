package io.designdots.devapreservations.database.sqlite.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import io.designdots.devapreservations.BuildConfig;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.sqlite.DbConstants;

public class EngineerTable {

    private static String TAG = EngineerTable.class.getCanonicalName();

    private static EngineerTable mInstance;
    private static SQLiteDatabase mDatabase;

    private EngineerTable(SQLiteDatabase database) {
        mDatabase = database;
    }

    /***
     * Initializing the database instance
     *
     * @param database
     */
    public static void init(SQLiteDatabase database) {
        if (mInstance == null) {
            mInstance = new EngineerTable(database);
        }
    }

    /***
     * @return instance
     */
    public static EngineerTable getInstance() {
        return mInstance;
    }

    public void insertallEngineers(ArrayList<PreservationEngineer> engineerList) {

        for (PreservationEngineer aEngineer : engineerList) {
            ContentValues values = new ContentValues();
            values.put(DbConstants.KEY_PARTY_KEY, aEngineer.partyKey);
            values.put(DbConstants.KEY_FIRST_NAME, aEngineer.firstName);
            values.put(DbConstants.KEY_SURNAME, aEngineer.surName);
            values.put(DbConstants.KEY_USERNAME, aEngineer.userName);
            values.put(DbConstants.KEY_ROLE, aEngineer.role);

            mDatabase.insert(DbConstants.TABLE_PRESERVATION_ENGINEER, null, values);
        }
    }


    public ArrayList<PreservationEngineer> getPreservationEngineers() {
        ArrayList<PreservationEngineer> aEngineersList = new ArrayList<>();
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATION_ENGINEER;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            PreservationEngineer engineer = new PreservationEngineer();
            engineer.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            engineer.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            engineer.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            engineer.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            engineer.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));

            aEngineersList.add(engineer);

            cursor.moveToNext();
        }

        return aEngineersList;
    }


    public PreservationEngineer getAllocatedEngineer(Integer aEngineerId) {
        PreservationEngineer engineer = null;
        Cursor cursor = null;
        String sql = "SELECT * FROM " + DbConstants.TABLE_PRESERVATION_ENGINEER + " WHERE " + DbConstants.KEY_PARTY_KEY + "=" + aEngineerId;
        cursor = mDatabase.rawQuery(sql, null);
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Cursor count ===" + cursor.getCount());
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            engineer = new PreservationEngineer();
            engineer.partyKey = cursor.getInt(cursor.getColumnIndex(DbConstants.KEY_PARTY_KEY));
            engineer.firstName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_FIRST_NAME));
            engineer.surName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_SURNAME));
            engineer.userName = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_USERNAME));
            engineer.role = cursor.getString(cursor.getColumnIndex(DbConstants.KEY_ROLE));


            cursor.moveToNext();
        }

        return engineer;
    }
}