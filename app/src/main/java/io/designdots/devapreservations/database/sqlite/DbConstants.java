package io.designdots.devapreservations.database.sqlite;

public class DbConstants {

    public static final Integer NOT_RECEIVED = 3;
    public static final Integer PARTIALLY_RECEIVED = 2;
    public static final Integer COMPLETELY_RECEIVED = 1;
    public static final Integer RETURNED = 4;

    public static final Integer UNASSIGNED = 0;
    public static final Integer ASSIGNED = 1;
    public static final Integer PRESERVATION_STARTED = 2;
    public static final Integer PRESERVATION_COMPLETED = 3;
    public static final Integer INSPECTION_STARTED = 4;
    public static final Integer INSPECTION_COMPLETED = 5;

    public static final Integer TEST_NOT_STARTED = 0;
    public static final Integer TEST_STARTED = 1;
    public static final Integer TEST_COMPLETED = 2;
    public static final Integer TEST_NOT_REQUIRED = 0;
    public static final Integer TEST_REQUIRED = 1;

    public static final Integer CMC_ADDED = 1;
    public static final Integer CMC_NOT_ADDED = 0;


    public static final Integer REUSABLE = 0;
    public static final Integer NON_REUSABLE = 1;

    public static final String TABLE_CONSUMABLE = "Consumable";
    public static final String TABLE_EQUIPMENT = "Equipment";
    public static final String TABLE_PRESERVATIVE = "Preservative";
    public static final String TABLE_RETURNABLE_TOOL = "ReturnableTool";
    public static final String TABLE_TECHNICIAN = "Technician";
    public static final String TABLE_PRESERVATION_ENGINEER = "PreservationEngineer";
    public static final String TABLE_PRESERVATIVE_ALLOCATION = "PreservativeAllocation";
    public static final String TABLE_CONSUMABLE_ALLOCATION = "ConsumableAllocation";
    public static final String TABLE_RETURNABLE_TOOL_ALLOCATION = "ReturnableToolAllocation";
    public static final String TABLE_TECHNICIAN_ASSIGNMENT = "TechnicianAssignment";
    public static final String TABLE_EQUIPMENT_PRESERVATIVE = "EquipmentPreservative";
    public static final String TABLE_EQUIPMENT_TECHNICAN_ASSIGNMENT = "EquipmentTechnicianAssignment";

    public static final String KEY_CONSUMABLEID = "consumableId";
    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_RECEIVED_QUANTITY = "receivedQuantity";
    public static final String KEY_RECEIPT_STATUS = "receiptStatus";
    public static final String KEY_CONSUMABLE_NAME = "consumableName";
    public static final String KEY_UOM = "unitOfMeasure";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_EQUIPMENT_ID = "equipmentId";
    public static final String KEY_EQUIPMENT_CATEGORY_NAME = "equipmentCategoryName";
    public static final String KEY_EQUIPMENT_NAME = "equipmentName";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_TAG_NUMBER = "tagNumber";
    public static final String KEY_ZONE = "zone";
    public static final String KEY_CONTENT_WHEN_FILLED = "contentWhenFilled";
    public static final String KEY_WATER_DIESEL_OIL_BASED = "waterOrDiselOilBased";
    public static final String KEY_EMPTY_VOLUME = "emptyVolumeM3";
    public static final String KEY_CONTENT_VOLUME = "contentVolume";
    public static final String KEY_PRESERVATION_ENGINEER = "preservationEngineer";
    public static final String KEY_PARTY_KEY = "partyKey";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_SURNAME = "surName";
    public static final String KEY_USERNAME = "userName";
    public static final String KEY_ROLE = "role";
    public static final String KEY_PRESERVATIVE_ID = "preservativeId";
    public static final String KEY_CHEMICAL_ID = "chemicalId";
    public static final String KEY_CHEMICAL_NAME = "chemicalName";
    public static final String KEY_RETURNABLE_TOOL_ID = "returnableToolId";
    public static final String KEY_BRAND = "brand";
    public static final String KEY_MODEL = "model";
    public static final String KEY_RETURN_REASON = "returnReason";
    public static final String KEY_TOOL_ID = "toolId";
    public static final String KEY_TOOL_NAME = "toolName";
    public static final String KEY_CONSUMABLE_ALLOC_KEY = "consumableAllocKey";
    public static final String KEY_EQUIPMENTS = "equipments";
    public static final String KEY_CREATED_DATE_TIME = "createdDateTime";
    public static final String KEY_PRESERVATIVE_ALLOC_KEY = "preservativeAllocKey";
    public static final String KEY_RETURNABLE_TOOL_ALLOC_KEY = "toolAllocKey";
    public static final String KEY_ISSUED_DATETIME = "issuedDateTime";
    public static final String KEY_RETURNED_DATETIME = "returnedDateTime";
    public static final String KEY_WORK_HOURS = "workHours";
    public static final String KEY_RETURN_STATUS = "returnStatus";
    public static final String KEY_ENGINEER_TECHNICIAN_ASSIGNMENT_ID = "engineerTechnicianAssignmentId";
    public static final String KEY_ENGINEER_ID = "engineerId";
    public static final String KEY_TECHNICIAN_ID = "technicianId";
    public static final String KEY_QUANTITY_REQUIRE = "quantityRequire";
    public static final String KEY_EQUIPMENT_PRESERVATIVE_ID = "equipmentPreservativeId";
    public static final String KEY_EQUIPMENT_PRESERVATIVE_DESCRIPTION = "preservativeDescription";
    public static final String KEY_GROUP_ID = "groupId";
    public static final String KEY_MAX_GROUP_ID = "maxGroupId";
    public static final String KEY_INTERNAL_PERCENTAGE_COMPLETION = "internalPercentageCompletion";
    public static final String KEY_EXTERNAL_PERCENTAGE_COMPLETION = "externalPercentageCompletion";
    public static final String KEY_TOTAL_PERCENTAGE_COMPLETION = "totalPercentageCompletion";
    public static final String KEY_PRESERVATION_START_DATE = "preservationStartDate";
    public static final String KEY_PRESERVATION_END_DATE = "preservationEndDate";
    public static final String KEY_PROCEDURE_DETAIL_ID = "procedureDetailId";
    public static final String KEY_PRESERVATION_TYPE = "preservationType";
    public static final String KEY_PRESERVATION_CATEGORY = "preservationCategory";
    public static final String KEY_BEFORE_IMAGE = "beforeImage";
    public static final String KEY_BEFORE_IMAGE_DATE = "beforeImageDateTime";
    public static final String KEY_DURING_IMAGE = "duringImage";
    public static final String KEY_DURING_IMAGE_DATE = "duringImageDateTime";
    public static final String KEY_AFTER_IMAGE = "afterImage";
    public static final String KEY_AFTER_IMAGE_DATE = "afterImageDateTime";
    public static final String KEY_IS_COMPATIBILITY_TEST_REQUIRED = "isCompatibilityTestRequired";
    public static final String KEY_COMPATIBILITY_TEST_REASON = "compatibilityTestReason";
    public static final String KEY_PERCENTAGE_OF_ADDITION = "percentageOfAddition";
    public static final String KEY_COMPATIBILITY_TEST_STATUS = "compatibilityTestStatus";
    public static final String KEY_COMPATIBILITY_TEST_BEFORE_PHOTO = "compatibilityTestBeforePhoto";
    public static final String KEY_COMPATIBILITY_TEST_BEFORE_PHOTO_DATE = "compatibilityTestBeforePhotoDateTime";
    public static final String KEY_COMPATIBILITY_TEST_AFTER_PHOTO = "compatiblityTestAfterPhoto";
    public static final String KEY_COMPATIBILITY_TEST_AFTER_PHOTO_DATE = "compatibilityTestAfterPhotoDateTime";
    public static final String KEY_TEST_START_TIME = "testStartDateTime";
    public static final String KEY_TEST_END_TIME = "testEndDateTime";
    public static final String KEY_IS_CMC_ADDED = "isCMCAdded";
    public static final String KEY_CMC_DETAILS = "cmcDetails";
    public static final String KEY_CMC_INSTALLED_DATE = "cmcInstalledDateTime";
    public static final String KEY_CMC_BEFORE_PHOTO = "cmcBeforePhoto";
    public static final String KEY_CMC_BEFORE_PHOTO_DATE = "cmcBeforePhotoDateTime";
    public static final String KEY_CMC_AFTER_PHOTO = "cmcAfterPhoto";
    public static final String KEY_CMC_AFTER_PHOTO_DATE = "cmcAfterPhotoDateTime";
    public static final String KEY_EQUIPMENT_STATUS = "equipmentStatus";


}
