package io.designdots.devapreservations.database.model;

import com.google.gson.annotations.SerializedName;

public class ReturnableTool {

    @SerializedName("returnableToolId")
    public Integer returnableToolId;

    @SerializedName("quantity")
    public Integer quantity;

    @SerializedName("receivedQuantity")
    public Integer receivedQuantity;

    @SerializedName("receiptStatus")
    public Integer receiptStatus;

    @SerializedName("brand")
    public String brand;

    @SerializedName("description")
    public String description;

    @SerializedName("model")
    public String model;

    @SerializedName("returnReason")
    public String returnReason;

    @SerializedName("toolId")
    public String toolId;

    @SerializedName("toolName")
    public String toolName;

    @SerializedName("unitOfMeasure")
    public String unitOfMeasure;
}
