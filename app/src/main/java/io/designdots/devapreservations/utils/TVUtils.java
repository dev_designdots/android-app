package io.designdots.devapreservations.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import io.designdots.devapreservations.R;

public class TVUtils {

    public static void setTextColor(Context ctx, TextView atv, int acolorid) {
        atv.setTextColor(ContextCompat.getColor(ctx, acolorid));
    }

    public static void setTextViewBold(TextView atv) {
        Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);
        atv.setTypeface(boldTypeface);
    }
}
