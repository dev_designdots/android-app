package io.designdots.devapreservations.utils;

public class Constants {

    public static String TENSPACE_SEPARATOR = "            ";
    public static String FIVESPACE_SEPARATOR = "     ";
    public static String FIFTEENSPACE_SEPARATOR = "               ";

    public static final String ALLOCATION = "Allocation";
    public static final String GROUP_OPERATION = "GroupOperation";
    public static final String ADD_NEW = "addNew";
    public static final String EDIT = "edit";
    public static final String PRESERVATIVE_ALLOCATION = "preservative";
    public static final String CONSUMABLE_ALLOCATION = "consumable";
    public static final String TOOL_ALLOCATION = "tool";

    public static final String TOOLS_LIST = "ToolsList";


    public static final String DATE_FORMAT = "dd-MM-yyyy hh:mm:ss a";


    public static final int CAMERA_REQUEST = 1888;
    public static final int CAMERA_PERMISSION_CODE = 100;

}
