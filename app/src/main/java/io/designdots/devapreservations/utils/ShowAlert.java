package io.designdots.devapreservations.utils;

import android.app.Dialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import io.designdots.devapreservations.R;

public class ShowAlert {


    public static void showDialog(AppCompatActivity activity, String title, String msg, String okbuttontext) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_message_only);
        TextView titleTXT = (TextView) dialog.findViewById(R.id.dialog_title_TXT);
        TextView msgTXT = (TextView) dialog.findViewById(R.id.dialog_message_TXT);
        titleTXT.setText(title);
        msgTXT.setText(msg);
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_ok_BTN);
        dialogButton.setText(okbuttontext);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    public static void showSnackMsg(AppCompatActivity mctx, String msg, int bgcolor, int txtcolor) {


        TSnackbar snackbar = TSnackbar.make(mctx.findViewById(android.R.id.content), msg, TSnackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(mctx.getResources().getColor(bgcolor));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(mctx.getResources().getColor(txtcolor));
        snackbar.show();


    }


}



