package io.designdots.devapreservations.modules.userroles.preservationengineer.myequipments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

public class PEPreservationStatusUpdate extends BaseAppcompactActivity {

    private EditText mInternalEDT, mExternalEDT;
    private TextView mTotalCompletionTXT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pepreservation_status_update);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
        setListeners();
    }


    private void initialiseViewsAndObjects() {
        mInternalEDT = findViewById(R.id.pe_preservation_status_internal_EDT);
        mExternalEDT = findViewById(R.id.pe_preservation_status_external_EDT);
        mTotalCompletionTXT = findViewById(R.id.pe_preservation_status_total_TXT);
    }

    private void setListeners() {
        mInternalEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int internal = 0;
                int external = 0;


                if (mInternalEDT.getText().toString().length() > 0) {
                    internal = Integer.parseInt(mInternalEDT.getText().toString());
                }

                if (mExternalEDT.getText().toString().length() > 0) {
                    external = Integer.parseInt(mExternalEDT.getText().toString());
                }

                int aTotalPercent = (internal + external) / 2;

                mTotalCompletionTXT.setText(aTotalPercent + "%");

            }
        });

        mExternalEDT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int internal = 0;
                int external = 0;


                if (mInternalEDT.getText().toString().length() > 0) {
                    internal = Integer.parseInt(mInternalEDT.getText().toString());
                }

                if (mExternalEDT.getText().toString().length() > 0) {
                    external = Integer.parseInt(mExternalEDT.getText().toString());
                }

                int aTotalPercent = (internal + external) / 2;

                mTotalCompletionTXT.setText(aTotalPercent + "%");


            }
        });
    }

    public void onSubmitButtonClicked(View view) {

    }
}
