package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.model.ConsumableAllocation;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.model.PreservativeAllocation;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.database.sqlite.tables.EngineerTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.AppUtils;
import io.designdots.devapreservations.utils.Constants;

public class MCMCConsumptionDetails extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private TextView mEquipmentsTXT, mTotalQtyAllocatedTXT, mPreservationEngineerTXT, mTechnicianTXT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmc_consumptiondetails);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCMCConsumptionDetails.this;

        mEquipmentsTXT = findViewById(R.id.mc_mc_consumption_equipments_TXT);
        mTotalQtyAllocatedTXT = findViewById(R.id.mc_mc_consumption_allocated_qty_TXT);
        mPreservationEngineerTXT = findViewById(R.id.mc_mc_consumption_details_engineer_name_TXT);
        mTechnicianTXT = findViewById(R.id.mc_mc_consumption_technician_name_TXT);

        Bundle aBundle = getIntent().getExtras();

        if (aBundle.getString(Constants.ALLOCATION).equals(Constants.PRESERVATIVE_ALLOCATION)) {
            Integer aPreservativeId, aPreservativeAllocationKey;

            aPreservativeId = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ID);
            aPreservativeAllocationKey = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ALLOC_KEY);

            Preservative preservative = PreservativeTable.getInstance().getAllocatedPreservative(aPreservativeId);
            PreservativeAllocation allocation = PreservativeAllocationTable.getInstance().getAllocation(aPreservativeAllocationKey);

            getSupportActionBar().setTitle("Consumption Details");

            String createdDateTime = allocation.createdDateTime;
            if (createdDateTime != null && createdDateTime.length() > 0)
                getSupportActionBar().setSubtitle(AppUtils.getDate(Long.parseLong(allocation.createdDateTime), Constants.DATE_FORMAT));

            String[] equipmentsarray = allocation.equipments.split(",");
            ArrayList<Integer> equipmentIds = new ArrayList<>();
            for (int i = 0; i < equipmentsarray.length; i++) {
                equipmentIds.add(Integer.parseInt(equipmentsarray[i]));
            }

            ArrayList<Equipment> aEquipmentsList = EquipmentTable.getInstance().getFinalEquipmentsListforPreservativeAllocation(aPreservativeId, equipmentIds);

            String aEquipmentTag = "";
            for (int i = 0; i < aEquipmentsList.size(); i++) {
                Equipment e = aEquipmentsList.get(i);
                aEquipmentTag = aEquipmentTag + e.tagNumber + "\n";
            }

            mEquipmentsTXT.setText(aEquipmentTag);

            String allocatedQty = allocation.quantity + " " + preservative.unitOfMeasure;

            mTotalQtyAllocatedTXT.setText(allocatedQty);

            PreservationEngineer engineer = EngineerTable.getInstance().getAllocatedEngineer(allocation.engineerId);
            Technician technician = TechnicianTable.getInstance().getAllocatedTechnician(allocation.technicianId);

            String enggName = engineer.firstName + " " + engineer.surName;
            String techName = technician.firstName + " " + technician.surName;
            mPreservationEngineerTXT.setText(enggName);
            mTechnicianTXT.setText(techName);


        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.CONSUMABLE_ALLOCATION)) {

            Integer aConsumableId, aConsumableAllocationKey;

            aConsumableId = aBundle.getInt(DbConstants.KEY_CONSUMABLEID);
            aConsumableAllocationKey = aBundle.getInt(DbConstants.KEY_CONSUMABLE_ALLOC_KEY);

            Consumable consumable = ConsumableTable.getInstance().getAllocatedConsumable(aConsumableId);
            ConsumableAllocation allocation = ConsumableAllocationTable.getInstance().getAllocation(aConsumableAllocationKey);



            getSupportActionBar().setTitle("Consumption Details");

            String createdDateTime = allocation.createdDateTime;
            if (createdDateTime != null && createdDateTime.length() > 0)
                getSupportActionBar().setSubtitle(AppUtils.getDate(Long.parseLong(allocation.createdDateTime), Constants.DATE_FORMAT));

            String[] equipmentsarray = allocation.equipments.split(",");
            ArrayList<Integer> equipmentIds = new ArrayList<>();
            for (int i = 0; i < equipmentsarray.length; i++) {
                equipmentIds.add(Integer.parseInt(equipmentsarray[i]));
            }

            ArrayList<Equipment> aEquipmentsList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            String aEquipmentTag = "";
            for (int i = 0; i < aEquipmentsList.size(); i++) {
                Equipment e = aEquipmentsList.get(i);
                aEquipmentTag = aEquipmentTag + e.tagNumber + "\n";
            }

            mEquipmentsTXT.setText(aEquipmentTag);

            String allocatedQty = allocation.quantity + " " + consumable.unitOfMeasure;

            mTotalQtyAllocatedTXT.setText(allocatedQty);

            PreservationEngineer engineer = EngineerTable.getInstance().getAllocatedEngineer(allocation.engineerId);
            Technician technician = TechnicianTable.getInstance().getAllocatedTechnician(allocation.technicianId);

            String enggName = engineer.firstName + " " + engineer.surName;
            String techName = technician.firstName + " " + technician.surName;
            mPreservationEngineerTXT.setText(enggName);
            mTechnicianTXT.setText(techName);


        }


    }


}
