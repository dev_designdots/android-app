package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;

public class MCMCReturnableToolsList extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcreturnable_tools_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });


        return true;
    }


    private void initialiseViewsAndObjects() {
        mCtx = MCMCReturnableToolsList.this;
        mLV = findViewById(R.id.mc_mc_returnable_tools_LV);
        mAdapter = new CustomListAdapter(ReturnableToolTable.getInstance().getDistinctTools());
        mLV.setAdapter(mAdapter);

        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                ReturnableTool tool = (ReturnableTool) adapterView.getItemAtPosition(pos);

                Intent i = new Intent(mCtx, MCMCReturnableToolItemsList.class);
                i.putExtra(DbConstants.KEY_TOOL_NAME, tool.toolName);
                startActivity(i);

            }
        });


    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;

        private ArrayList<ReturnableTool> mReturnableToolsList;
        private ArrayList<ReturnableTool> mFilteredList;

        public CustomListAdapter(ArrayList<ReturnableTool> distinctTools) {

            inflater = mCtx.getLayoutInflater();
            this.mReturnableToolsList = distinctTools;
            mFilteredList = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mReturnableToolsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mReturnableToolsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_returnable_tool_item, null);
                holder.mtoolName = (TextView) convertView.findViewById(R.id.row_mc_rs_returnabletool_name_TXT);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ReturnableTool tool = mReturnableToolsList.get(pos);

            String toolname = tool.toolName + " (" + tool.receivedQuantity + "/" + tool.quantity + ")";

            holder.mtoolName.setText(toolname);


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<ReturnableTool> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mReturnableToolsList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final ReturnableTool g : mFilteredList) {
                                if (g.toolName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mReturnableToolsList = (ArrayList<ReturnableTool>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mtoolName;
        }
    }

}
