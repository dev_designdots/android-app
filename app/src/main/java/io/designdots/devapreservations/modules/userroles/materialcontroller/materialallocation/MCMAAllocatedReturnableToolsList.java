package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.Dialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.ScrollingTabContainerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.model.ReturnableToolAllocation;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.EngineerTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.AppUtils;
import io.designdots.devapreservations.utils.Constants;

public class MCMAAllocatedReturnableToolsList extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mCustomListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaallocated_returnable_tools_list);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }


    private void initialiseViewsAndObjects() {
        mCtx = MCMAAllocatedReturnableToolsList.this;
        mLV = findViewById(R.id.mc_ma_allocated_tools_LV);


        ArrayList<String> aparsedToolsList = getIntent().getStringArrayListExtra(Constants.TOOLS_LIST);
        ArrayList<ReturnableTool> aToolsList = new ArrayList<>();
        Gson aGson = new Gson();

        for (String astr : aparsedToolsList) {
            aToolsList.add(aGson.fromJson(astr, ReturnableTool.class));
        }

        ArrayList<ReturnableToolAllocation> allocationDetailsList = ReturnableToolAllocationTable.getInstance().getAllocatedToolsDetails(aToolsList);


        mCustomListAdapter = new CustomListAdapter(aToolsList, allocationDetailsList);
        mLV.setAdapter(mCustomListAdapter);

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<ReturnableTool> mReturnableToolsList;
        ArrayList<ReturnableToolAllocation> mAllocationList;

        public CustomListAdapter(ArrayList<ReturnableTool> returnableToolsList, ArrayList<ReturnableToolAllocation> allocationsList) {
            inflater = mCtx.getLayoutInflater();
            this.mReturnableToolsList = returnableToolsList;
            this.mAllocationList = allocationsList;
        }

        @Override
        public int getCount() {
            return mReturnableToolsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return null;
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_ma_allocated_tool_item, null);
                holder.mToolNameTXT = (TextView) convertView.findViewById(R.id.mc_ma_allocated_returnable_tool_name_TXT);
                holder.mToolAllocatedEngineerTXT = convertView.findViewById(R.id.mc_ma_allocated_returnable_tool_engineer_TXT);
                holder.mToolAllocatedTechnicianTXT = convertView.findViewById(R.id.mc_ma_allocated_returnable_tool_technician_TXT);
                holder.mToolAllocatedDateTimeTXT = convertView.findViewById(R.id.mc_ma_allocated_returnable_tool_datetime_TXT);
                holder.mReturnToolMenuIMGBTN = convertView.findViewById(R.id.mc_ma_allocated_returnable_tool_more_menu);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ReturnableTool returnableTool = mReturnableToolsList.get(pos);
            holder.mToolNameTXT.setText(returnableTool.toolId);


            for (final ReturnableToolAllocation allocation : mAllocationList) {
                if (allocation.toolId.equals(returnableTool.returnableToolId)) {
                    PreservationEngineer engg = EngineerTable.getInstance().getAllocatedEngineer(allocation.engineerId);
                    Technician tech = TechnicianTable.getInstance().getAllocatedTechnician(allocation.technicianId);

                    String aEngineerName = engg.firstName + " " + engg.surName;
                    String aTechnicianName = tech.firstName + " " + tech.surName;
                    holder.mToolAllocatedDateTimeTXT.setText(AppUtils.getDate(Long.parseLong(allocation.issuedDateTime), Constants.DATE_FORMAT));
                    holder.mToolAllocatedEngineerTXT.setText(aEngineerName);
                    holder.mToolAllocatedTechnicianTXT.setText(aTechnicianName);


                    if (returnableTool.receiptStatus == DbConstants.RETURNED) {
                        holder.mReturnToolMenuIMGBTN.setVisibility(View.GONE);
                    } else {
                        holder.mReturnToolMenuIMGBTN.setVisibility(View.VISIBLE);


                        holder.mReturnToolMenuIMGBTN.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                PopupMenu popup = new PopupMenu(mCtx, view);
                                popup.getMenuInflater().inflate(R.menu.menu_mc_received_list,
                                        popup.getMenu());
                                popup.show();

                                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {

                                        switch (item.getItemId()) {

                                            case R.id.action_return_tool:

                                                final Dialog dialog = new Dialog(mCtx);
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                dialog.setCancelable(false);
                                                dialog.setContentView(R.layout.dialog_return_tool);

                                                final Button aSubmitBTN, aCancelBTN;
                                                aSubmitBTN = dialog.findViewById(R.id.dialog_return_tool_submit_BTN);
                                                aCancelBTN = dialog.findViewById(R.id.dialog_return_tool_cancel_BTN);

                                                TextView aToolId = dialog.findViewById(R.id.dialog_return_tool_id_TXT);
                                                aToolId.setText(returnableTool.toolId);
                                                final Spinner aReturnStatus = dialog.findViewById(R.id.dialog_return_tool_status_SPN);

                                                aSubmitBTN.setEnabled(false);

                                                aReturnStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                                        switch (i) {
                                                            case 0:
                                                                aSubmitBTN.setEnabled(false);

                                                                break;

                                                            case 1:

                                                                aSubmitBTN.setEnabled(true);

                                                                break;

                                                            case 2:

                                                                aSubmitBTN.setEnabled(true);

                                                                break;
                                                        }

                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> adapterView) {


                                                    }
                                                });


                                                aCancelBTN.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                                aSubmitBTN.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {

                                                        mReturnableToolsList.get(pos).receiptStatus = DbConstants.RETURNED;

                                                        allocation.returnStatus = aReturnStatus.getSelectedItemPosition();
                                                        allocation.returnedDateTime = "" + SystemClock.currentThreadTimeMillis();

                                                        ReturnableToolTable.getInstance().updateTools(mReturnableToolsList);
                                                        ReturnableToolAllocationTable.getInstance().updateToolAllocation(allocation);

                                                        notifyDataSetChanged();

                                                    }
                                                });


                                                break;
                                        }
                                        return true;
                                    }
                                });


                            }
                        });

                    }
                }
            }

            return convertView;
        }

        @Override
        public Filter getFilter() {
            return null;
        }


        class ViewHolder {

            TextView mToolNameTXT, mToolAllocatedEngineerTXT, mToolAllocatedTechnicianTXT, mToolAllocatedDateTimeTXT;
            ImageButton mReturnToolMenuIMGBTN;
        }
    }


}
