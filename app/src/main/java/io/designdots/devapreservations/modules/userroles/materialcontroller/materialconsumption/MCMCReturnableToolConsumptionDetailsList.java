package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.ReturnableToolAllocation;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.utils.AppUtils;
import io.designdots.devapreservations.utils.Constants;

public class MCMCReturnableToolConsumptionDetailsList extends AppCompatActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcreturnable_tool_consumption_details_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initialiseViewsAndObjects() {
        try {
            mCtx = MCMCReturnableToolConsumptionDetailsList.this;
            mLV = findViewById(R.id.mc_mc_returnable_tool_consumption_details_LV);

            final Integer aReturnabletoolId = getIntent().getIntExtra(DbConstants.KEY_RETURNABLE_TOOL_ID, 0);

            final String aToolIdName = getIntent().getStringExtra(DbConstants.KEY_TOOL_ID);
            ArrayList<ReturnableToolAllocation> allocationList = ReturnableToolAllocationTable.getInstance().getAllocationDetails(aReturnabletoolId);

            mAdapter = new CustomListAdapter(allocationList);
            mLV.setAdapter(mAdapter);

            mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                    Intent i = new Intent(mCtx, MCMCReturnableToolLife.class);
                    i.putExtra(DbConstants.KEY_RETURNABLE_TOOL_ID, aReturnabletoolId);
                    ReturnableToolAllocation alloc = (ReturnableToolAllocation) mAdapter.getItem(pos);
                    Gson gson = new Gson();
                    i.putExtra(DbConstants.KEY_RETURNABLE_TOOL_ALLOC_KEY, gson.toJson(alloc));
                    i.putExtra(DbConstants.KEY_TOOL_ID, aToolIdName);
                    startActivity(i);

                }
            });

            getSupportActionBar().setTitle(aToolIdName);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<ReturnableToolAllocation> mAllocationDetailsList;

        public CustomListAdapter(ArrayList<ReturnableToolAllocation> allocationsList) {

            inflater = mCtx.getLayoutInflater();

            this.mAllocationDetailsList = allocationsList;
        }

        @Override
        public int getCount() {
            return mAllocationDetailsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mAllocationDetailsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mAllocationDetailsList.get(pos).toolAllocKey;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_mc_tool_consumption_details, null);
                holder.mTagNo = convertView.findViewById(R.id.row_mc_mc_tool_consumption_details_equipment_tag_no_TXT);
                holder.mWorkHrs = convertView.findViewById(R.id.row_mc_mc_tool_consumption_details_work_hours_TXT);
                holder.mReturnStatus = convertView.findViewById(R.id.row_mc_mc_tool_consumption_details_return_status_TXT);
                holder.mAssignedOn = convertView.findViewById(R.id.row_mc_mc_tool_consumption_details_assigned_on_TXT);
                holder.mReturnedOn = convertView.findViewById(R.id.row_mc_mc_tool_consumption_details_returned_on_TXT);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ReturnableToolAllocation allocation = mAllocationDetailsList.get(pos);


            String[] equipmentsarray = allocation.equipments.split(",");
            ArrayList<Integer> equipmentIds = new ArrayList<>();
            for (int i = 0; i < equipmentsarray.length; i++) {
                equipmentIds.add(Integer.parseInt(equipmentsarray[i]));
            }

            ArrayList<Equipment> equipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            String aEquipmentTag = "";
            for (int i = 0; i < equipmentList.size(); i++) {

                Equipment e = equipmentList.get(i);
                aEquipmentTag = aEquipmentTag + e.tagNumber;
                if (i != 0 && ((i + 1) % 3) == 0) {
                    aEquipmentTag = aEquipmentTag + "\n";
                } else {
                    aEquipmentTag = aEquipmentTag + Constants.TENSPACE_SEPARATOR;
                }

            }
            holder.mTagNo.setText(aEquipmentTag);

            holder.mWorkHrs.setText("40 Hrs");

            if (allocation.returnStatus != null) {
                if (allocation.returnStatus == 1) {
                    holder.mReturnStatus.setText("Reusable");
                } else if (allocation.returnStatus == 2) {
                    holder.mReturnStatus.setText("Non-Reusable");
                }
            }


            if (allocation.issuedDateTime != null) {
                String assignedOn = "Assigned On\n" + AppUtils.getDate(Long.parseLong("" + allocation.issuedDateTime), Constants.DATE_FORMAT);
                holder.mAssignedOn.setText(assignedOn);
            }

            if (allocation.returnedDateTime != null) {
                String returnedOn = "Returned On\n" + AppUtils.getDate(Long.parseLong("" + allocation.returnedDateTime), Constants.DATE_FORMAT);
                holder.mAssignedOn.setText(returnedOn);
            }


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return null;
        }


        class ViewHolder {

            TextView mTagNo, mWorkHrs, mReturnStatus, mAssignedOn, mReturnedOn;

        }
    }

}
