package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;
import io.designdots.devapreservations.utils.TVUtils;

public class MCMAQuantitySummaryList extends AppCompatActivity {


    private AppCompatActivity mCtx;
    private ListView mLV;
    private EditText mQuantityEDT;
    private TextView mUnitTXT, mToolNamesTXT;
    private Button mConfirmBTN;
    private CustomListAdapter mAdapter;
    private String mUOM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setContentView(R.layout.activity_mcmaquantity_summary_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMAQuantitySummaryList.this;
        mQuantityEDT = findViewById(R.id.mc_ma_allocation_summary_qty_EDT);
        mUnitTXT = findViewById(R.id.mc_ma_allocation_summary_uom_TXT);
        mToolNamesTXT = findViewById(R.id.mc_ma_allocation_summary_tool_names_TXT);
        mConfirmBTN = findViewById(R.id.mc_ma_confirm_allocation_BTN);
        mLV = findViewById(R.id.mc_ma_allocation_summary_LV);


        Integer aEngineerId, aTechnicianId, aConsumableorPreservativeId;

        ArrayList<Integer> equipmentIds;

        Bundle aBundle = getIntent().getExtras();
        aEngineerId = aBundle.getInt(DbConstants.KEY_ENGINEER_ID);
        aTechnicianId = aBundle.getInt(DbConstants.KEY_TECHNICIAN_ID);
        mUOM = aBundle.getString(DbConstants.KEY_UOM);
        equipmentIds = aBundle.getIntegerArrayList(DbConstants.KEY_EQUIPMENT_ID);

        if (aBundle.getString(Constants.ALLOCATION).equals(Constants.PRESERVATIVE_ALLOCATION)) {

            setTitle("Preservative Qty");

            aConsumableorPreservativeId = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ID);
            mQuantityEDT.setEnabled(false);

            ArrayList<Equipment> aEquipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforPreservativeAllocation(aConsumableorPreservativeId, equipmentIds);


            Float sum = Float.valueOf(0);
            for (Equipment e : aEquipmentList) {
                sum = sum + e.allocatedQuantity;
            }
            mQuantityEDT.setText("" + sum);


            mAdapter = new CustomListAdapter(aEquipmentList, true);


        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.CONSUMABLE_ALLOCATION)) {

            setTitle("Consumable Qty");

            mQuantityEDT.setEnabled(true);
            mUnitTXT.setText(mUOM);

            ArrayList<Equipment> aEquipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            mAdapter = new CustomListAdapter(aEquipmentList, false);
        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.TOOL_ALLOCATION)) {

            ArrayList<ReturnableTool> aToolsList =
                    ReturnableToolTable.getInstance().getFinalReturnableToolsforAllocation(aBundle.getIntegerArrayList(DbConstants.KEY_RETURNABLE_TOOL_ID));

            mQuantityEDT.setEnabled(false);
            mQuantityEDT.setText("" + aToolsList.size());
            mUnitTXT.setText(mUOM);
            setTitle(aToolsList.get(0).toolName);

            String aToolIdNames = "Tool ID\n";
            for (ReturnableTool tool : aToolsList) {
                aToolIdNames = aToolIdNames + tool.toolId + "\n";
            }

            mToolNamesTXT.setVisibility(View.VISIBLE);
            mToolNamesTXT.setText(aToolIdNames);


            ArrayList<Equipment> aEquipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            mAdapter = new CustomListAdapter(aEquipmentList, false);
        }


        mUnitTXT.setText(mUOM);


        mLV.setAdapter(mAdapter);


        mConfirmBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle b = getIntent().getExtras();


                if (mQuantityEDT.getText().toString().trim().length() > 0) {

                        b.putString(DbConstants.KEY_QUANTITY, mQuantityEDT.getText().toString());


                    Intent i = new Intent(mCtx, MCMAConfirmAllocation.class);
                    i.putExtras(b);
                    startActivity(i);
                } else {

                    ShowAlert.showSnackMsg(mCtx, "Please enter the allocated quantity.", R.color.colorRed, R.color.colorPaleWhite);
                }

            }
        });


    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<Equipment> mEquipmentsList;
        Boolean isQuantityEnabled;

        public CustomListAdapter(ArrayList<Equipment> equipmentsList, Boolean aQuantityAvailable) {

            inflater = mCtx.getLayoutInflater();
            this.mEquipmentsList = equipmentsList;
            this.isQuantityEnabled = aQuantityAvailable;
        }

        @Override
        public int getCount() {
            return mEquipmentsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mEquipmentsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mEquipmentsList.get(pos).equipmentId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_material_quantity_list_item, null);
                holder.mTextOne = (TextView) convertView.findViewById(R.id.qty_list_item_TXT_one);
                holder.mTextTwo = (TextView) convertView.findViewById(R.id.qty_list_item_TXT_two);
                holder.mTextCount = (TextView) convertView.findViewById(R.id.qty_list_item_count_TXT);
                holder.mTextName = (TextView) convertView.findViewById(R.id.qty_list_item_name_TXT);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Equipment equipment = mEquipmentsList.get(pos);

            holder.mTextCount.setText("" + (pos + 1));
            TVUtils.setTextColor(mCtx, holder.mTextCount, R.color.colorBlack);

            holder.mTextName.setText(equipment.equipmentName);
            TVUtils.setTextColor(mCtx, holder.mTextName, R.color.colorBlack);

            holder.mTextOne.setText(equipment.tagNumber);
            TVUtils.setTextColor(mCtx, holder.mTextOne, R.color.colorGrey);

            if (isQuantityEnabled) {
                holder.mTextTwo.setText(equipment.allocatedQuantity + " " + mUOM);
                TVUtils.setTextColor(mCtx, holder.mTextTwo, R.color.colorBlack);
            } else {
                holder.mTextTwo.setText("");
            }


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return null;
        }


        class ViewHolder {

            TextView mTextOne, mTextTwo, mTextName, mTextCount;

        }
    }


}
