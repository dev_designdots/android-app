package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.utils.Constants;

public class MCMCConsumablesList extends BaseAppcompactActivity {

    private BaseAppcompactActivity mCtx;
    private ListView mMCMCConsumablesLV;
    private CustomListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcconsumables_list);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        return true;
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMCConsumablesList.this;
        mMCMCConsumablesLV = findViewById(R.id.mc_mc_consumables_LV);
        mAdapter = new CustomListAdapter(ConsumableTable.getInstance().getAllConsumables());
        mMCMCConsumablesLV.setAdapter(mAdapter);


        mMCMCConsumablesLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                Intent i = new Intent(mCtx, MCMCConsumablesDetailsList.class);
                Bundle aBundle =new Bundle();
                Integer aConsumableId = (int)mAdapter.getItemId(pos);
                aBundle.putString(Constants.ALLOCATION, Constants.CONSUMABLE_ALLOCATION);
                aBundle.putInt(DbConstants.KEY_CONSUMABLEID,aConsumableId);
                i.putExtras(aBundle);
                startActivity(i);

            }
        });

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;

        ArrayList<Consumable> mConsumableList;
        ArrayList<Consumable> mFilteredList;

        public CustomListAdapter(ArrayList<Consumable> consumablesList) {
            inflater = mCtx.getLayoutInflater();
            this.mConsumableList = consumablesList;
            mFilteredList = new ArrayList<>();
        }


        @Override
        public int getCount() {
            return mConsumableList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mConsumableList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mConsumableList.get(pos).consumableId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {
            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_mc_consumable_list_item, null);

                holder.mTextOneTXT = convertView.findViewById(R.id.row_mc_mc_consumable_list_TXT_one);
                holder.mTextTwoTXT = convertView.findViewById(R.id.row_mc_mc_consumable_list_TXT_two);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Consumable consumable = mConsumableList.get(pos);
            String aConsumableName = consumable.consumableName;
            String aAllocatedQuantity = ConsumableAllocationTable.getInstance().getAllocatedConsumableQuantity(consumable.consumableId) + " " + consumable.unitOfMeasure;

            holder.mTextOneTXT.setText(aConsumableName);
            holder.mTextTwoTXT.setText(aAllocatedQuantity);


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Consumable> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mConsumableList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Consumable g : mFilteredList) {
                                if (g.consumableName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mConsumableList = (ArrayList<Consumable>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        class ViewHolder {

            TextView mTextOneTXT, mTextTwoTXT;

        }
    }
}
