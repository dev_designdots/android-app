package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;
import io.designdots.devapreservations.utils.TVUtils;

public class MCMAConsumablesList extends BaseAppcompactActivity {


    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;
    private int mSelectedpostion = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaconsumable_list);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        return true;
    }


    private void initialiseViewsAndObjects() {

        mCtx = MCMAConsumablesList.this;
        mLV = findViewById(R.id.mc_ma_consumable_LV);
        mAdapter = new CustomListAdapter(ConsumableTable.getInstance().getAllConsumables());
        mLV.setAdapter(mAdapter);


        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedpostion = i;
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    public void onNextButtonClicked(View view) {

        if (mSelectedpostion != -1) {

            Intent i = new Intent(mCtx, MCMAEngineerList.class);
            Bundle aBundle = new Bundle();
            aBundle.putInt(DbConstants.KEY_CONSUMABLEID, (int) mAdapter.getItemId(mSelectedpostion));
            aBundle.putString(DbConstants.KEY_UOM, ((Consumable) mAdapter.getItem(mSelectedpostion)).unitOfMeasure);
            aBundle.putString(Constants.ALLOCATION, Constants.CONSUMABLE_ALLOCATION);
            i.putExtras(aBundle);
            startActivity(i);


        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select a Consumable", R.color.colorRed, R.color.colorPaleWhite);
        }


    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        private ArrayList<Consumable> mConsumableList;
        private ArrayList<Consumable> mFilteredList;

        public CustomListAdapter(ArrayList<Consumable> consumableList) {

            inflater = mCtx.getLayoutInflater();
            this.mConsumableList = consumableList;
        }

        @Override
        public int getCount() {
            return mConsumableList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mConsumableList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mConsumableList.get(pos).consumableId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_material_list_item, null);
                holder.mTextOneTV = (TextView) convertView.findViewById(R.id.row_mc_material_list_TXT_one);
                holder.mTextTwoTV = (TextView) convertView.findViewById(R.id.row_mc_material_list_TXT_two);
                holder.mTextThreeTV = (TextView) convertView.findViewById(R.id.row_mc_material_list_TXT_three);
                holder.mRootLAY = convertView.findViewById(R.id.row_mc_material_list_root_LAY);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (pos == mSelectedpostion) {
                holder.mRootLAY.setBackgroundResource(R.drawable.bg_box_selected);
            } else {
                holder.mRootLAY.setBackgroundResource(R.drawable.bg_box);
            }

            Consumable consumable = mConsumableList.get(pos);


            holder.mTextOneTV.setText(consumable.consumableName);
            TVUtils.setTextColor(mCtx, holder.mTextOneTV, R.color.colorBlack);

            holder.mTextTwoTV.setText("Available Quantity");
            TVUtils.setTextColor(mCtx, holder.mTextTwoTV, R.color.colorGrey);

            holder.mTextThreeTV.setText(consumable.availableQuantity + "  " + consumable.unitOfMeasure);
            TVUtils.setTextColor(mCtx, holder.mTextThreeTV, R.color.colorBlack);

            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Consumable> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mConsumableList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Consumable g : mFilteredList) {
                                if (g.consumableName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mConsumableList = (ArrayList<Consumable>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mTextOneTV, mTextTwoTV, mTextThreeTV;
            LinearLayout mRootLAY;
        }
    }


}
