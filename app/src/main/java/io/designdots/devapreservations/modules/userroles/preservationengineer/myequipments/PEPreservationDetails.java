package io.designdots.devapreservations.modules.userroles.preservationengineer.myequipments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.utils.TVUtils;

public class PEPreservationDetails extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private RadioGroup mPreservationRGRP;
    private LinearLayout mBottomLAY, mCMCDetailsLAY;
    private Button mAddCMCBTN;
    private TextView mCMCDateTXT, mCMCNoTXT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pepreservation_details);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();

        setListeners();

    }

    private void setListeners() {

        mPreservationRGRP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                switch (checkedId) {

                    case R.id.pe_preservation_internal_RBTN:
                        mBottomLAY.setVisibility(View.VISIBLE);

                        break;


                    case R.id.pe_preservation_external_RBTN:
                        mBottomLAY.setVisibility(View.GONE);

                        break;
                }

            }
        });

        mAddCMCBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Show cmc dialog
            }
        });

    }

    private void initialiseViewsAndObjects() {
        mCtx = PEPreservationDetails.this;
        mLV = findViewById(R.id.pe_preservation_details_LV);
        mPreservationRGRP = findViewById(R.id.preservation_details_RGRP);
        mAddCMCBTN = findViewById(R.id.preservation_details_add_cmc_BTN);
        mBottomLAY = findViewById(R.id.pe_preservation_details_bottom_LAY);
        mCMCDetailsLAY = findViewById(R.id.preservation_details_cmc_details_LAY);
        mCMCDateTXT = findViewById(R.id.preservation_details_cmc_added_date_TXT);
        mCMCNoTXT = findViewById(R.id.preservation_details_cmc_no_TXT);

        mPreservationRGRP.check(R.id.pe_preservation_internal_RBTN);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;

        public CustomListAdapter() {

            inflater = mCtx.getLayoutInflater();
        }

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_preservation_details_list_item, null);

                holder.mPreservationCompletionTXT = convertView.findViewById(R.id.row_preservation_detail_completion_TXT);
                holder.mPreservationCategoryTXT = convertView.findViewById(R.id.row_preservation_detail_preservation_category_TXT);
                holder.mPreservativeNameTXT = convertView.findViewById(R.id.row_preservation_detail_preservative_name_TXT);
                holder.mPreservationProcedureTXT = convertView.findViewById(R.id.row_preservation_detail_preservation_procedure_TXT);
                holder.mPreservativeQtyTXT = convertView.findViewById(R.id.row_preservation_detail_preservative_qty_TXT);
                holder.mStartEndPreservationBTN = convertView.findViewById(R.id.row_preservation_details_start_end_BTN);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return null;
        }


        class ViewHolder {

            TextView mPreservationCompletionTXT, mPreservationCategoryTXT, mPreservativeNameTXT, mPreservationProcedureTXT, mPreservativeQtyTXT;

            Button mStartEndPreservationBTN;
        }
    }


}
