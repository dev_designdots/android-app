package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.model.ConsumableAllocation;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation.MCMAConsumablesList;
import io.designdots.devapreservations.utils.Constants;

public class MCMCConsumablesDetailsList extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcconsumables_details_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMCConsumablesDetailsList.this;
        mLV = findViewById(R.id.mc_mc_consumable_details_LV);
        Bundle aBundle = getIntent().getExtras();

        Integer consumableId = aBundle.getInt(DbConstants.KEY_CONSUMABLEID, 0);

        Consumable consumable = ConsumableTable.getInstance().getAllocatedConsumable(consumableId);
        setTitle(consumable.consumableName);

        ArrayList<ConsumableAllocation> consumableAllocations = ConsumableAllocationTable.getInstance().getAllocatedDataforConsumable(consumableId);
        mAdapter = new CustomListAdapter(consumableId, consumableAllocations);
        mLV.setAdapter(mAdapter);


        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Intent i = new Intent(mCtx, MCMCConsumptionDetails.class);
                Bundle aBundle = getIntent().getExtras();
                aBundle.putInt(DbConstants.KEY_CONSUMABLE_ALLOC_KEY,
                        ((ConsumableAllocation) mAdapter.getItem(pos)).consumableAllocKey);
                i.putExtras(aBundle);
                startActivity(i);
            }
        });


    }

    class CustomListAdapter extends BaseAdapter {

        LayoutInflater inflater;

        Integer mConsumableId;
        ArrayList<ConsumableAllocation> mAllocationsList;

        public CustomListAdapter(Integer consumableId, ArrayList<ConsumableAllocation> allocationList)

        {
            inflater = mCtx.getLayoutInflater();
            this.mConsumableId = consumableId;
            this.mAllocationsList = allocationList;
        }

        @Override
        public int getCount() {
            return mAllocationsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mAllocationsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mAllocationsList.get(pos).consumableId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_mc_preservative_details_item, null);
                holder.mAllocatedDateTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_date_TXT);
                holder.mAllocatedTimeTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_time_TXT);
                holder.mEquipmentTagNoTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_equipment_tag_no_TXT);
                holder.mAllocatedQtyTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_total_qty_allocated_TXT);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ConsumableAllocation allocation = mAllocationsList.get(pos);

            String[] equipmentsarray = allocation.equipments.split(",");
            ArrayList<Integer> equipmentIds = new ArrayList<>();
            for (int i = 0; i < equipmentsarray.length; i++) {
                equipmentIds.add(Integer.parseInt(equipmentsarray[i]));
            }

            ArrayList<Equipment> aEquipmentsList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            String aEquipmentTag = "";
            for (int i = 0; i < aEquipmentsList.size(); i++) {

                Equipment e = aEquipmentsList.get(i);
                aEquipmentTag = aEquipmentTag + e.tagNumber;
                if (i != 0 && ((i + 1) % 3) == 0) {
                    aEquipmentTag = aEquipmentTag + "\n";
                } else {
                    aEquipmentTag = aEquipmentTag + Constants.TENSPACE_SEPARATOR;
                }

            }
            holder.mEquipmentTagNoTXT.setText(aEquipmentTag);

            String aAllocatedQty = allocation.quantity + " " + ConsumableTable.getInstance().getAllocatedConsumable(mConsumableId).unitOfMeasure;
            holder.mAllocatedQtyTXT.setText(aAllocatedQty);


            return convertView;
        }

        class ViewHolder {

            TextView mEquipmentTagNoTXT, mAllocatedQtyTXT, mAllocatedDateTXT, mAllocatedTimeTXT;

        }
    }

}
