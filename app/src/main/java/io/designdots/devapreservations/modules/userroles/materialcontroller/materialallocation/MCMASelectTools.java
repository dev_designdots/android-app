package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.utils.Constants;

public class MCMASelectTools extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;
    private ArrayList<Integer> mSelectedToolIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaselect_tools);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });
        return true;
    }


    private void initialiseViewsAndObjects() {
        mCtx = MCMASelectTools.this;
        mLV = findViewById(R.id.mc_ma_select_tools_LV);

        ArrayList<String> aparsedToolsList = getIntent().getStringArrayListExtra(Constants.TOOLS_LIST);
        ArrayList<ReturnableTool> aToolsList = new ArrayList<>();
        Gson aGson = new Gson();

        for (String astr : aparsedToolsList) {
            aToolsList.add(aGson.fromJson(astr, ReturnableTool.class));
        }

        mAdapter = new CustomListAdapter(aToolsList);
        mLV.setAdapter(mAdapter);

    }

    public void onNextButtonClicked(View view) {

        Intent i = new Intent(mCtx, MCMAEngineerList.class);
        Bundle aBundle = new Bundle();
        aBundle.putIntegerArrayList(DbConstants.KEY_RETURNABLE_TOOL_ID, mSelectedToolIds);
        aBundle.putString(DbConstants.KEY_UOM, ((ReturnableTool) mAdapter.getItem(0)).unitOfMeasure);
        aBundle.putString(Constants.ALLOCATION, Constants.TOOL_ALLOCATION);
        i.putExtras(aBundle);
        startActivity(i);

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<ReturnableTool> mReturnableToolsList;
        ArrayList<ReturnableTool> mFilteredList;


        public CustomListAdapter(ArrayList<ReturnableTool> aToolsList) {
            inflater = mCtx.getLayoutInflater();
            mReturnableToolsList = aToolsList;
            mSelectedToolIds = new ArrayList<>();

        }

        @Override
        public int getCount() {
            return mReturnableToolsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mReturnableToolsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mReturnableToolsList.get(pos).returnableToolId;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_ma_tool_item, null);
                holder.mToolNameTXT = (TextView) convertView.findViewById(R.id.row_mc_ma_tool_name_TXT);
                holder.mToolSelectionCHBX = convertView.findViewById(R.id.row_mc_ma_tool_selection_CHBX);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ReturnableTool aTool = mReturnableToolsList.get(pos);

            String aToolName = aTool.toolId;
            holder.mToolNameTXT.setText(aToolName);


            holder.mToolSelectionCHBX.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((CheckBox) view).isChecked()) {

                        if (!mSelectedToolIds.contains(mReturnableToolsList.get(pos).returnableToolId)) {
                            mSelectedToolIds.add(mReturnableToolsList.get(pos).returnableToolId);
                        }
                    } else {
                        if (mSelectedToolIds.contains(mReturnableToolsList.get(pos).returnableToolId)) {
                            mSelectedToolIds.remove(mReturnableToolsList.get(pos).returnableToolId);
                        }
                    }
                }
            });


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<ReturnableTool> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mReturnableToolsList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final ReturnableTool g : mFilteredList) {
                                if (g.toolId.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mReturnableToolsList = (ArrayList<ReturnableTool>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mToolNameTXT;
            CheckBox mToolSelectionCHBX;
        }
    }


}
