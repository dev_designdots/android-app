package io.designdots.devapreservations.modules.launch;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.gson.Gson;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.database.sqlite.tables.EngineerTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentPreservativeTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianAssignmentTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.modules.userlogin.LoginActivity;
import io.designdots.devapreservations.utils.AppUtils;
import io.designdots.devapreservations.webservices.model.MaterialControllerDownload;

public class SplashActivity extends BaseAppcompactActivity {

    private static int SPLASH_TIME_OUT = 3000;

    long maxWorkingTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String aResponse = "";
        try {
            aResponse = AppUtils.AssetJSONFile("materialforward.json", getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }

        MaterialControllerDownload mat = new Gson().fromJson(aResponse, MaterialControllerDownload.class);

        EquipmentTable.getInstance().insertAllEquipments(mat.materialController.equipments);
        PreservativeTable.getInstance().insertAllPreservatives(mat.materialController.preservatives);
        ConsumableTable.getInstance().insertAllConsumables(mat.materialController.consumables);
        ReturnableToolTable.getInstance().insertAllReturnableTools(mat.materialController.returnableTools);
        EngineerTable.getInstance().insertallEngineers(mat.materialController.preservationEngineers);
        TechnicianTable.getInstance().insertallTechnicians(mat.materialController.technicians);
        TechnicianAssignmentTable.getInstance().insertAllTechnicianAssignments(mat.materialController.technicianAssignments);

        String myDate = "2018/09/03 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        maxWorkingTime = date.getTime();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (System.currentTimeMillis() < maxWorkingTime) {

                    // This method will be executed once the timer is over
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();

                }
            }
        }, SPLASH_TIME_OUT);
    }
}
