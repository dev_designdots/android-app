package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;

public class MCMAReturnableToolsList extends BaseAppcompactActivity {


    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;
    private int mSelectedpostion = -1;
    private RadioGroup mReturnableToolsRGRP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmareturnable_tools_list);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });


        return true;
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMAReturnableToolsList.this;
        mLV = findViewById(R.id.mc_ma_returnable_tools_LV);
        mReturnableToolsRGRP = findViewById(R.id.mc_ma_returnable_tools_RGRP);


        mReturnableToolsRGRP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int selectId) {

                switch (selectId) {

                    case R.id.mc_ma_returnable_tools_unallocated_RBTN:
                        mAdapter = new CustomListAdapter(ReturnableToolTable.getInstance().getUnAllocatedTools());
                        mLV.setAdapter(mAdapter);
                        break;

                    case R.id.mc_ma_returnable_tools_allocated_RBTN:
                        mAdapter = new CustomListAdapter(ReturnableToolTable.getInstance().getAllocatedTools());
                        mLV.setAdapter(mAdapter);
                        break;
                }


            }
        });

        mReturnableToolsRGRP.check(R.id.mc_ma_returnable_tools_unallocated_RBTN);


        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedpostion = i;
                mAdapter.notifyDataSetChanged();
            }
        });

    }

    @SuppressWarnings("unchecked")
    public void onNextButtonClicked(View view) {


        if (mSelectedpostion != -1) {

            ArrayList<ReturnableTool> aToolsList;
            aToolsList = (ArrayList<ReturnableTool>) mAdapter.getItem(mSelectedpostion);
            ArrayList<String> parsedToolsList = new ArrayList<>();
            Gson aGson = new Gson();
            for (ReturnableTool tool : aToolsList) {
                parsedToolsList.add(aGson.toJson(tool));
            }


            if (mReturnableToolsRGRP.getCheckedRadioButtonId() == R.id.mc_ma_returnable_tools_unallocated_RBTN) {


                Intent i = new Intent(mCtx, MCMASelectTools.class);
                i.putStringArrayListExtra(Constants.TOOLS_LIST, parsedToolsList);
                startActivity(i);

            } else {

                Intent i = new Intent(mCtx, MCMAAllocatedReturnableToolsList.class);
                i.putStringArrayListExtra(Constants.TOOLS_LIST, parsedToolsList);
                startActivity(i);
            }


        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select a Returnable Tool", R.color.colorRed, R.color.colorPaleWhite);
        }

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        HashMap<String, ArrayList<ReturnableTool>> mReturnableToolsMap;

        List<Object> mKeysList;
        List<Object> mFilteredList;


        public CustomListAdapter(HashMap<String, ArrayList<ReturnableTool>> returnableToolsMap) {

            inflater = mCtx.getLayoutInflater();
            this.mReturnableToolsMap = returnableToolsMap;

            mKeysList = Arrays.asList(mReturnableToolsMap.keySet().toArray());
        }

        @Override
        public int getCount() {
            return mKeysList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mReturnableToolsMap.get(mKeysList.get(pos).toString());
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {


                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_material_list_item, null);
                holder.mTextOneTV = (TextView) convertView.findViewById(R.id.row_mc_material_list_TXT_one);
                holder.mTextTwoTV = (TextView) convertView.findViewById(R.id.row_mc_material_list_TXT_two);
                holder.mTextThreeTV = (TextView) convertView.findViewById(R.id.row_mc_material_list_TXT_three);
                holder.mRootLAY = convertView.findViewById(R.id.row_mc_material_list_root_LAY);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            if (pos == mSelectedpostion) {
                holder.mRootLAY.setBackgroundResource(R.drawable.bg_box_selected);
            } else {
                holder.mRootLAY.setBackgroundResource(R.drawable.bg_box);
            }

            String aTextOne = mKeysList.get(pos).toString();

            String aTextTwo = "Available Quantity";

            String aTextThree = "" + (mReturnableToolsMap.get(mKeysList.get(pos).toString())).size();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                holder.mTextOneTV.setText(Html.fromHtml(aTextOne, Html.FROM_HTML_MODE_COMPACT));
                holder.mTextTwoTV.setText(Html.fromHtml(aTextTwo, Html.FROM_HTML_MODE_COMPACT));
                holder.mTextThreeTV.setText(Html.fromHtml(aTextThree, Html.FROM_HTML_MODE_COMPACT));

            } else {

                holder.mTextOneTV.setText(Html.fromHtml(aTextOne));
                holder.mTextTwoTV.setText(Html.fromHtml(aTextTwo));
                holder.mTextThreeTV.setText(Html.fromHtml(aTextThree));
            }


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Object> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mKeysList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Object g : mFilteredList) {
                                if (g.toString().toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mKeysList = (ArrayList<Object>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mTextOneTV, mTextTwoTV, mTextThreeTV;
            LinearLayout mRootLAY;
        }
    }


}
