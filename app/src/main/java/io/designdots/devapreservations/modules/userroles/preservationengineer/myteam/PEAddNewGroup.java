package io.designdots.devapreservations.modules.userroles.preservationengineer.myteam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.modules.userroles.preservationengineer.PELanding;
import io.designdots.devapreservations.utils.ShowAlert;

public class PEAddNewGroup extends BaseAppcompactActivity {


    private AppCompatActivity mCtx;
    TextView mGroupName, mGroupMembers;
    ArrayList<Integer> mSelectedTechnicians;
    Integer mGroupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peadd_new_group);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    private void initialiseViewsAndObjects() {
        mCtx = PEAddNewGroup.this;
        mGroupName = findViewById(R.id.pe_new_grp_name_TXT);
        mGroupMembers = findViewById(R.id.pe_grp_members_TXT);

        mGroupId = getIntent().getIntExtra(DbConstants.KEY_GROUP_ID, -1);
        mSelectedTechnicians = getIntent().getIntegerArrayListExtra(DbConstants.KEY_TECHNICIAN_ID);

        String aGroupName = "Group " + mGroupId+1;
        mGroupName.setText(aGroupName);

        String aTechnicians = "";

        for (Integer atid : mSelectedTechnicians) {
            Technician atechnician = TechnicianTable.getInstance().getAllocatedTechnician(atid);
            aTechnicians = aTechnicians + atechnician.firstName + " " + atechnician.surName + "\n";
        }

        mGroupMembers.setText(aTechnicians);



    }

    public void onCreateGroupClicked(View view) {

        if (mGroupId != null && mSelectedTechnicians != null) {
            TechnicianTable.getInstance().assignTechnicianstoGroup(mSelectedTechnicians, mGroupId);
            Intent i = new Intent(mCtx, PELanding.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        }

    }
}
