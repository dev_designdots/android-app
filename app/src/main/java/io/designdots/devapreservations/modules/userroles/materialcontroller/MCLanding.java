package io.designdots.devapreservations.modules.userroles.materialcontroller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.modules.userlogin.LoginActivity;
import io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation.MCMaterialAllocationLanding;
import io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption.MCMaterialConsumptionLanding;
import io.designdots.devapreservations.modules.userroles.materialcontroller.receivedstock.MCReceivedStockLanding;

public class MCLanding extends BaseAppcompactActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private BaseAppcompactActivity mCtx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mclanding);


        initialiseViewsAndObjects();

    }

    private void initialiseViewsAndObjects() {
        mCtx = MCLanding.this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0); // 0-index header

        TextView aWelcomeUserTXT, aLastUpdateTimeTXT;
        aWelcomeUserTXT = headerLayout.findViewById(R.id.welcome_user_TXT);
        aLastUpdateTimeTXT = headerLayout.findViewById(R.id.last_updated_datetime_TXT);


        aWelcomeUserTXT.setText("Welcome Logesh!");
        aLastUpdateTimeTXT.setText("Last updated on 30 August 2018 08:45 AM");


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            Intent i = new Intent(mCtx, LoginActivity.class);
// set the new task and clear flags
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        }

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void onClickMaterialConsumption(View aView) {

        Intent i = new Intent(mCtx, MCMaterialConsumptionLanding.class);
        startActivity(i);


    }

    public void onClickMaterialAllocation(View aView) {

        Intent i = new Intent(mCtx, MCMaterialAllocationLanding.class);
        startActivity(i);

    }

    public void onClickReceivedStock(View aView) {

        Intent i = new Intent(mCtx, MCReceivedStockLanding.class);
        startActivity(i);

    }
}
