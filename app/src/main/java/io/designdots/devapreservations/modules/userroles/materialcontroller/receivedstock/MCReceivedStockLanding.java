package io.designdots.devapreservations.modules.userroles.materialcontroller.receivedstock;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

public class MCReceivedStockLanding extends BaseAppcompactActivity {

    private BaseAppcompactActivity mCtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcreceived_stock);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCReceivedStockLanding.this;

    }

    public void onClickPreservativesReceivedStock(View view) {
        Intent i = new Intent(mCtx, MCRSPreservativesList.class);
        startActivity(i);
    }

    public void onClickConsumablesReceivedStock(View view) {
        Intent i = new Intent(mCtx, MCRSConsumablesList.class);
        startActivity(i);
    }

    public void onClickReturnableToolsReceivedStock(View view) {
        Intent i = new Intent(mCtx, MCRSReturnableToolsList.class);
        startActivity(i);
    }
}
