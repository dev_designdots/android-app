package io.designdots.devapreservations.modules.userlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.modules.userroles.materialcontroller.MCLanding;
import io.designdots.devapreservations.modules.userroles.preservationengineer.PELanding;
import io.designdots.devapreservations.modules.userroles.qualitycontroller.QCLanding;
import io.designdots.devapreservations.utils.ShowAlert;

public class LoginActivity extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private EditText mEmailIdEDT, mPasswordEDT;

    String aMaterialUser = "logesh@designdots.io";
    String aMaterialPwd = "material@123";

    String aPreservationUser = "kavitha@designdots.io";
    String aPreservationPwd = "preservation@123";

    String aQCUser = "sathish@designdots.io";
    String aQCPassword = "quality@123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initialiseviewsandobjects();
    }

    private void initialiseviewsandobjects() {

        mCtx = LoginActivity.this;

        mEmailIdEDT = findViewById(R.id.input_email);
        mPasswordEDT = findViewById(R.id.input_password);


    }

    public void onclickLogin(View view) {

        String email = mEmailIdEDT.getText().toString().trim().toLowerCase();
        String password = mPasswordEDT.getText().toString().trim();


        if (email.length() > 0 && password.length() > 0) {

            if (email.equals(aMaterialUser) && password.equals(aMaterialPwd)) {

                Intent i = new Intent(LoginActivity.this, MCLanding.class);
                startActivity(i);


            } else if (email.equals(aPreservationUser) && password.equals(aPreservationPwd)) {

                Intent i = new Intent(LoginActivity.this, PELanding.class);
                startActivity(i);


            } else if (email.equals(aQCUser) && password.equals(aQCPassword)) {

                Intent i = new Intent(LoginActivity.this, QCLanding.class);
                startActivity(i);

            } else {

                ShowAlert.showSnackMsg(mCtx, "Invalid username or password", R.color.colorRed, R.color.colorPaleWhite);

            }

        } else {

            ShowAlert.showSnackMsg(mCtx, "Please enter valid credentials", R.color.colorRed, R.color.colorPaleWhite);
        }


    }
}
