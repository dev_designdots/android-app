package io.designdots.devapreservations.modules.userroles.qualitycontroller.compatibilitytests;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.utils.ShowAlert;

public class QCCTNotRequired extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qccompatibility_test_not_required);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        mCtx = QCCTNotRequired.this;


    }

    public void onSubmitButtonClicked(View view) {


        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_message_only);
        TextView titleTXT = (TextView) dialog.findViewById(R.id.dialog_title_TXT);
        TextView msgTXT = (TextView) dialog.findViewById(R.id.dialog_message_TXT);
        titleTXT.setText("Reason Submitted");
        msgTXT.setText("Reason Submitted Successfully");
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_ok_BTN);
        dialogButton.setText("Ok");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mCtx.onBackPressed();
            }
        });
        dialog.show();
    }
}
