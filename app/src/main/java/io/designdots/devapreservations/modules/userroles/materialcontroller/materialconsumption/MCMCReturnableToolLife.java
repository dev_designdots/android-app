package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.model.ReturnableToolAllocation;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.EngineerTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.AppUtils;
import io.designdots.devapreservations.utils.Constants;

public class MCMCReturnableToolLife extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private TextView mAssignedOnTXT, mReceivedOnTXT, mWorkHrsTXT, mEquipmentTagTXT, mEngineerNameTXT, mTechnicianNameTXT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcreturnable_tool_life);


        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        try {
            mCtx = MCMCReturnableToolLife.this;
            mAssignedOnTXT = findViewById(R.id.tool_life_assigned_on_TXT);
            mReceivedOnTXT = findViewById(R.id.tool_life_returned_on_TXT);
            mEquipmentTagTXT = findViewById(R.id.tool_life_tag_no_TXT);
            mWorkHrsTXT = findViewById(R.id.tool_life_work_hours_TXT);
            mEngineerNameTXT = findViewById(R.id.tool_life_engineer_TXT);
            mTechnicianNameTXT = findViewById(R.id.tool_life_technician_TXT);

            Integer aReturnableToolId = getIntent().getIntExtra(DbConstants.KEY_RETURNABLE_TOOL_ID, 0);
            String aToolIdName = getIntent().getStringExtra(DbConstants.KEY_TOOL_ID);
            ReturnableToolAllocation allocation = new Gson().fromJson(getIntent().getStringExtra(DbConstants.KEY_RETURNABLE_TOOL_ALLOC_KEY), ReturnableToolAllocation.class);


            getSupportActionBar().setTitle("Tool Life (" + aToolIdName + ")");

            String aDateStr = "", issuedDateStr = "", returnedDateStr = "";
            if (allocation.returnedDateTime != null) {
                aDateStr = AppUtils.getDate(Long.parseLong(allocation.returnedDateTime), Constants.DATE_FORMAT);
                returnedDateStr = "Returned On\n" + aDateStr;
            } else if (allocation.issuedDateTime != null) {
                aDateStr = AppUtils.getDate(Long.parseLong(allocation.issuedDateTime), Constants.DATE_FORMAT);
                issuedDateStr = "Issued On\n" + aDateStr;
            }
            getSupportActionBar().setSubtitle(aDateStr);

            mAssignedOnTXT.setText(issuedDateStr);
            mReceivedOnTXT.setText(returnedDateStr);


            String[] equipmentsarray = allocation.equipments.split(",");
            ArrayList<Integer> equipmentIds = new ArrayList<>();
            for (int i = 0; i < equipmentsarray.length; i++) {
                equipmentIds.add(Integer.parseInt(equipmentsarray[i]));
            }

            ArrayList<Equipment> aEquipmentsList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            String aEquipmentTag = "";
            for (int i = 0; i < aEquipmentsList.size(); i++) {
                Equipment e = aEquipmentsList.get(i);
                aEquipmentTag = aEquipmentTag + e.tagNumber + "\n";
            }

            mEquipmentTagTXT.setText(aEquipmentTag);

            PreservationEngineer engineer = EngineerTable.getInstance().getAllocatedEngineer(allocation.engineerId);
            Technician technician = TechnicianTable.getInstance().getAllocatedTechnician(allocation.technicianId);
            String enggName = engineer.firstName + " " + engineer.surName;
            String techName = technician.firstName + " " + technician.surName;

            mEngineerNameTXT.setText(enggName);
            mTechnicianNameTXT.setText(techName);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}



