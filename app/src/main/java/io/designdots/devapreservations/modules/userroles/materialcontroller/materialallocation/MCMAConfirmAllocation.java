package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.Dialog;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.model.ConsumableAllocation;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.model.PreservativeAllocation;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.model.ReturnableToolAllocation;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.database.sqlite.tables.EngineerTable;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.modules.userroles.materialcontroller.MCLanding;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;

public class MCMAConfirmAllocation extends BaseAppcompactActivity {

    private BaseAppcompactActivity mCtx;

    private TextView mText1, mText2, mText3, mText4, mText5, mText6, mText7, mText8, mText9, mText10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaconfirm_allocation);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCMAConfirmAllocation.this;

        mText1 = findViewById(R.id.mc_ma_confirm_allocation_TXT_one);
        mText2 = findViewById(R.id.mc_ma_confirm_allocation_TXT_two);
        mText3 = findViewById(R.id.mc_ma_confirm_allocation_TXT_three);
        mText4 = findViewById(R.id.mc_ma_confirm_allocation_TXT_four);
        mText5 = findViewById(R.id.mc_ma_confirm_allocation_TXT_five);
        mText6 = findViewById(R.id.mc_ma_confirm_allocation_TXT_six);
        mText7 = findViewById(R.id.mc_ma_confirm_allocation_TXT_seven);
        mText8 = findViewById(R.id.mc_ma_confirm_allocation_TXT_eight);
        mText9 = findViewById(R.id.mc_ma_confirm_allocation_TXT_nine);
        mText10 = findViewById(R.id.mc_ma_confirm_allocation_TXT_ten);

        Integer aEngineerId, aTechnicianId, aConsumableorPreservativeId;
        String aUOM, aQuantity;

        ArrayList<Integer> equipmentIds, returnabletoolIds;

        Bundle aBundle = getIntent().getExtras();
        aEngineerId = aBundle.getInt(DbConstants.KEY_ENGINEER_ID);
        aTechnicianId = aBundle.getInt(DbConstants.KEY_TECHNICIAN_ID);
        aUOM = aBundle.getString(DbConstants.KEY_UOM);
        aQuantity = aBundle.getString(DbConstants.KEY_QUANTITY);
        equipmentIds = aBundle.getIntegerArrayList(DbConstants.KEY_EQUIPMENT_ID);


        if (aBundle.getString(Constants.ALLOCATION).equals(Constants.PRESERVATIVE_ALLOCATION)) {
            aConsumableorPreservativeId = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ID);
            ArrayList<Equipment> aEquipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforPreservativeAllocation(aConsumableorPreservativeId, equipmentIds);
            Preservative preservative = PreservativeTable.getInstance().getAllocatedPreservative(aConsumableorPreservativeId);
            PreservationEngineer engineer = EngineerTable.getInstance().getAllocatedEngineer(aEngineerId);
            Technician technician = TechnicianTable.getInstance().getAllocatedTechnician(aTechnicianId);

            mText1.setText("Preservative Name");
            mText2.setText(preservative.chemicalName);
            mText3.setText("Equipments");
            String aTagIds = "";
            for (Equipment e : aEquipmentList) {
                aTagIds = aTagIds + e.tagNumber + "\n";
            }
            mText4.setText(aTagIds);
            mText5.setText("Total Quantity Allocated");
            mText6.setText(aQuantity + " " + preservative.unitOfMeasure);
            mText7.setText("Preservation Engineer");
            mText8.setText(engineer.firstName + " " + engineer.surName);
            mText9.setText("Technician");
            mText10.setText(technician.firstName + " " + technician.surName);

        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.CONSUMABLE_ALLOCATION)) {
            aConsumableorPreservativeId = aBundle.getInt(DbConstants.KEY_CONSUMABLEID);
            ArrayList<Equipment> aEquipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);
            Consumable consumable = ConsumableTable.getInstance().getAllocatedConsumable(aConsumableorPreservativeId);
            PreservationEngineer engineer = EngineerTable.getInstance().getAllocatedEngineer(aEngineerId);
            Technician technician = TechnicianTable.getInstance().getAllocatedTechnician(aTechnicianId);

            mText1.setText("Consumable Name");
            mText2.setText(consumable.consumableName);
            mText3.setText("Equipments");
            String aTagIds = "";
            for (Equipment e : aEquipmentList) {
                aTagIds = aTagIds + e.tagNumber + "\n";
            }
            mText4.setText(aTagIds);
            mText5.setText("Total Quantity Allocated");
            mText6.setText(aQuantity + " " + consumable.unitOfMeasure);
            mText7.setText("Preservation Engineer");
            mText8.setText(engineer.firstName + " " + engineer.surName);
            mText9.setText("Technician");
            mText10.setText(technician.firstName + " " + technician.surName);

        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.TOOL_ALLOCATION)) {
            returnabletoolIds = aBundle.getIntegerArrayList(DbConstants.KEY_RETURNABLE_TOOL_ID);
            ArrayList<Equipment> aEquipmentList = EquipmentTable.getInstance().getFinalEquipmentsListforAllocation(equipmentIds);

            ArrayList<ReturnableTool> aToolsList =
                    ReturnableToolTable.getInstance().getFinalReturnableToolsforAllocation(returnabletoolIds);
            PreservationEngineer engineer = EngineerTable.getInstance().getAllocatedEngineer(aEngineerId);
            Technician technician = TechnicianTable.getInstance().getAllocatedTechnician(aTechnicianId);

            mText1.setText("Returnable Tool Name");
            mText2.setText(aToolsList.get(0).toolName);
            mText3.setText("Equipments");
            String aTagIds = "";
            for (Equipment e : aEquipmentList) {
                aTagIds = aTagIds + e.tagNumber + "\n";
            }
            mText4.setText(aTagIds);
            mText5.setText("Total Quantity Allocated");
            mText6.setText(aQuantity + " " + aToolsList.get(0).unitOfMeasure);
            mText7.setText("Preservation Engineer");
            mText8.setText(engineer.firstName + " " + engineer.surName);
            mText9.setText("Technician");
            mText10.setText(technician.firstName + " " + technician.surName);

        }


    }

    public void onAllocateButtonClicked(View view) {

        String aMessage = "", aTitle = "Allocation Completed";


        Integer aEngineerId, aTechnicianId, aConsumableorPreservativeId;
        String aUOM, aQuantity;

        ArrayList<Integer> equipmentIds, returnabletoolIds;

        Bundle aBundle = getIntent().getExtras();
        aEngineerId = aBundle.getInt(DbConstants.KEY_ENGINEER_ID);
        aTechnicianId = aBundle.getInt(DbConstants.KEY_TECHNICIAN_ID);
        aUOM = aBundle.getString(DbConstants.KEY_UOM);
        aQuantity = aBundle.getString(DbConstants.KEY_QUANTITY);
        equipmentIds = aBundle.getIntegerArrayList(DbConstants.KEY_EQUIPMENT_ID);


        String aEquipments = "";
        for (int i = 0; i < equipmentIds.size(); i++) {
            if (i == (equipmentIds.size() - 1)) {
                aEquipments = aEquipments + equipmentIds.get(i);
            } else {
                aEquipments = aEquipments + equipmentIds.get(i) + ",";
            }
        }


        if (aBundle.getString(Constants.ALLOCATION).equals(Constants.PRESERVATIVE_ALLOCATION)) {

            aConsumableorPreservativeId = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ID);


            PreservativeAllocation allocation = new PreservativeAllocation();
            allocation.createdDateTime = "" + System.currentTimeMillis();
            allocation.equipments = aEquipments;
            allocation.quantity = Float.parseFloat(aQuantity);
            allocation.preservativeId = aConsumableorPreservativeId;
            allocation.engineerId = aEngineerId;
            allocation.technicianId = aTechnicianId;
            PreservativeAllocationTable.getInstance().allocatePreservative(allocation);

            aMessage = "Preservatives Allocated Successfully";


        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.CONSUMABLE_ALLOCATION)) {

            aConsumableorPreservativeId = aBundle.getInt(DbConstants.KEY_CONSUMABLEID);

            ConsumableAllocation allocation = new ConsumableAllocation();
            allocation.createdDateTime = "" + System.currentTimeMillis();
            allocation.equipments = aEquipments;
            allocation.quantity = Float.parseFloat(aQuantity);
            allocation.consumableId = aConsumableorPreservativeId;
            allocation.engineerId = aEngineerId;
            allocation.technicianId = aTechnicianId;
            ConsumableAllocationTable.getInstance().allocateConsumable(allocation);

            aMessage = "Consumables Allocated Successfully";

        } else if (aBundle.getString(Constants.ALLOCATION).equals(Constants.TOOL_ALLOCATION)) {

            returnabletoolIds = aBundle.getIntegerArrayList(DbConstants.KEY_RETURNABLE_TOOL_ID);

            for (Integer aToolid : returnabletoolIds) {

                ReturnableToolAllocation allocation = new ReturnableToolAllocation();
                allocation.toolId = aToolid;
                allocation.equipments = aEquipments;
                allocation.issuedDateTime = "" + SystemClock.currentThreadTimeMillis();
                allocation.engineerId = aEngineerId;
                allocation.technicianId = aTechnicianId;

                ReturnableToolAllocationTable.getInstance().allocateorReturnReturnableTool(allocation);

            }

            aMessage = "Returnable Tools allocated Successfully";


        }

        final Dialog dialog = new Dialog(mCtx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_message_only);
        TextView titleTXT = dialog.findViewById(R.id.dialog_title_TXT);
        TextView msgTXT = dialog.findViewById(R.id.dialog_message_TXT);
        titleTXT.setText(aTitle);
        msgTXT.setText(aMessage);
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
        Button dialogButton = dialog.findViewById(R.id.dialog_ok_BTN);
        dialogButton.setText("Ok");
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent i = new Intent(mCtx, MCLanding.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
        dialog.show();

    }
}
