package io.designdots.devapreservations.modules.userroles.preservationengineer.myteam;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.model.TechnicianGroup;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;

public class PEMyTeam extends BaseAppcompactActivity {


    private AppCompatActivity mCtx;
    RadioButton mAllUsersRBTN, mGroupsRBTN;
    RadioGroup mTeamRGRP;
    ListView mTeamLV;
    CustomListAdapter mAdapter;

    @Override
    protected void onResume() {
        super.onResume();

        mAllUsersRBTN.setChecked(true);
        mGroupsRBTN.setChecked(false);
        mAdapter = new CustomListAdapter(false);
        invalidateOptionsMenu();
        mTeamLV.setAdapter(mAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemy_team);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    private void initialiseViewsAndObjects() {
        mCtx = PEMyTeam.this;

        mTeamRGRP = findViewById(R.id.team_divisions_RGRP);
        mAllUsersRBTN = findViewById(R.id.pe_all_technicians_RBTN);
        mGroupsRBTN = findViewById(R.id.pe_groups_RBTN);
        mTeamLV = findViewById(R.id.pe_team_LV);


        mGroupsRBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAllUsersRBTN.setChecked(false);
                mAdapter = new CustomListAdapter(true);
                invalidateOptionsMenu();
                mTeamLV.setAdapter(mAdapter);
            }
        });

        mAllUsersRBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGroupsRBTN.setChecked(false);
                mAdapter = new CustomListAdapter(false);
                invalidateOptionsMenu();
                mTeamLV.setAdapter(mAdapter);
            }
        });


        mTeamLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                mAdapter.onListItemClicked(pos);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pemy_team_menu, menu);

        final SearchView aSearchview = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        aSearchview.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        if (mGroupsRBTN.isChecked()) {
            menu.findItem(R.id.action_search).setVisible(false);
            menu.findItem(R.id.action_add).setVisible(true);
        } else {
            menu.findItem(R.id.action_search).setVisible(true);
            menu.findItem(R.id.action_add).setVisible(false);
        }

        menu.findItem(R.id.action_add).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent i = new Intent(mCtx, PESelectGroupTechnicians.class);
                i.putExtra(Constants.GROUP_OPERATION, Constants.ADD_NEW);
                startActivity(i);


                return false;
            }
        });


        return true;
    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;

        ArrayList<Technician> mTechniciansList;
        ArrayList<TechnicianGroup> mTechnicianGroupsList;
        Boolean isGroupChecked = false;

        public CustomListAdapter(Boolean isGroupChecked) {
            inflater = mCtx.getLayoutInflater();
            this.isGroupChecked = isGroupChecked;

            this.mTechniciansList = TechnicianTable.getInstance().getAllTechnicians();
            this.mTechnicianGroupsList = TechnicianTable.getInstance().getTechnicianGroups();
        }


        @Override
        public int getCount() {

            if (isGroupChecked)
                return mTechnicianGroupsList.size();
            else
                return mTechniciansList.size();
        }

        @Override
        public Object getItem(int pos) {
            return pos;
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_pe_team_member_item, null);

                holder.mGroupLAY = convertView.findViewById(R.id.group_LAY);
                holder.mTeamMemberLAY = convertView.findViewById(R.id.team_member_LAY);
                holder.mGroupName = convertView.findViewById(R.id.group_name_TXT);
                holder.mGrpMemCount = convertView.findViewById(R.id.group_member_count_TXT);
                holder.mTeamMemberName = convertView.findViewById(R.id.row_team_member_name_TXT);
                holder.mTeamMemberId = convertView.findViewById(R.id.row_team_member_id_TXT);
                holder.mTeamMemberGrpName = convertView.findViewById(R.id.row_team_member_group_name_TXT);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (isGroupChecked) {
                holder.mGroupLAY.setVisibility(View.VISIBLE);
                holder.mTeamMemberLAY.setVisibility(View.GONE);
                TechnicianGroup technicianGroup = mTechnicianGroupsList.get(pos);

                String aGroupName = "Group " + (technicianGroup.groupId + 1);
                String aMemberCount = "" + technicianGroup.technicians.size();

                holder.mGroupName.setText(aGroupName);
                holder.mGrpMemCount.setText(aMemberCount);


            } else {
                holder.mGroupLAY.setVisibility(View.GONE);
                holder.mTeamMemberLAY.setVisibility(View.VISIBLE);
                Technician technician = mTechniciansList.get(pos);

                String aTechnicianName = technician.firstName + " " + technician.surName;
                String aTechnicianID = "" + technician.partyKey;
                String aTechnicianGroupName;
                if (technician.groupId >= 0) {
                    holder.mTeamMemberGrpName.setVisibility(View.VISIBLE);
                    aTechnicianGroupName = "Group " + (technician.groupId + 1);
                } else {
                    aTechnicianGroupName = "";
                    holder.mTeamMemberGrpName.setVisibility(View.INVISIBLE);

                }

                holder.mTeamMemberName.setText(aTechnicianName);
                holder.mTeamMemberId.setText(aTechnicianID);
                holder.mTeamMemberGrpName.setText(aTechnicianGroupName);

            }

            return convertView;
        }


        @Override
        public Filter getFilter() {
            return null;
        }

        public void onListItemClicked(int pos) {

            if (isGroupChecked) {
                TechnicianGroup aGroup = mTechnicianGroupsList.get(pos);
                Integer aGroupID = aGroup.groupId;

                Intent i = new Intent(mCtx, PESelectGroupTechnicians.class);
                i.putExtra(Constants.GROUP_OPERATION, Constants.EDIT);
                i.putExtra(DbConstants.KEY_GROUP_ID, aGroupID);
                startActivity(i);

            }
        }


        class ViewHolder {
            LinearLayout mGroupLAY, mTeamMemberLAY;

            TextView mTeamMemberName, mTeamMemberId, mTeamMemberGrpName, mGroupName, mGrpMemCount;

        }
    }

}
