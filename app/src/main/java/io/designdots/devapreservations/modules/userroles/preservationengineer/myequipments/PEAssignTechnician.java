package io.designdots.devapreservations.modules.userroles.preservationengineer.myequipments;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.customcomponents.NothingSelectedSpinnerAdapter;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.model.TechnicianGroup;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.ShowAlert;

public class PEAssignTechnician extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;


    private RadioButton mAllUsersRBTN, mGroupsRBTN;
    private Spinner mSpinner;
    private TextView mChooseTitleTXT;
    private ArrayList<TechnicianGroup> mTechGroupsList;
    private ArrayList<Technician> mTechniciansList;
    private Integer mSelectedItem = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peassign_technician);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    private void initialiseViewsAndObjects() {

        mCtx = PEAssignTechnician.this;

        mAllUsersRBTN = findViewById(R.id.pe_all_technicians_RBTN);
        mSpinner = findViewById(R.id.pe_assign_technican_SPN);
        mGroupsRBTN = findViewById(R.id.pe_groups_RBTN);
        mChooseTitleTXT = findViewById(R.id.pe_assign_technician_title_TXT);

        mTechGroupsList = TechnicianTable.getInstance().getTechnicianGroups();

        mTechniciansList = TechnicianTable.getInstance().getAllTechniciansWithoutGroup();

        mAllUsersRBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onIndividualSelected();
            }
        });

        mGroupsRBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGroupSelected();
            }
        });


        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {

                mSelectedItem = pos;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

                mSelectedItem = -1;

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        mAllUsersRBTN.setChecked(true);
        onIndividualSelected();

    }

    private void onIndividualSelected() {

        mGroupsRBTN.setChecked(false);
        mChooseTitleTXT.setText("Select Technician");

        if (mTechniciansList.size() > 0) {
            String[] aTechnicianNames = new String[mTechniciansList.size()];
            for (int i = 0; i < mTechniciansList.size(); i++) {
                Technician t = mTechniciansList.get(i);
                String aName = t.firstName + " " + t.surName;
                aTechnicianNames[i] = aName;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mCtx,
                    android.R.layout.simple_spinner_dropdown_item, aTechnicianNames);
            mSpinner.setPrompt("Select a technician");
            mSpinner.setAdapter(
                    new NothingSelectedSpinnerAdapter(
                            adapter,
                            R.layout.spinner_technician_nothing_selected,
                            // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                            mCtx));

        } else {

            mSpinner.setPrompt("Individual not available ");

        }

    }

    private void onGroupSelected() {
        mAllUsersRBTN.setChecked(false);
        mChooseTitleTXT.setText("Select Group");

        if (mTechGroupsList.size() > 0) {
            String[] aGroupNames = new String[mTechGroupsList.size()];
            for (int i = 0; i < mTechGroupsList.size(); i++) {
                TechnicianGroup t = mTechGroupsList.get(i);
                String aName = "Group" + (t.groupId + 1);
                aGroupNames[i] = aName;
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mCtx,
                    android.R.layout.simple_spinner_dropdown_item, aGroupNames);
            mSpinner.setPrompt("Select a technician");
            mSpinner.setAdapter(
                    new NothingSelectedSpinnerAdapter(
                            adapter,
                            R.layout.spinner_group_nothing_selected,
                            // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                            mCtx));

        } else {

            mSpinner.setPrompt("Individual not available ");

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }


    public void onClickConfirm(View view) {

        if (mSelectedItem >= 0) {
            if (mGroupsRBTN.isChecked()) {

            } else {

            }

        } else {
            ShowAlert.showSnackMsg(mCtx, "choose a Individual or a Group", R.color.colorRed, R.color.colorPaleWhite);
        }

    }
}
