package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.utils.Constants;

public class MCMCPreservativesList extends BaseAppcompactActivity {


    private BaseAppcompactActivity mCtx;
    private ListView mConsumedPreservativesLV;
    private CustomListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcpreservatives_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        return true;
    }


    private void initialiseViewsAndObjects() {
        mCtx = MCMCPreservativesList.this;
        mConsumedPreservativesLV = findViewById(R.id.mc_mc_preservatives_LV);

        ArrayList<Preservative> preservatives = PreservativeTable.getInstance().getAllPreservatives();
        mAdapter = new CustomListAdapter(preservatives);
        mConsumedPreservativesLV.setAdapter(mAdapter);

        mConsumedPreservativesLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Intent i = new Intent(mCtx, MCMCPreservativeDetailsList.class);
                Bundle aBundle =new Bundle();
                Integer aPreservativeId = (int)mAdapter.getItemId(pos);
                aBundle.putString(Constants.ALLOCATION, Constants.PRESERVATIVE_ALLOCATION);
                aBundle.putInt(DbConstants.KEY_PRESERVATIVE_ID,aPreservativeId);
                i.putExtras(aBundle);
                startActivity(i);
            }
        });
    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<Preservative> mPreservativesList;
        ArrayList<Preservative> mFilteredList;

        public CustomListAdapter(ArrayList<Preservative> preservatives) {
            inflater = mCtx.getLayoutInflater();
            this.mPreservativesList = preservatives;
            mFilteredList = new ArrayList<>();
        }


        @Override
        public int getCount() {
            return mPreservativesList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mPreservativesList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mPreservativesList.get(pos).preservativeId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {
            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_mc_preservatives_list_item, null);

                holder.mPreservativeNameTXT = convertView.findViewById(R.id.row_mc_mc_preservative_name_TXT);
                holder.mTotalQtyTXT = convertView.findViewById(R.id.row_mc_mc_preservative_total_qty_TXT);
                holder.mConsumedQtyTXT = convertView.findViewById(R.id.row_mc_mc_preservative_consumed_qty_TXT);
                holder.mRemainingQtyTXT = convertView.findViewById(R.id.row_mc_mc_preservative_remaining_qty_TXT);

                convertView.setTag(holder);
            } else {

                holder = (ViewHolder) convertView.getTag();
            }

            Preservative preservative = mPreservativesList.get(pos);

            String aTotalQty = preservative.quantity + " " + preservative.unitOfMeasure;
            Float aAllocatedQty = PreservativeAllocationTable.getInstance().getAllocatedPreservativeQuantity(preservative.preservativeId);
            String aConsumedQty = aAllocatedQty + " " + preservative.unitOfMeasure;
            String aRemainingQty = preservative.receivedQuantity - aAllocatedQty + " " + preservative.unitOfMeasure;

            holder.mPreservativeNameTXT.setText(preservative.chemicalName);
            holder.mTotalQtyTXT.setText(aTotalQty);
            holder.mConsumedQtyTXT.setText(aConsumedQty);
            holder.mRemainingQtyTXT.setText(aRemainingQty);


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Preservative> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mPreservativesList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Preservative g : mFilteredList) {
                                if (g.chemicalName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mPreservativesList = (ArrayList<Preservative>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        class ViewHolder {
            TextView mPreservativeNameTXT, mTotalQtyTXT, mConsumedQtyTXT, mRemainingQtyTXT;
        }
    }

}
