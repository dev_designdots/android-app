package io.designdots.devapreservations.modules.userroles.qualitycontroller.compatibilitytests;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

public class QCCTEquipmentDetails extends BaseAppcompactActivity {

    private TextView mBeforePhotoDateTXT, mDuringPhotoDateTXT, mEqpmtCategoryTXT, mEqpmtLocationTXT,
            mEqpmtZoneTXT, mContentNameTXT, mWaterDieselOilTXT, mEmptyVolTXT, mContentVolTXT;

    private Button mStartTestBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qccompatibility_test_equipment_details);


        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {


    }


    public void onStartTestClicked(View view) {

    }
}
