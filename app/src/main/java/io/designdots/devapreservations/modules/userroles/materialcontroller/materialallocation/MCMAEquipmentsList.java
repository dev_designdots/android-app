package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.TVUtils;

public class MCMAEquipmentsList extends BaseAppcompactActivity {


    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;
    private ArrayList<Integer> mSelectedEquipments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaequipments_list);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        return true;
    }


    private void initialiseViewsAndObjects() {
        mCtx = MCMAEquipmentsList.this;

        mLV = findViewById(R.id.mc_ma_equipments_LV);

        Bundle aBundle = getIntent().getExtras();
        Integer aPreservativeId, aEngineerId;

        aEngineerId = aBundle.getInt(DbConstants.KEY_ENGINEER_ID);

        if (aBundle.getString(Constants.ALLOCATION).equals(Constants.PRESERVATIVE_ALLOCATION)) {
            aPreservativeId = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ID);
            mAdapter = new CustomListAdapter(EquipmentTable.getInstance().getEquipmentsforPreservativeAllocation(aPreservativeId, aEngineerId));
        } else {
            mAdapter = new CustomListAdapter(EquipmentTable.getInstance().getEquipmentsUnderEngineer(aEngineerId));
        }

        mLV.setAdapter(mAdapter);


    }

    public void onNextButtonClicked(View view) {

        Intent i = new Intent(mCtx, MCMAQuantitySummaryList.class);
        Bundle aBundle = getIntent().getExtras();
        aBundle.putIntegerArrayList(DbConstants.KEY_EQUIPMENT_ID, mSelectedEquipments);
        i.putExtras(aBundle);
        startActivity(i);

    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<Equipment> mEquipmentList;
        ArrayList<Equipment> mFilteredList;

        public CustomListAdapter(ArrayList<Equipment> equipmentList) {

            inflater = mCtx.getLayoutInflater();
            mEquipmentList = equipmentList;
            mSelectedEquipments = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mEquipmentList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mEquipmentList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mEquipmentList.get(pos).equipmentId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_list_item, null);
                holder.mEquipmentName = (TextView) convertView.findViewById(R.id.row_mc_list_TXT_one);
                holder.mEquipmentTag = (TextView) convertView.findViewById(R.id.row_mc_list_TXT_two);
                holder.mOptionsIBTN = (ImageButton) convertView.findViewById(R.id.row_mc_list_menu_more_options_IMGBTN);
                holder.mChooseCHBX = (AppCompatCheckBox) convertView.findViewById(R.id.row_mc_list_CHBX);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mOptionsIBTN.setVisibility(View.GONE);

            final Equipment equipment = mEquipmentList.get(pos);

            holder.mEquipmentName.setText(equipment.equipmentName);
            TVUtils.setTextColor(mCtx, holder.mEquipmentName, R.color.colorBlack);

            holder.mEquipmentTag.setText(equipment.tagNumber);
            TVUtils.setTextColor(mCtx, holder.mEquipmentTag, R.color.colorGrey);


            if (mSelectedEquipments.contains(equipment.equipmentId)) {
                holder.mChooseCHBX.setChecked(true);
            } else {
                holder.mChooseCHBX.setChecked(false);
            }

            holder.mChooseCHBX.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((CheckBox) view).isChecked()) {
                        // update for the checkbox view
                        if (!mSelectedEquipments.contains(equipment.equipmentId)) {
                            mSelectedEquipments.add(equipment.equipmentId);
                        }


                    } else {
                        // update for the checkbox
                        if (mSelectedEquipments.contains(equipment.equipmentId)) {
                            mSelectedEquipments.remove(equipment.equipmentId);
                        }

                        notifyDataSetChanged();

                    }
                }
            });


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Equipment> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mEquipmentList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Equipment g : mFilteredList) {

                                if (g.equipmentName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mEquipmentList = (ArrayList<Equipment>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mEquipmentName, mEquipmentTag;
            AppCompatCheckBox mChooseCHBX;
            ImageButton mOptionsIBTN;

        }
    }


}
