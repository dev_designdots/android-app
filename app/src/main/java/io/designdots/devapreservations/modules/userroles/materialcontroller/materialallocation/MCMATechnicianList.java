package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.ShowAlert;
import io.designdots.devapreservations.utils.TVUtils;

public class MCMATechnicianList extends BaseAppcompactActivity {


    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;
    private int mSelectedpostion = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmatechnician_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });

        return true;
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCMATechnicianList.this;
        mLV = findViewById(R.id.mc_ma_technicians_LV);
        Bundle aBundle = getIntent().getExtras();
        Integer aEngineerId = aBundle.getInt(DbConstants.KEY_ENGINEER_ID);
        mAdapter = new CustomListAdapter(TechnicianTable.getInstance().getTechnicians(aEngineerId));
        mLV.setAdapter(mAdapter);

        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedpostion = i;
                mAdapter.notifyDataSetChanged();
            }
        });


    }

    public void onNextButtonClicked(View view) {

        if (mSelectedpostion != -1) {

            Intent i = new Intent(mCtx, MCMAEquipmentsList.class);
            Bundle aBundle = getIntent().getExtras();
            aBundle.putInt(DbConstants.KEY_TECHNICIAN_ID, (int) mAdapter.getItemId(mSelectedpostion));
            i.putExtras(aBundle);
            startActivity(i);

        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select a Technician", R.color.colorRed, R.color.colorPaleWhite);
        }

    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<Technician> mTechnicanList;
        ArrayList<Technician> mFilteredList;

        public CustomListAdapter(ArrayList<Technician> technicians) {
            inflater = mCtx.getLayoutInflater();
            this.mTechnicanList = technicians;
        }

        @Override
        public int getCount() {
            return mTechnicanList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mTechnicanList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mTechnicanList.get(pos).partyKey;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_technician_engineer_item, null);
                holder.mName = (TextView) convertView.findViewById(R.id.technician_engineer_name_TXT);
                holder.mRootLay = convertView.findViewById(R.id.row_tech_engineer_root_LAY);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (pos == mSelectedpostion) {
                holder.mRootLay.setBackgroundResource(R.drawable.bg_box_selected);
            } else {
                holder.mRootLay.setBackgroundResource(R.drawable.bg_box);
            }

            Technician technician = mTechnicanList.get(pos);

            String aName = technician.firstName + " " + technician.surName;
            holder.mName.setText(aName);
            TVUtils.setTextColor(mCtx, holder.mName, R.color.colorBlack);


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Technician> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mTechnicanList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Technician g : mFilteredList) {
                                String aN = g.firstName + g.surName;
                                if (aN.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mTechnicanList = (ArrayList<Technician>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mName;
            LinearLayout mRootLay;
        }
    }


}
