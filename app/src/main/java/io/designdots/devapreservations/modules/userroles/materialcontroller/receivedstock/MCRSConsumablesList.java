package io.designdots.devapreservations.modules.userroles.materialcontroller.receivedstock;

import android.app.Dialog;
import android.app.SearchManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ConsumableTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;

public class MCRSConsumablesList extends BaseAppcompactActivity {

    private BaseAppcompactActivity mCtx;
    private ListView mReceivedLV;
    CustomListAdapter mAdapter;
    private RadioGroup mConsumablesRGRP;
    private Button mConfirmBTN;
    private ArrayList<Integer> mSelectedConsumables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcrsconsumables_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });


        return true;
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCRSConsumablesList.this;
        mReceivedLV = (ListView) findViewById(R.id.mc_rs_consumables_LV);
        mConsumablesRGRP = findViewById(R.id.mc_rs_consumables_tabs_RDGRP);
        mConfirmBTN = findViewById(R.id.mc_rs_consumables_confirm_BTN);

        mConsumablesRGRP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int selectedId) {

                updateListView(selectedId);

            }
        });
        mConsumablesRGRP.check(R.id.mc_rs_consumables_not_received_RBTN);


    }

    private void updateListView(int selectedId) {

        ArrayList<Consumable> consumablesList = new ArrayList<>();

        switch (selectedId) {
            case R.id.mc_rs_consumables_not_received_RBTN:
                consumablesList = ConsumableTable.getInstance().getConsumables(DbConstants.NOT_RECEIVED);
                mAdapter = new CustomListAdapter(consumablesList, true);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.VISIBLE);
                break;
            case R.id.mc_rs_consumables_partially_received_RBTN:
                consumablesList = ConsumableTable.getInstance().getConsumables(DbConstants.PARTIALLY_RECEIVED);
                mAdapter = new CustomListAdapter(consumablesList, true);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.VISIBLE);
                break;
            case R.id.mc_rs_consumables_fully_received_RBTN:
                consumablesList = ConsumableTable.getInstance().getConsumables(DbConstants.COMPLETELY_RECEIVED);
                mAdapter = new CustomListAdapter(consumablesList, false);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.GONE);
                break;
        }
    }

    public void onClickConfirm(View view) {
        if (mSelectedConsumables.size() > 0) {
            // Update database here
            ConsumableTable.getInstance().updateConsumablesStatus(mAdapter.getUpdatedConsumablesList());
            ShowAlert.showDialog(mCtx, "Confirmation", mSelectedConsumables.size() + " out of " + mAdapter.getCount() + " items added successfully", "Confirm");
            updateListView(mConsumablesRGRP.getCheckedRadioButtonId());
        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select atleast one consumable", R.color.colorRed, R.color.colorPaleWhite);
        }

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        private ArrayList<Consumable> mConsumablesList;
        private ArrayList<Consumable> mFilteredList;
        private boolean mEnableMenu;


        public CustomListAdapter(ArrayList<Consumable> ConsumableList, boolean enableMenu) {

            inflater = mCtx.getLayoutInflater();
            this.mConsumablesList = ConsumableList;
            this.mEnableMenu = enableMenu;
            mSelectedConsumables = new ArrayList<>();
            mSelectedConsumables.clear();

        }


        @Override
        public int getCount() {
            return mConsumablesList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return mConsumablesList.get(i).consumableId;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_received_stock_preservative_item, null);
                holder.mConsumableName = convertView.findViewById(R.id.row_received_stock_preservative_name_TXT);
                holder.mConsumableDescription = convertView.findViewById(R.id.row_received_stock_preservative_chemical_id_TXT);
                holder.mConsumableTotalQty = convertView.findViewById(R.id.row_received_stock_preservative_qty_TXT);
                holder.mConsumableReceivedQty = convertView.findViewById(R.id.row_received_stock_preservative_receivedQty_TXT);
                holder.mselectionCHBX = convertView.findViewById(R.id.row_received_stock_preservative_CHBX);
                holder.mOptionsIMGBTN = convertView.findViewById(R.id.row_received_stock_preservative_list_menu_more_options_IMGBTN);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final Consumable aConsumable = mConsumablesList.get(pos);

            holder.mConsumableName.setText(aConsumable.consumableName);
            holder.mselectionCHBX.setTag(pos);

            String totalQty = aConsumable.quantity + "  " + aConsumable.unitOfMeasure;
            holder.mConsumableTotalQty.setText(totalQty);

            if (aConsumable.receiptStatus == DbConstants.PARTIALLY_RECEIVED) {
                String receivedqty = "Received Quantity : " + aConsumable.receivedQuantity + "  " + aConsumable.unitOfMeasure;
                holder.mConsumableReceivedQty.setText(receivedqty);
                holder.mConsumableReceivedQty.setVisibility(View.VISIBLE);
            } else {
                holder.mConsumableReceivedQty.setVisibility(View.GONE);
            }


            if (mEnableMenu) {

                if (mSelectedConsumables.contains(aConsumable.consumableId)) {
                    holder.mselectionCHBX.setChecked(true);
                } else {
                    holder.mselectionCHBX.setChecked(false);
                }


                holder.mselectionCHBX.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (((CheckBox) view).isChecked()) {
                            // update for the checkbox view
                            if (!mSelectedConsumables.contains(aConsumable.consumableId)) {
                                mSelectedConsumables.add(aConsumable.consumableId);
                            }
                            // updating the data object
                            mConsumablesList.get((int) view.getTag()).receiptStatus = DbConstants.COMPLETELY_RECEIVED;
                            mConsumablesList.get((int) view.getTag()).receivedQuantity = mConsumablesList.get((int) view.getTag()).quantity;

                        } else {
                            // update for the checkbox
                            if (mSelectedConsumables.contains(aConsumable.consumableId)) {
                                mSelectedConsumables.remove(aConsumable.consumableId);
                            }
                            // updating the data objects
                            mConsumablesList.get((int) view.getTag()).receivedQuantity = Float.parseFloat("0");
                            mConsumablesList.get((int) view.getTag()).receiptStatus = DbConstants.NOT_RECEIVED;

                            notifyDataSetChanged();

                        }
                    }
                });

                holder.mOptionsIMGBTN.setVisibility(View.VISIBLE);
                holder.mOptionsIMGBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PopupMenu popup = new PopupMenu(mCtx, view);
                        popup.getMenuInflater().inflate(R.menu.menu_mc_received_list,
                                popup.getMenu());
                        popup.show();

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {

                                switch (item.getItemId()) {

                                    case R.id.action_partiallyreceived:

                                        final Dialog dialog = new Dialog(mCtx);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setCancelable(false);
                                        dialog.setContentView(R.layout.dialog_update_quantity);
                                        TextView titleTXT = (TextView) dialog.findViewById(R.id.dialog_title_TXT);
                                        TextInputLayout qtyInputLay = (TextInputLayout) dialog.findViewById(R.id.input_qty_update);
                                        final EditText qtyUpdateEDT = (EditText) dialog.findViewById(R.id.qty_update_EDT);
                                        TextView qtyUnitTXT = (TextView) dialog.findViewById(R.id.qty_unit_TXT);

                                        titleTXT.setText(getResources().getString(R.string.mc_dialog_title_partially_received));
                                        qtyInputLay.setHint(getResources().getString(R.string.mc_dialog_hint_enter_qty));
                                        qtyUnitTXT.setText(aConsumable.unitOfMeasure);
                                        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
                                        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_ok_BTN);
                                        dialogButton.setText(R.string.btn_ok);
                                        dialogButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();

                                                if (qtyUpdateEDT.getText().toString().length() > 0) {

                                                    // update for the checkbox
                                                    if (!mSelectedConsumables.contains(mConsumablesList.get(pos).consumableId)) {
                                                        mSelectedConsumables.add(mConsumablesList.get(pos).consumableId);
                                                    }
                                                    // Updating the data object
                                                    mConsumablesList.get(pos).receivedQuantity = Float.parseFloat(qtyUpdateEDT.getText().toString());
                                                    mConsumablesList.get(pos).receiptStatus = DbConstants.PARTIALLY_RECEIVED;
                                                    notifyDataSetChanged();
                                                }
                                            }
                                        });
                                        dialog.show();

                                        break;
                                }

                                return true;
                            }
                        });


                    }
                });
            } else

            {
                holder.mOptionsIMGBTN.setVisibility(View.GONE);
                holder.mselectionCHBX.setVisibility(View.INVISIBLE);
            }


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Consumable> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mConsumablesList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Consumable g : mFilteredList) {
                                if (g.consumableName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mConsumablesList = (ArrayList<Consumable>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        public ArrayList<Consumable> getUpdatedConsumablesList() {

            return mConsumablesList;
        }


        class ViewHolder {

            TextView mConsumableName, mConsumableDescription, mConsumableTotalQty, mConsumableReceivedQty;
            AppCompatCheckBox mselectionCHBX;
            ImageButton mOptionsIMGBTN;
        }
    }

}
