package io.designdots.devapreservations.modules.userroles.materialcontroller.receivedstock;

import android.app.Dialog;
import android.app.SearchManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;
import io.designdots.devapreservations.utils.TVUtils;

public class MCRSPreservativesList extends BaseAppcompactActivity {

    private BaseAppcompactActivity mCtx;
    private ListView mReceivedLV;
    private CustomListAdapter mAdapter;
    private RadioGroup mPreservativesRGRP;
    private Button mConfirmBTN;
    private ArrayList<Integer> mSelectedPreservatives;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcrspreservatives_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });


        return true;
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCRSPreservativesList.this;
        mReceivedLV = findViewById(R.id.mc_rs_preservatives_LV);
        mConfirmBTN = findViewById(R.id.mc_rs_preservatives_confirm_BTN);
        mPreservativesRGRP = findViewById(R.id.mc_rs_preservatives_tabs_RDGRP);

        mPreservativesRGRP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int selectedId) {

                updateListView(selectedId);

            }
        });


        mPreservativesRGRP.check(R.id.mc_rs_preservatives_not_received_RBTN);
    }

    private void updateListView(int selectedId) {

        ArrayList<Preservative> preservativesList = new ArrayList<>();

        switch (selectedId) {
            case R.id.mc_rs_preservatives_not_received_RBTN:
                preservativesList = PreservativeTable.getInstance().getPreservatives(DbConstants.NOT_RECEIVED);
                mAdapter = new CustomListAdapter(preservativesList, true);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.VISIBLE);
                break;
            case R.id.mc_rs_preservatives_partially_received_RBTN:
                preservativesList = PreservativeTable.getInstance().getPreservatives(DbConstants.PARTIALLY_RECEIVED);
                mAdapter = new CustomListAdapter(preservativesList, true);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.VISIBLE);
                break;
            case R.id.mc_rs_preservatives_fully_received_RBTN:
                preservativesList = PreservativeTable.getInstance().getPreservatives(DbConstants.COMPLETELY_RECEIVED);
                mAdapter = new CustomListAdapter(preservativesList, false);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.GONE);

                break;
        }

    }

    public void OnClickConfirm(View view) {

        if (mSelectedPreservatives.size() > 0) {
            // Update database here
            PreservativeTable.getInstance().updatePreservativesStatus(mAdapter.getUpdatedPreservativeList());
            ShowAlert.showDialog(mCtx, "Confirmation", mSelectedPreservatives.size() + " out of " + mAdapter.getCount() + " items added successfully", "Confirm");
            updateListView(mPreservativesRGRP.getCheckedRadioButtonId());
        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select atleast one preservative", R.color.colorRed, R.color.colorPaleWhite);
        }
    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        private ArrayList<Preservative> mPreservativeList;
        private ArrayList<Preservative> mFilteredList;
        private boolean mEnableMenu;


        public CustomListAdapter(ArrayList<Preservative> PreservativesList, boolean enableMenu) {

            inflater = mCtx.getLayoutInflater();
            this.mPreservativeList = PreservativesList;
            this.mEnableMenu = enableMenu;
            mSelectedPreservatives = new ArrayList<>();
            mSelectedPreservatives.clear();

        }

        public ArrayList<Preservative> getUpdatedPreservativeList() {
            return mPreservativeList;
        }

        @Override
        public int getCount() {
            return mPreservativeList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return mPreservativeList.get(i).preservativeId;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_received_stock_preservative_item, null);
                holder.mChemicalId = convertView.findViewById(R.id.row_received_stock_preservative_chemical_id_TXT);
                holder.mChemicalName = convertView.findViewById(R.id.row_received_stock_preservative_name_TXT);
                holder.mTotalQty = convertView.findViewById(R.id.row_received_stock_preservative_qty_TXT);
                holder.mReceivedQTY = convertView.findViewById(R.id.row_received_stock_preservative_receivedQty_TXT);
                holder.mselectionCHBX = convertView.findViewById(R.id.row_received_stock_preservative_CHBX);
                holder.mOptionsIMGBTN = convertView.findViewById(R.id.row_received_stock_preservative_list_menu_more_options_IMGBTN);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final Preservative aPreservative = mPreservativeList.get(pos);

            holder.mChemicalId.setText(aPreservative.chemicalId);
            holder.mselectionCHBX.setTag(pos);

            holder.mChemicalName.setText(aPreservative.chemicalName);
            holder.mChemicalId.setText(aPreservative.chemicalId);

            String totalQty = aPreservative.quantity + "  " + aPreservative.unitOfMeasure;
            holder.mTotalQty.setText(totalQty);

            if (aPreservative.receiptStatus == DbConstants.PARTIALLY_RECEIVED) {
                String receivedqty = "Received Quantity : " + aPreservative.receivedQuantity + "  " + aPreservative.unitOfMeasure;
                holder.mReceivedQTY.setText(receivedqty);
                holder.mReceivedQTY.setVisibility(View.VISIBLE);
            } else {
                holder.mReceivedQTY.setVisibility(View.GONE);
            }


            if (mEnableMenu) {

                if (mSelectedPreservatives.contains(aPreservative.preservativeId)) {
                    holder.mselectionCHBX.setChecked(true);
                } else {
                    holder.mselectionCHBX.setChecked(false);
                }

                holder.mselectionCHBX.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (((CheckBox) view).isChecked()) {
                            // update for the checkbox view
                            if (!mSelectedPreservatives.contains(aPreservative.preservativeId)) {
                                mSelectedPreservatives.add(aPreservative.preservativeId);
                            }
                            // updating the data object
                            mPreservativeList.get((int) view.getTag()).receiptStatus = DbConstants.COMPLETELY_RECEIVED;
                            mPreservativeList.get((int) view.getTag()).receivedQuantity = mPreservativeList.get((int) view.getTag()).quantity;

                        } else {
                            // update for the checkbox
                            if (mSelectedPreservatives.contains(aPreservative.preservativeId)) {
                                mSelectedPreservatives.remove(aPreservative.preservativeId);
                            }
                            // updating the data objects
                            mPreservativeList.get((int) view.getTag()).receivedQuantity = Float.parseFloat("0");
                            mPreservativeList.get((int) view.getTag()).receiptStatus = DbConstants.NOT_RECEIVED;

                            notifyDataSetChanged();

                        }
                    }
                });

                holder.mOptionsIMGBTN.setVisibility(View.VISIBLE);
                holder.mOptionsIMGBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PopupMenu popup = new PopupMenu(mCtx, view);
                        popup.getMenuInflater().inflate(R.menu.menu_mc_received_list,
                                popup.getMenu());
                        popup.show();

                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {

                                switch (item.getItemId()) {

                                    case R.id.action_partiallyreceived:

                                        final Dialog dialog = new Dialog(mCtx);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setCancelable(false);
                                        dialog.setContentView(R.layout.dialog_update_quantity);
                                        TextView titleTXT = (TextView) dialog.findViewById(R.id.dialog_title_TXT);
                                        final TextInputLayout qtyInputLay = (TextInputLayout) dialog.findViewById(R.id.input_qty_update);
                                        final EditText qtyUpdateEDT = (EditText) dialog.findViewById(R.id.qty_update_EDT);
                                        TextView qtyUnitTXT = (TextView) dialog.findViewById(R.id.qty_unit_TXT);

                                        titleTXT.setText(getResources().getString(R.string.mc_dialog_title_partially_received));
                                        qtyInputLay.setHint(getResources().getString(R.string.mc_dialog_hint_enter_qty));
                                        qtyUnitTXT.setText(aPreservative.unitOfMeasure);
                                        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.FILL_PARENT;
                                        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_ok_BTN);
                                        dialogButton.setText(R.string.btn_ok);
                                        dialogButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();

                                                if (qtyUpdateEDT.getText().toString().length() > 0) {

                                                    // update for the checkbox
                                                    if (!mSelectedPreservatives.contains(mPreservativeList.get(pos).preservativeId)) {
                                                        mSelectedPreservatives.add(mPreservativeList.get(pos).preservativeId);
                                                    }
                                                    // Updating the data object
                                                    mPreservativeList.get(pos).receivedQuantity = Float.parseFloat(qtyUpdateEDT.getText().toString());
                                                    mPreservativeList.get(pos).receiptStatus = DbConstants.PARTIALLY_RECEIVED;
                                                    notifyDataSetChanged();
                                                }
                                            }
                                        });
                                        dialog.show();

                                        break;
                                }

                                return true;
                            }
                        });


                    }
                });
            } else {
                holder.mOptionsIMGBTN.setVisibility(View.GONE);
                holder.mselectionCHBX.setVisibility(View.INVISIBLE);
            }

            return convertView;
        }

        @Override
        public Filter getFilter() {

            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<Preservative> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mPreservativeList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final Preservative g : mFilteredList) {
                                if (g.chemicalName.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mPreservativeList = (ArrayList<Preservative>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mChemicalId, mChemicalName, mReceivedQTY, mTotalQty;
            AppCompatCheckBox mselectionCHBX;
            ImageButton mOptionsIMGBTN;
        }
    }

}
