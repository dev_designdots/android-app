package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

public class MCMaterialConsumptionLanding extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaterial_consumption);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMaterialConsumptionLanding.this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    public void onClickPreservativesConsumption(View view) {

        Intent i = new Intent(mCtx, MCMCPreservativesList.class);
        startActivity(i);

    }

    public void onClickConsumablesConsumption(View view) {
        Intent i = new Intent(mCtx, MCMCConsumablesList.class);
        startActivity(i);
    }

    public void onClickReturnableToolsConsumption(View view) {
        Intent i = new Intent(mCtx, MCMCReturnableToolsList.class);
        startActivity(i);
    }
}
