package io.designdots.devapreservations.modules.userroles.qualitycontroller.cmc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.utils.ShowAlert;
import io.designdots.devapreservations.utils.TVUtils;

public class QCCMCEquipmentsList extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qccmcequipments_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        mCtx = QCCMCEquipmentsList.this;

        mLV = findViewById(R.id.qc_cmc_equipments_LV);
        mAdapter = new CustomListAdapter();
        mLV.setAdapter(mAdapter);


        ShowAlert.showSnackMsg(mCtx, "10 Equipments to be tested", R.color.colorRed, R.color.colorPaleWhite);


    }

    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;

        public CustomListAdapter() {

            inflater = mCtx.getLayoutInflater();
        }

        @Override
        public int getCount() {
            return 10;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_list_item, null);
                holder.mTextOne = (TextView) convertView.findViewById(R.id.row_mc_list_TXT_one);
                holder.mTextTwo = (TextView) convertView.findViewById(R.id.row_mc_list_TXT_two);
                holder.mOptionsIBTN = (ImageButton) convertView.findViewById(R.id.row_mc_list_menu_more_options_IMGBTN);
                holder.mChooseCHBX = (AppCompatCheckBox) convertView.findViewById(R.id.row_mc_list_CHBX);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            // hide Checbox;
            holder.mChooseCHBX.setVisibility(View.GONE);
            holder.mChooseCHBX.setEnabled(false);
            holder.mOptionsIBTN.setImageResource(R.drawable.ic_arrow_right);


            holder.mTextOne.setText("Air Winch " + i + 1);
            TVUtils.setTextColor(mCtx, holder.mTextOne, R.color.colorBlack);

            holder.mTextTwo.setText("D " + i + 1);
            TVUtils.setTextColor(mCtx, holder.mTextTwo, R.color.colorGrey);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(mCtx, QCCMCDetails.class);
                    startActivity(i);
                }
            });


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return null;
        }


        class ViewHolder {

            TextView mTextOne, mTextTwo;
            AppCompatCheckBox mChooseCHBX;
            ImageButton mOptionsIBTN;

        }
    }


}
