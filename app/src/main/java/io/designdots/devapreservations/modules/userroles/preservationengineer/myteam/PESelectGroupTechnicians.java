package io.designdots.devapreservations.modules.userroles.preservationengineer.myteam;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.TechnicianTable;
import io.designdots.devapreservations.utils.Constants;
import io.designdots.devapreservations.utils.ShowAlert;

public class PESelectGroupTechnicians extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mSelectTechnicianLV;
    private CustomListAdapter mAdapter;
    ArrayList<Integer> mSelectedTechnicians;
    LinearLayout mEditOptionsLAY;
    Integer mGroupId;
    Button mNextBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peselect_group_technicians);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    private void initialiseViewsAndObjects() {
        mCtx = PESelectGroupTechnicians.this;
        mEditOptionsLAY = findViewById(R.id.pe_grp_edit_buttons_LAY);
        mSelectTechnicianLV = findViewById(R.id.pe_select_technicians_LV);
        mNextBTN = findViewById(R.id.pe_select_technician_next_BTN);
        mSelectedTechnicians = new ArrayList<>();


        if (getIntent().getStringExtra(Constants.GROUP_OPERATION).equals(Constants.ADD_NEW)) {
            mEditOptionsLAY.setVisibility(View.GONE);
            mNextBTN.setVisibility(View.VISIBLE);

            Integer maxGroupId = TechnicianTable.getInstance().getMaxGroupId();
            mGroupId = maxGroupId + 1;
            ArrayList<Technician> aUnallocatedTechniciansList = TechnicianTable.getInstance().getAllTechniciansWithoutGroup();

            mAdapter = new CustomListAdapter(aUnallocatedTechniciansList);
            mSelectTechnicianLV.setAdapter(mAdapter);

        } else if (getIntent().getStringExtra(Constants.GROUP_OPERATION).equals(Constants.EDIT)) {
            mEditOptionsLAY.setVisibility(View.VISIBLE);
            mNextBTN.setVisibility(View.GONE);
            mGroupId = getIntent().getIntExtra(DbConstants.KEY_GROUP_ID, -1);

            ArrayList<Technician> aGroupTechnicians = TechnicianTable.getInstance().getAllTechniciansUnderGroup(mGroupId);
            mAdapter = new CustomListAdapter(aGroupTechnicians);
            mSelectTechnicianLV.setAdapter(mAdapter);

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        final SearchView aSearchview = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        aSearchview.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        return true;
    }

    public void onDoneButtonClicked(View view) {
        PESelectGroupTechnicians.this.finish();

    }

    public void onRemoveButtonClicked(View view) {

        if (mSelectedTechnicians.size() > 0) {
            TechnicianTable.getInstance().removeTechniciansFromGroup(mSelectedTechnicians);
            mGroupId = getIntent().getIntExtra(DbConstants.KEY_GROUP_ID, -1);
            ArrayList<Technician> aGroupTechnicians = TechnicianTable.getInstance().getAllTechniciansUnderGroup(mGroupId);
            mAdapter = new CustomListAdapter(aGroupTechnicians);
            mSelectTechnicianLV.setAdapter(mAdapter);
        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select atleast one technican", R.color.colorRed, R.color.colorPaleWhite);
        }

    }

    public void onNextButtonClicked(View view) {

        if (mSelectedTechnicians.size() > 0) {
            Intent i = new Intent(mCtx, PEAddNewGroup.class);
            i.putExtra(DbConstants.KEY_GROUP_ID, mGroupId);
            i.putExtra(DbConstants.KEY_TECHNICIAN_ID, mSelectedTechnicians);
            startActivity(i);
        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select atleast one technican", R.color.colorRed, R.color.colorPaleWhite);
        }

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;

        ArrayList<Technician> mTechniciansList;

        public CustomListAdapter(ArrayList<Technician> techniciansList) {

            inflater = mCtx.getLayoutInflater();
            this.mTechniciansList = techniciansList;
            mSelectedTechnicians.clear();
        }


        @Override
        public int getCount() {
            return mTechniciansList.size();
        }

        @Override
        public Object getItem(int pos) {
            return pos;
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_pe_choose_technician_item, null);
                holder.mTechnicianName = convertView.findViewById(R.id.pe_technician_name_TXT);
                holder.mTechnicianId = convertView.findViewById(R.id.pe_choose_technician_id_TXT);
                holder.mTechnicianSelectionCHBX = convertView.findViewById(R.id.pe_technician_select_CHBX);


                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final Technician technician = mTechniciansList.get(pos);

            String aTechnicianName = technician.firstName + " " + technician.surName;
            String aTechnicianId = "" + technician.partyKey;

            holder.mTechnicianName.setText(aTechnicianName);
            holder.mTechnicianId.setText(aTechnicianId);

            if (mSelectedTechnicians.contains(technician.partyKey)) {
                holder.mTechnicianSelectionCHBX.setChecked(true);
            } else {
                holder.mTechnicianSelectionCHBX.setChecked(false);
            }

            holder.mTechnicianSelectionCHBX.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (((AppCompatCheckBox) view).isChecked()) {
                        if (!mSelectedTechnicians.contains(technician.partyKey))
                            mSelectedTechnicians.add(technician.partyKey);
                    } else {
                        if (mSelectedTechnicians.contains(technician.partyKey))
                            mSelectedTechnicians.remove(technician.partyKey);
                    }
                }
            });


            return convertView;
        }


        @Override
        public Filter getFilter() {
            return null;
        }


        class ViewHolder {

            TextView mTechnicianName, mTechnicianId;
            AppCompatCheckBox mTechnicianSelectionCHBX;

        }
    }


}
