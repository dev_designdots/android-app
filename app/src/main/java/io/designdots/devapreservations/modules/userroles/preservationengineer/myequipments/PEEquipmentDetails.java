package io.designdots.devapreservations.modules.userroles.preservationengineer.myequipments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

import static io.designdots.devapreservations.utils.Constants.CAMERA_PERMISSION_CODE;
import static io.designdots.devapreservations.utils.Constants.CAMERA_REQUEST;

public class PEEquipmentDetails extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private LinearLayout mStarEndLAY, mBeforeAfterLAY;
    private TextView mBeforePhotoDateTXT, mDuringPhotoDateTXT, mEqpmtCategoryTXT, mEqpmtLocationTXT,
            mEqpmtZoneTXT, mContentNameTXT, mWaterDieselOilTXT, mEmptyVolTXT, mContentVolTXT;
    private ImageView mBeforePhotoIMG, mDuringPhotoIMG, mSelectedIMG;
    private Button mBeforPhotoResetBTN, mDuringPhotoResetBTN, mConfirmBTN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peequipment_details);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();

        setListeners();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        mCtx = PEEquipmentDetails.this;

        mStarEndLAY = findViewById(R.id.pe_equipment_details_start_end_LAY);
        mBeforeAfterLAY = findViewById(R.id.pe_equipment_details_photo_LAY);
        mBeforePhotoDateTXT = findViewById(R.id.pe_equipments_before_photo_date_TXT);
        mDuringPhotoDateTXT = findViewById(R.id.pe_equipments_during_photo_date_TXT);
        mEqpmtCategoryTXT = findViewById(R.id.pe_equipments_category_TXT);
        mEqpmtLocationTXT = findViewById(R.id.pe_equipments_location_TXT);
        mEqpmtZoneTXT = findViewById(R.id.pe_equipments_zone_TXT);
        mContentNameTXT = findViewById(R.id.pe_equipments_content_when_filled_TXT);
        mWaterDieselOilTXT = findViewById(R.id.pe_equipments_water_diesel_oil_TXT);
        mEmptyVolTXT = findViewById(R.id.pe_equipments_empty_vol_TXT);
        mContentVolTXT = findViewById(R.id.pe_equipments_content_vol_TXT);
        mBeforePhotoIMG = findViewById(R.id.pe_equipments_before_IMG);
        mDuringPhotoIMG = findViewById(R.id.pe_equipments_during_IMG);
        mBeforPhotoResetBTN = findViewById(R.id.pe_equipments_before_photo_reset_BTN);
        mDuringPhotoResetBTN = findViewById(R.id.pe_equipments_during_photo_reset_BTN);
        mConfirmBTN = findViewById(R.id.pe_equipments_confirm_BTN);


    }

    private void setListeners() {

        mDuringPhotoResetBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mBeforPhotoResetBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        mBeforePhotoIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedIMG = mBeforePhotoIMG;


            }
        });

        mDuringPhotoIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedIMG = mDuringPhotoIMG;

            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mCtx, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new
                        Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                Toast.makeText(mCtx, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            mSelectedIMG.setImageBitmap(photo);
        }
    }

}