package io.designdots.devapreservations.modules.userroles.materialcontroller.materialallocation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

public class MCMaterialAllocationLanding extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmaterial_allocation);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


        initialiseViewsAndObjects();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMaterialAllocationLanding.this;
    }

    public void onClickPreservativesAllocation(View view) {

        Intent i = new Intent(mCtx, MCMAPreservativesList.class);
        startActivity(i);
    }

    public void onCLickConsumablesAllocation(View view) {
        Intent i = new Intent(mCtx, MCMAConsumablesList.class);
        startActivity(i);
    }

    public void onClickReturnableToolsAllocation(View view) {
        Intent i = new Intent(mCtx, MCMAReturnableToolsList.class);
        startActivity(i);
    }
}
