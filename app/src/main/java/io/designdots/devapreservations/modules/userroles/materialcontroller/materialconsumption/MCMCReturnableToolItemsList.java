package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.utils.Constants;

public class MCMCReturnableToolItemsList extends BaseAppcompactActivity {

    private AppCompatActivity mCtx;
    private ListView mLV;
    private CustomListAdapter mAdapter;
    private String mToolName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcreturnable_tool_items_list);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });
        return true;
    }

    private void initialiseViewsAndObjects() {
        mCtx = MCMCReturnableToolItemsList.this;

        mToolName = getIntent().getStringExtra(DbConstants.KEY_TOOL_NAME);
        setTitle(mToolName);

        mLV = findViewById(R.id.mc_mc_returnable_tools_items_LV);
        ArrayList<ReturnableTool> aToolsList = ReturnableToolTable.getInstance().getAllTools(mToolName);
        mAdapter = new CustomListAdapter(aToolsList);
        mLV.setAdapter(mAdapter);

        mLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                Intent i = new Intent(mCtx, MCMCReturnableToolConsumptionDetailsList.class);
                i.putExtra(DbConstants.KEY_RETURNABLE_TOOL_ID, (int) mAdapter.getItemId(pos));
                i.putExtra(DbConstants.KEY_TOOL_ID, ((ReturnableTool) mAdapter.getItem(pos)).toolId);
                startActivity(i);

            }
        });


    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        ArrayList<ReturnableTool> mToolsList;
        ArrayList<ReturnableTool> mFilteredList;

        public CustomListAdapter(ArrayList<ReturnableTool> toolsList) {

            inflater = mCtx.getLayoutInflater();
            this.mToolsList = toolsList;
            mFilteredList = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mToolsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mToolsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mToolsList.get(pos).returnableToolId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_mc_returnable_tool_item, null);
                holder.mToolName = (TextView) convertView.findViewById(R.id.row_mc_mc_tool_name_TXT);
                holder.mToolDescription = convertView.findViewById(R.id.row_mc_mc_tool_description_TXT);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ReturnableTool tool = mToolsList.get(pos);

            holder.mToolName.setText(tool.toolId);

            String des = tool.brand + Constants.TENSPACE_SEPARATOR + tool.model + Constants.TENSPACE_SEPARATOR + tool.description;
            holder.mToolDescription.setText(des);


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<ReturnableTool> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mToolsList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final ReturnableTool g : mFilteredList) {
                                if (g.toolId.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mToolsList = (ArrayList<ReturnableTool>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }


        class ViewHolder {

            TextView mToolName, mToolDescription;

        }
    }

}
