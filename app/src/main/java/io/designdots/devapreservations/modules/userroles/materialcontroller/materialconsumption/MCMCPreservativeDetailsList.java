package io.designdots.devapreservations.modules.userroles.materialcontroller.materialconsumption;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.model.PreservativeAllocation;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.EquipmentTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeAllocationTable;
import io.designdots.devapreservations.database.sqlite.tables.PreservativeTable;
import io.designdots.devapreservations.utils.Constants;

public class MCMCPreservativeDetailsList extends BaseAppcompactActivity {


    private BaseAppcompactActivity mCtx;
    private ListView mMCMCPreservativeDetailsLV;
    private CustomListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcmcpreservative_details_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void initialiseViewsAndObjects() {
        mCtx = MCMCPreservativeDetailsList.this;
        mMCMCPreservativeDetailsLV = findViewById(R.id.mc_mc_preservatives_details_LV);

        Bundle aBundle = getIntent().getExtras();

        Integer preservativeId = aBundle.getInt(DbConstants.KEY_PRESERVATIVE_ID, 0);

        Preservative preservative = PreservativeTable.getInstance().getAllocatedPreservative(preservativeId);
        setTitle(preservative.chemicalName);

        ArrayList<PreservativeAllocation> preservativeAllocations = PreservativeAllocationTable.getInstance().getAllocatedDataforPreservative(preservativeId);
        mAdapter = new CustomListAdapter(preservativeId, preservativeAllocations);
        mMCMCPreservativeDetailsLV.setAdapter(mAdapter);

        mMCMCPreservativeDetailsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {

                Intent i = new Intent(mCtx, MCMCConsumptionDetails.class);
                Bundle aBundle = getIntent().getExtras();
                aBundle.putInt(DbConstants.KEY_PRESERVATIVE_ALLOC_KEY,
                        ((PreservativeAllocation) mAdapter.getItem(pos)).preservativeAllocKey);
                i.putExtras(aBundle);
                startActivity(i);
            }
        });
    }

    class CustomListAdapter extends BaseAdapter {

        LayoutInflater inflater;
        Integer mPreservativeId;
        ArrayList<PreservativeAllocation> mPreservativesAllocationList;

        public CustomListAdapter(Integer preservativeId, ArrayList<PreservativeAllocation> preservativeAllocationsList) {

            inflater = mCtx.getLayoutInflater();
            this.mPreservativeId = preservativeId;
            this.mPreservativesAllocationList = preservativeAllocationsList;
        }

        @Override
        public int getCount() {
            return mPreservativesAllocationList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mPreservativesAllocationList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mPreservativesAllocationList.get(pos).preservativeId;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_mc_mc_preservative_details_item, null);
                holder.mAllocatedDateTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_date_TXT);
                holder.mAllocatedTimeTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_time_TXT);
                holder.mEquipmentTagNoTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_equipment_tag_no_TXT);
                holder.mAllocatedQtyTXT = convertView.findViewById(R.id.row_mc_mc_preservative_details_total_qty_allocated_TXT);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            PreservativeAllocation allocation = mPreservativesAllocationList.get(pos);

            String[] equipmentsarray = allocation.equipments.split(",");
            ArrayList<Integer> equipmentIds = new ArrayList<>();
            for (int i = 0; i < equipmentsarray.length; i++) {
                equipmentIds.add(Integer.parseInt(equipmentsarray[i]));
            }

            ArrayList<Equipment> aEquipmentsList = EquipmentTable.getInstance().getFinalEquipmentsListforPreservativeAllocation(mPreservativeId, equipmentIds);

            String aEquipmentTag = "";
            for (int i = 0; i < aEquipmentsList.size(); i++) {

                Equipment e = aEquipmentsList.get(i);
                aEquipmentTag = aEquipmentTag + e.tagNumber;
                if (i != 0 && ((i + 1) % 3) == 0) {
                    aEquipmentTag = aEquipmentTag + "\n";
                } else {
                    aEquipmentTag = aEquipmentTag + Constants.TENSPACE_SEPARATOR;
                }

            }
            holder.mEquipmentTagNoTXT.setText(aEquipmentTag);

            String aAllocatedQty = allocation.quantity + " " + PreservativeTable.getInstance().getAllocatedPreservative(mPreservativeId).unitOfMeasure;
            holder.mAllocatedQtyTXT.setText(aAllocatedQty);

            return convertView;
        }

        class ViewHolder {

            TextView mEquipmentTagNoTXT, mAllocatedQtyTXT, mAllocatedDateTXT, mAllocatedTimeTXT;

        }
    }
}
