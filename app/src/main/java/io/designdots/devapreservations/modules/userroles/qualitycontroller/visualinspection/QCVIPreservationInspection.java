package io.designdots.devapreservations.modules.userroles.qualitycontroller.visualinspection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;

public class QCVIPreservationInspection extends BaseAppcompactActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qcvipreservation_inspection);
    }
}
