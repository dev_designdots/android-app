package io.designdots.devapreservations.modules.userroles.materialcontroller.receivedstock;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.designdots.devapreservations.R;
import io.designdots.devapreservations.app.BaseAppcompactActivity;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.sqlite.DbConstants;
import io.designdots.devapreservations.database.sqlite.tables.ReturnableToolTable;
import io.designdots.devapreservations.utils.ShowAlert;

public class MCRSReturnableToolsDetailsList extends BaseAppcompactActivity {


    private BaseAppcompactActivity mCtx;
    private ListView mReceivedLV;
    private CustomListAdapter mAdapter;
    private RadioGroup mReturnableToolsRGRP;
    private ArrayList<Integer> mSelectedTools;
    private String mToolName;
    private Button mConfirmBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcrsreturnable_tools_details_list);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initialiseViewsAndObjects();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                super.onBackPressed();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                if (TextUtils.isEmpty(newText)) {
                    mAdapter.getFilter().filter("");
                } else {
                    mAdapter.getFilter().filter(newText);
                }
                return true;
            }
        });
        return true;
    }

    private void initialiseViewsAndObjects() {

        mCtx = MCRSReturnableToolsDetailsList.this;
        mToolName = getIntent().getStringExtra(DbConstants.KEY_TOOL_NAME);
        setTitle(mToolName);

        mReturnableToolsRGRP = findViewById(R.id.mc_rs_returnable_tools_tabs_RDGRP);
        mConfirmBTN = findViewById(R.id.mc_rs_returnable_tool_details_confirm_BTN);
        mReceivedLV = findViewById(R.id.mc_rs_returnabletools_details_LV);


        mReturnableToolsRGRP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int selId) {

                updateListView(selId);


            }
        });

        mReturnableToolsRGRP.check(R.id.mc_rs_returnable_tools_not_received_RBTN);


    }

    private void updateListView(int selId) {


        switch (selId) {

            case R.id.mc_rs_returnable_tools_not_received_RBTN:
                mAdapter = new CustomListAdapter(ReturnableToolTable.getInstance().getTools(mToolName, DbConstants.NOT_RECEIVED), true);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.VISIBLE);

                break;

            case R.id.mc_rs_eturnable_tools_received_RBTN:
                mAdapter = new CustomListAdapter(ReturnableToolTable.getInstance().getTools(mToolName, DbConstants.COMPLETELY_RECEIVED), false);
                mReceivedLV.setAdapter(mAdapter);
                mConfirmBTN.setVisibility(View.GONE);

                break;

        }
    }

    public void onClickConfirm(View view) {

        if (mSelectedTools.size() > 0) {
            // Update database here
            ReturnableToolTable.getInstance().updateTools(mAdapter.getUpdatedConsumablesList());
            ShowAlert.showDialog(mCtx, "Confirmation", mSelectedTools.size() + " out of " + mAdapter.getCount() + " items received successfully", "Confirm");

            updateListView(mReturnableToolsRGRP.getCheckedRadioButtonId());

        } else {
            ShowAlert.showSnackMsg(mCtx, "Please select atleast one tool", R.color.colorRed, R.color.colorPaleWhite);
        }

    }


    class CustomListAdapter extends BaseAdapter implements Filterable {

        LayoutInflater inflater;
        private ArrayList<ReturnableTool> mReturnableToolsList;
        private ArrayList<ReturnableTool> mFilteredList;
        private Boolean isCheckEnabled = false;

        public CustomListAdapter(ArrayList<ReturnableTool> returnableToolsList, Boolean CheckEnabled) {

            inflater = mCtx.getLayoutInflater();
            this.isCheckEnabled = CheckEnabled;
            this.mReturnableToolsList = returnableToolsList;
            mSelectedTools = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mReturnableToolsList.size();
        }

        @Override
        public Object getItem(int pos) {
            return mReturnableToolsList.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return mReturnableToolsList.get(pos).returnableToolId;
        }

        @Override
        public View getView(final int pos, View convertView, ViewGroup viewGroup) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_received_stock_returnable_tool, null);
                holder.mToolId = convertView.findViewById(R.id.row_mc_rs_returnable_tool_id_TXT);
                holder.mBrand = convertView.findViewById(R.id.row_mc_rs_returnable_tool_brand_TXT);
                holder.mModel = convertView.findViewById(R.id.row_mc_rs_returnable_tool_model_TXT);
                holder.mDescription = convertView.findViewById(R.id.row_mc_rs_returnable_tool_description_TXT);
                holder.mselectionCHBX = convertView.findViewById(R.id.row_mc_rs_returnable_tool_CHBX);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ReturnableTool tool = mReturnableToolsList.get(pos);
            holder.mToolId.setText(tool.toolId);
            holder.mBrand.setText(tool.brand);
            holder.mModel.setText(tool.model);
            holder.mDescription.setText(tool.description);

            if (isCheckEnabled) {
                holder.mselectionCHBX.setVisibility(View.VISIBLE);

                if (mSelectedTools.contains(tool.returnableToolId)) {
                    holder.mselectionCHBX.setChecked(true);
                } else {
                    holder.mselectionCHBX.setChecked(false);
                }

                holder.mselectionCHBX.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (((CheckBox) view).isChecked()) {

                            if (!mSelectedTools.contains(mReturnableToolsList.get(pos).returnableToolId)) {
                                mSelectedTools.add(mReturnableToolsList.get(pos).returnableToolId);
                            }
                            mReturnableToolsList.get(pos).receiptStatus = DbConstants.COMPLETELY_RECEIVED;
                        } else {
                            if (mSelectedTools.contains(mReturnableToolsList.get(pos).returnableToolId)) {
                                mSelectedTools.remove(mReturnableToolsList.get(pos).returnableToolId);
                            }
                            mReturnableToolsList.get(pos).receiptStatus = DbConstants.NOT_RECEIVED;
                        }
                    }
                });
            } else {
                holder.mselectionCHBX.setVisibility(View.INVISIBLE);
            }


            return convertView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {

                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<ReturnableTool> results = new ArrayList<>();
                    if (mFilteredList == null)
                        mFilteredList = mReturnableToolsList;
                    if (constraint != null) {
                        if (mFilteredList != null && mFilteredList.size() > 0) {
                            for (final ReturnableTool g : mFilteredList) {
                                if (g.toolId.toLowerCase()
                                        .contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mReturnableToolsList = (ArrayList<ReturnableTool>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        public ArrayList<ReturnableTool> getUpdatedConsumablesList() {
            return mReturnableToolsList;
        }


        class ViewHolder {

            TextView mToolId, mBrand, mModel, mDescription;
            AppCompatCheckBox mselectionCHBX;
        }
    }

}
