package io.designdots.devapreservations.webservices.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.Technician;

public class PreservationEngineerForward {

    @SerializedName("technicians")
    ArrayList<Technician> technicians;

    @SerializedName("equipments")
    ArrayList<Equipment> equipments;

}
