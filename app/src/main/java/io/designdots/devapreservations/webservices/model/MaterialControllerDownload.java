package io.designdots.devapreservations.webservices.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.designdots.devapreservations.database.model.Consumable;
import io.designdots.devapreservations.database.model.Equipment;
import io.designdots.devapreservations.database.model.PreservationEngineer;
import io.designdots.devapreservations.database.model.Preservative;
import io.designdots.devapreservations.database.model.ReturnableTool;
import io.designdots.devapreservations.database.model.Technician;
import io.designdots.devapreservations.database.model.TechnicianAssignment;
import io.designdots.devapreservations.database.model.WorkDay;

public class MaterialControllerDownload {

    @SerializedName("materialController")
    public MaterialController materialController;

    public class MaterialController {

        @SerializedName("consumables")
        public ArrayList<Consumable> consumables;
        @SerializedName("preservatives")
        public ArrayList<Preservative> preservatives;
        @SerializedName("returnableTools")
        public ArrayList<ReturnableTool> returnableTools;
        @SerializedName("equipments")
        public ArrayList<Equipment> equipments;
        @SerializedName("technicians")
        public ArrayList<Technician> technicians;
        @SerializedName("preservationEngineers")
        public ArrayList<PreservationEngineer> preservationEngineers;
        @SerializedName("technicianAssignments")
        public ArrayList<TechnicianAssignment> technicianAssignments;
        @SerializedName("workDaysList")
        public ArrayList<WorkDay> workDaysList;


    }

}