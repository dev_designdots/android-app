package io.designdots.devapreservations.app;

import android.app.Application;

import io.designdots.devapreservations.database.sqlite.DbManager;

public class DevaPreservationApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //Initializing database instances for All tables
        DbManager.init(getApplicationContext());
        DbManager.open();
    }


}
